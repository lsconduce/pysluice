VIRTUAL_ENV ?= $(PWD)/venv
export VIRTUAL_ENV

all: build

clean:
	rm -rf $(VIRTUAL_ENV)

test: $(VIRTUAL_ENV)/requirements.txt
	env PATH=$(VIRTUAL_ENV)/bin:$(PATH) pip install mock
	env PATH=$(VIRTUAL_ENV)/bin:$(PATH) python setup.py test

build: $(VIRTUAL_ENV)/requirements.txt
	env PATH=$(VIRTUAL_ENV)/bin:$(PATH) python setup.py build

install: $(VIRTUAL_ENV)/requirements.txt
	env PATH=$(VIRTUAL_ENV)/bin:$(PATH) pip uninstall mock || exit 0
	env PATH=$(VIRTUAL_ENV)/bin:$(PATH) python setup.py install

$(VIRTUAL_ENV)/bin/pip:
	virtualenv $(VIRTUAL_ENV)

$(VIRTUAL_ENV)/requirements.txt: setup.py requirements.txt $(VIRTUAL_ENV)/bin/pip
	env PATH=$(VIRTUAL_ENV)/bin:$(PATH) pip install -r requirements.txt
	cp requirements.txt $@
