PACKAGE_VERSION=`echo -e "import sluice\nprint sluice.PYSLUICE_VERSION" | python -`
REPO="git@git.mct.io:util/pysluice.git"
BRANCH="develop"
DEBS_PATH=$(pwd)/debs/python-sluice
BUILD_PATH=buildtmp

BASE_PATH=$(pwd)

#create the control, prerm and postinst files
sed -e "s,@VERSION@,${PACKAGE_VERSION},g" ${DEBS_PATH}/DEBIAN/control.in \
    > ${DEBS_PATH}/DEBIAN/control
sed -e "s,@prefix@,${BIN_PATH},g" ${DEBS_PATH}/DEBIAN/postinst.in \
    > ${DEBS_PATH}/DEBIAN/postinst
chmod +x ${DEBS_PATH}/DEBIAN/postinst
sed -e "s,@prefix@,${BIN_PATH},g" ${DEBS_PATH}/DEBIAN/prerm.in \
    > ${DEBS_PATH}/DEBIAN/prerm
chmod +x ${DEBS_PATH}/DEBIAN/prerm

# Remove the old files
rm -fr ${DEBS_PATH}/${BUILD_PATH}
rm -fr ${DEBS_PATH}/etc
rm -fr ${DEBS_PATH}/usr
rm -fr ${DEBS_PATH}/Library
rm -fr ${DEBS_PATH}/System

# checkout the latest from the defined branch
git archive --format=tar --prefix=${BUILD_PATH}/ \
    --remote=${REPO} ${BRANCH} | tar -C ${DEBS_PATH} -x

#build the python source
pushd ${DEBS_PATH}/${BUILD_PATH}
python setup.py install --root=${DEBS_PATH}
popd

rm -fr ${DEBS_PATH}/${BUILD_PATH}

# create the deb
dpkg-deb -b debs/python-sluice "$@"
