from setuptools import setup, find_packages
from setuptools.command.install import install
import os, shutil
import pkg_resources

try:
    import sluice
    version = sluice.PYSLUICE_VERSION
except ImportError as e:
    version = '1.1.8'

def splitpath(fn):
    path = []
    while fn:
        head, tail = os.path.split(fn)
        if head == fn:
            path.insert(0, head)
            break
        path.insert(0, tail)
        fn = head
    return path

class myinstall(install):
    def run(self):
        install.run(self)

setup(
    name         = 'pysluice',
    version      = version,
    #packages     = find_packages(),
    install_requires     = ['pyplasma >= 1.1.1', 'numpy'],
    extras_require = {
        'texture-generator': ['Pillow', 'conduce-pycairo'],
        'mysql': ['MySQL-python'],
        'postgres': ['psycopg2'],
        'geocode': ['pyparsing', 'psycopg2'],
        'twitter': ['twitter', 'scikit-learn', 'numpy', 'scipy']
    },
    author       = 'MCT',
    author_email = 'support@mct.io',
    description  = 'Python implementation of Sluice Plasma protocol',
    license      = '???',
    keywords     = 'sluice emcs mct plasma',
    url          = 'http://mct.io/',
    packages     = ['sluice', 'sluice.geocode', 'sluice.sentiment',
                     'sluice.settings', 'sluice.settings.fluoro'],
    classifiers  = ['Development Status :: 2 - Pre-Alpha',
                    'Environment :: Console',
                    'Intended Audience :: Developers',
                    'Intended Audience :: Information Technology',
                    'Intended Audience :: Science/Research',
                    'Intended Audience :: Telecommunications Industry',
                    'Topic :: Communications',
                    'Topic :: Software Development :: User Interfaces',
    ],
    test_suite = 't.main.get_suite',
    cmdclass = { 'install': myinstall },
)

