1.1.8
=====

* Fixes issue with new protein file naming conventions and static-layers
* Removes deprecated treatment runner code
* API change requiring config.json to be given to Settings object
* Use explicit paths when loading sluice-settings and config.json files
* Adds a cplasma-based texture daemon

