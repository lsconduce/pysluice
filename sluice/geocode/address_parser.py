import sys, os, re
from copy import deepcopy
from pyparsing import *
from sluice.util import ucfirst

STREET_TYPES = {
    'street': 'St',
    'boulevard': 'Blvd',
    'lane': 'Ln',
    'road': 'Rd',
    'avenue': 'Ave',
    'circle': 'Cir',
    'cove': 'Cv',
    'drive': 'Dr',
    'parkway': 'Pkwy',
    'court': 'Ct',
    'square': 'Sq',
    'loop': 'Lp',
    'route': 'Rt',
    'highway': 'Hwy',
    'way': 'Way',
}

UNIT_TYPES = {
    'suite': 'Ste',
    'apartment': 'Apt',
    'room': 'Rm',
}

def create_street_parser():
    ## http://pyparsing.wikispaces.com/file/view/streetAddressParser.py

    # define number as a set of words
    units = oneOf("Zero One Two Three Four Five Six Seven Eight Nine Ten"
                  "Eleven Twelve Thirteen Fourteen Fifteen Sixteen Seventeen"
                  "Eighteen Nineteen", caseless=True)
    tens = oneOf("Ten Twenty Thirty Forty Fourty Fifty Sixty Seventy Eighty"
                 "Ninety",caseless=True)
    hundred = CaselessLiteral("Hundred")
    thousand = CaselessLiteral("Thousand")
    OPT_DASH = Optional("-")
    numberword = ((( units + OPT_DASH + Optional(thousand) + OPT_DASH + 
                      Optional(units + OPT_DASH + hundred) + OPT_DASH + 
                      Optional(tens)) ^ tens ) 
                   + OPT_DASH + Optional(units) )

    # number can be any of the forms 123, 21B, 222-A or 23 1/2
    housenumber = originalTextFor( numberword | Combine(Word(nums) + 
                        Optional(OPT_DASH + oneOf(list(alphas)) +
                                 FollowedBy(White()))) + 
                        Optional(OPT_DASH + "1/2")
                        )
    numberSuffix = oneOf("st th nd rd").setName("numberSuffix")
    streetnumber = originalTextFor( Word(nums) + 
                     Optional(OPT_DASH + "1/2") +
                     Optional(numberSuffix) )

    # just a basic word of alpha characters, Maple, Main, etc.
    name = ~numberSuffix + Word(alphas)

    # types of streets - extend as desired
    street_types = list(Keyword(x, caseless=True)
                        for x in STREET_TYPES.keys()+STREET_TYPES.values())
    type_ = Combine(MatchFirst(street_types) +
                    Optional(".").suppress())

    # street name 
    nsew = Combine(oneOf("N S E W North South East West NW NE SW SE", caseless=True) +
                   Optional("."))
    #streetName = (Combine(Optional(nsew) + streetnumber + 
    streetName = (Combine(streetnumber + 
                          Optional("1/2") + 
                          Optional(numberSuffix),
                          joinString=" ", adjacent=False)
                  ^ Combine(~numberSuffix +
                            OneOrMore(~type_ + Combine(Word(alphas) +
                                      Optional("."))),
                            joinString=" ", adjacent=False)
                  ^ Combine("Avenue" + Word(alphas),
                            joinString=" ", adjacent=False)
                  ).setName("name")

    # PO Box handling
    acronym = lambda s : Regex(r"\.?\s*".join(s)+r"\.?")
    poBoxRef = ((acronym("PO") | acronym("APO") | acronym("AFP")) + 
                 Optional(CaselessLiteral("BOX"))) + \
               Word(alphanums)("boxnumber")

    # basic street address
    streetReference = Optional(nsew)('direction') + \
                      streetName.setResultsName("name") + \
                      Optional(type_).setResultsName("type") + \
                      Optional(nsew+FollowedBy(White())).setResultsName('quadrant')
    direct = housenumber.setResultsName("number") + streetReference
    intersection = ( streetReference.setResultsName("crossStreet") + 
                     oneOf( '@ & at and',caseless=True) +
                     streetReference.setResultsName("street") )
    streetAddress = ( poBoxRef("street")
                      ^ direct.setResultsName("street")
                      ^ streetReference.setResultsName("street")
                      ^ intersection )

    # how to add Apt, Suite, etc.
    unit_types = ' '.join(UNIT_TYPES.keys()+UNIT_TYPES.values())+' #'
    suiteRef = (
                oneOf(unit_types, caseless=True) + 
                Optional(".") + 
                Word(alphanums+'-')("suitenumber"))
    streetAddress = streetAddress + Optional(Optional(Suppress(',')) +
                                             suiteRef("suite"))

    return streetAddress

def create_address_parser():
    streetAddress = get_street_parser()
    cityRef = Combine(OneOrMore(Word(alphas)), joinString=' ', adjacent=False).setResultsName('city')
    stateRef = (Regex(r'[A-Z]{2}') ^ OneOrMore(Word(alphas))).setResultsName('state')
    cityStateRef = cityRef + Optional(Suppress(',')) + stateRef
    zipRef = Combine(Word(nums).setResultsName('zipcode')+Optional(Suppress('-')+Word(nums).setResultsName('plus4')), joinString='-', adjacent=False).setResultsName('fullzip')
    fullAddress = streetAddress + cityStateRef + Optional(zipRef)
    return fullAddress

__parsers = dict()
def get_address_parser():
    if 'address' not in __parsers:
        __parsers['address'] = create_address_parser()
    return __parsers['address']

def get_street_parser():
    if 'street' not in __parsers:
        __parsers['street'] = create_street_parser()
    return __parsers['street']

def extract_street_info(xaddr, daddr):
    if xaddr.street:
        #print xaddr.dump()
        daddr['address'] = ' '.join(ucfirst(x.strip()) for x in xaddr.street)
        if xaddr.suite:
            daddr['address'] += ' '+ucfirst(' '.join(xaddr.suite))
        if xaddr.boxnumber:
            daddr['box'] = xaddr.boxnumber
        else:
            if xaddr.street.number:
                daddr['number'] = xaddr.street.number.strip().upper()
            daddr['street'] = ucfirst(xaddr.street.name)
            daddr['street_name'] = ucfirst(xaddr.street.name)
            if xaddr.street.direction:
                daddr['street'] = '%s %s' % (xaddr.street.direction.upper(), daddr['street'])
            if xaddr.street.type:
                daddr['street'] += ' '+STREET_TYPES.get(xaddr.street.type.lower(), ucfirst(xaddr.street.type))
            if xaddr.street.quadrant:
                daddr['street'] += ' '+' '.join(xaddr.street.quadrant).upper()
            if xaddr.crossStreet:
                daddr['intersection'] = xaddr.crossStreet.name
                if xaddr.crossStreet.type:
                    daddr['intersection'] += ' '+STREET_TYPES.get(xaddr.crossStreet.type.lower(), ucfirst(xaddr.crossStreet.type))
            if xaddr.suite:
                daddr['unit'] = ucfirst(' '.join(xaddr.suite))

def parse_address(addr):
    p = get_address_parser()
    xaddr = p.parseString(addr, parseAll=True)
    daddr = {
        'string': addr,
    }
    extract_street_info(xaddr, daddr)
    daddr['city'] = ucfirst(xaddr.city)
    daddr['state'] = xaddr.state.upper()
    if xaddr.fullzip:
        daddr['zipcode'] = xaddr.fullzip.zipcode
        if xaddr.fullzip.plus4:
            daddr['plus4'] = xaddr.fullzip.plus4
    return daddr

def parse_street(addr):
    p = get_street_parser()
    xaddr = p.parseString(addr, parseAll=True)
    daddr = {
        'string': addr,
    }
    extract_street_info(xaddr, daddr)
    return daddr

if '__main__' == __name__:
    import sys, json
    print json.dumps(parse_address(' '.join(sys.argv[1:])), indent=2, sort_keys=True)

