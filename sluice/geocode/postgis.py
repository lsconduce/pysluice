import decimal
from sluice.dbwrapper import DBWrapper

db = DBWrapper(settings='postgis-settings.protein', engine='postgres')

def __format_postgis_results(sql, *args):
    results = list()
    cur = db.query(sql, *args)
    for row in cur:
        res = dict()
        for k, v in row.iteritems():
            if isinstance(v, decimal.Decimal):
                v = float(v)
            res[k.strip('_')] = v
        if res['number'] is not None:
            res['number'] = str(res['number'])
        res['street'] = ' '.join(str(x) for x in filter(None, [res['direction'], res['street_name'], res['type'], res['quadrant']]))
        res['address'] = ' '.join(str(x) for x in filter(None, [res['number'], res['direction'], res['street_name'], res['type'], res['quadrant'], res['unit']]))
        results.append(res)
    return results

def postgis_lookup(addr):
    sql = """
SELECT g.rating, ST_X(g.geomout) As lng, ST_Y(g.geomout) As lat, 
    (addy).address As number_,
    (addy).predirAbbrev AS direction,
    (addy).streetname As street_name, 
    (addy).streettypeabbrev As type_,
    (addy).postdirAbbrev AS quadrant,
    (addy).internal AS unit_,
    (addy).location As city,
    (addy).stateabbrev As state_,
    (addy).zip AS zipcode
    FROM geocode(%s) As g; 
"""
    return __format_postgis_results(sql, addr)

def postgis_reverse_lookup(lat, lng):
    sql = """
SELECT r.addy[1].address AS number_,
    r.addy[1].predirAbbrev AS direction,
    r.addy[1].streetname AS street_name,
    r.addy[1].streettypeabbrev AS type_,
    r.addy[1].postdirAbbrev AS quadrant,
    r.addy[1].internal AS unit_,
    r.addy[1].location as city,
    r.addy[1].stateabbrev AS state_,
    r.addy[1].zip AS zipcode,
    r.street[1] AS cross_street,
    %s AS lat,
    %s AS lng
    FROM reverse_geocode(ST_GeomFromText(%s, 4269)) As r
"""
    point = 'POINT(%s %s)' % (lng, lat)
    return __format_postgis_results(sql, lat, lng, point)

if '__main__' == __name__:
    import sys, json
    print json.dumps(postgis_lookup(' '.join(sys.argv[1:])), indent=2, sort_keys=True)

