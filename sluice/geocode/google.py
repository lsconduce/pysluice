import urllib, urllib2, json

GOOGLE_GEOCODE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?%s'
GOOGLE_API_KEY = None

def __google_lookup(**kwargs):
    ## possible to replace this with http://www.datasciencetoolkit.org/
    args = { 'sensor': 'false' }
    args.update(kwargs)
    url = GOOGLE_GEOCODE_URL % urllib.urlencode(args)
    data = json.loads(urllib2.urlopen(url).read())
    if data is None:
        raise Exception('No data for %s' % addr)
    if 0 == len(data):
        return None
    if data.get('status') == 'OVER_QUERY_LIMIT':
        raise LookupError
    if data.get('status') == 'ZERO_RESULTS':
        return None
    addr = { 'raw': data }
    for comp in data['results'][0]['address_components']:
        if 'street_number' in comp['types']:
            addr['number'] = comp['long_name']
        elif 'route' in comp['types']:
            addr['street'] = comp['short_name']
        elif 'locality' in comp['types']:
            addr['city'] = comp['long_name']
        elif 'administrative_area_level_2' in comp['types']:
            addr['county'] = comp['long_name']
        elif 'administrative_area_level_1' in comp['types']:
            addr['state'] = comp['short_name']
        elif 'country' in comp['types']:
            addr['country'] = comp['long_name']
        elif 'postal_code' in comp['types']:
            addr['zipcode'] = comp['long_name']
    addr['address'] = u' '.join(unicode(x) for x in filter(None, (addr.get('number'), addr.get('street'))))
    addr['lat'] = data['results'][0]['geometry']['location']['lat']
    addr['lng'] = data['results'][0]['geometry']['location']['lng']
    return addr

def google_lookup(addr):
    return __google_lookup(address=addr)

def google_reverse_lookup(lat, lng):
    return __google_lookup(latlng='%s,%s' % (lat, lng))

if '__main__' == __name__:
    import sys
    print json.dumps(google_lookup(' '.join(sys.argv[1:])), indent=2, sort_keys=True)

