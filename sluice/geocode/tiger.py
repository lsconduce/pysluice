import os, re
from copy import deepcopy
from sluice.api import Sluice
from sluice.dbwrapper import DBWrapper
from sluice.geometry import Location, Edge
from sluice.shapefile import ZipReader
from sluice.geocode.address_parser import parse_address

db = DBWrapper(db='tiger')
tiger = Sluice.get_share_file('tiger')

def tiger_lookup(addr, fuzzy=False):
    if isinstance(addr, basestring):
        addr = parse_address(addr)
    raddr = deepcopy(addr)
    if 'address' not in addr:
        raddr.update(zipcode_lookup(addr['zipcode']))
        return raddr
    if 'street_name' not in addr:
        daddr = parse_address(addr['address'])
        raddr.update(daddr)
        num = int(re.sub(r'\D', '', daddr['number']))
        if fuzzy:
            street = daddr['street_name']
        else:
            street = daddr['street']
    else:
        num = int(re.sub(r'\D', '', addr['number']))
        if fuzzy:
            street = addr['street_name']
        else:
            street = addr['street']
    if fuzzy:
        sql = "SELECT fips, record FROM line WHERE (zipcode_left = %s OR zipcode_right = %s) AND street LIKE %s AND start <= %s and end >= %s"
        args = (addr['zipcode'], addr['zipcode'], '%%%s%%' % street)
    else:
        sql = "SELECT fips, record FROM line WHERE (zipcode_left = %s OR zipcode_right = %s) AND street = %s AND start <= %s and end >= %s"
        args = (addr['zipcode'], addr['zipcode'], street, num, num)
    last_fips = None
    sf = None
    for row in db.query(sql, *args):
        if last_fips != row['fips']:
            sf = ZipReader(os.path.join(tiger, 'tl_2013_%05d_addrfeat.zip' % row['fips']))
            last_fips = row['fips']
        fields = list(x[0] for x in sf.fields[1:])
        rec = dict(zip(fields, sf.record(row['record'])))
        shp = sf.shape(row['record'])
        for side in 'LR':
            loc = side_match(addr['zipcode'], num, rec, shp, side)
            if loc:
                raddr['lat'] = loc[0]
                raddr['lng'] = loc[1]
                return raddr
    if not fuzzy:
        return tiger_lookup(raddr, True)
    return raddr

def get_numeric(txt):
    if txt is None:
        return None
    try:
        return int(txt)
    except ValueError:
        return int(re.sub(r'[^\d\.\-]', '', txt))

def side_match(zipcode, num, rec, shp, side):
    if rec['ZIP'+side] != zipcode:
        #print 'wrong zip'
        return None
    if (rec['PARITY'+side] == 'E' and num % 2 == 1) or (rec['PARITY'+side] == 'O' and num % 2 == 0):
        #print 'wrong side (%s / %d)' % (rec['PARITY'+side], (num % 2))
        return None
    nums = filter(None, [get_numeric(rec[side+'FROMHN']), get_numeric(rec[side+'TOHN'])])
    if len(nums) == 2 and nums[0] != nums[1]:
        ratio = (num - min(nums)) / float(max(nums) - min(nums))
    elif (len(nums) == 2 and nums[0] == nums[1]) or len(nums) == 1:
        if nums[0] == num:
            ratio = 0
        else:
            #print 'not in range'
            return None
    else:
        #print 'no nums'
        return None
    edge = Edge.from_shapefile(shp.points[0], shp.points[-1])
    ## interpolate
    r = (edge * ratio).q
    ## get out of the middle of the street
    bearing = edge.bearing()
    if side == 'L':
        bearing -= 90
        if bearing < -180:
            bearing += 360
    else:
        bearing += 90
        if bearing > 180:
            bearing -= 360
    return r.destination(bearing, 0.020)

if '__main__' == __name__:
    import sys, json
    print json.dumps(tiger_lookup(' '.join(sys.argv[1:])), indent=2, sort_keys=True)

