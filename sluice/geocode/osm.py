import urllib, urllib2, json

OSM_GEOCODE_URL = 'http://open.mapquestapi.com/nominatim/v1/search?%s'

def osm_lookup(addr):
    args = {
        'format': 'json',
        'addressdetails': 1,
        'q': addr
    }
    url = OSM_GEOCODE_URL % urllib.urlencode(args)
    data = json.loads(urllib2.urlopen(url).read())
    if data is None:
        raise Exception('No data for %s' % addr)
    if 0 == len(data):
        return None
    addr = { 'raw': data }
    addr.update(data[0]['address'])
    addr['number'] = addr.pop('house_number', None)
    addr['street'] = addr.pop('road', None)
    addr['address'] = u' '.join(unicode(x) for x in filter(None, (addr['number'], addr['street'])))
    addr['city'] = addr.pop('city', None)
    addr['county'] = addr.pop('county', None)
    addr['state'] = addr.pop('state', None)
    addr['zipcode'] = addr.pop('postcode', None)
    addr['country'] = addr.pop('country', None)
    addr['country_code'] = addr.pop('country_code', None)
    addr['lat'] = float(data[0]['lat'])
    addr['lng'] = float(data[0]['lon'])
    return addr

def osm_reverse_lookup(lat, lng):
    return osm_lookup('%s,%s' % (lat, lng))

if '__main__' == __name__:
    import sys
    print json.dumps(osm_lookup(' '.join(sys.argv[1:])), indent=2, sort_keys=True)

