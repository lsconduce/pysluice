import json
from sluice.api import Sluice
from sluice.geometry import Location, Edge, make_location

zipcodes = None
def get_zipcodes():
    global zipcodes
    if zipcodes is None:
        fn = Sluice.get_share_file('zipcodes.json')
        zipcodes = json.load(file(fn))
    return zipcodes

def zipcode_lookup(zipcode):
    return get_zipcodes().get(str(zipcode))

## not an exact lookup, but good enough for most purposes
def zipcode_reverse_lookup(lat, lng):
    loc = Location(lat, lng)
    zipcodes = get_zipcodes()
    data = sorted(zipcodes.values(), key=lambda x: Edge(loc, make_location(x)).distance())
    return data[0]

def nearby(lat, lng, distance=50):
    zipcodes = get_zipcodes()
    data = list(x for x in zipcodes.values() if Edge(loc, make_location(x)).distance() <= distance)
    return data

if '__main__' == __name__:
    import sys
    print json.dumps(zipcode_lookup(sys.argv[1]), indent=2, sort_keys=True)

