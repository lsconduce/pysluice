"""
Use lat_lon_lookup(addr)

This will try OSM first, which does not have a limit API calls.  If that fails
try using Google using a set of API keys.
"""
import dbm
import cPickle as pickle
from datetime import datetime
import unicodedata

from sluice.geocode import google, osm

class DoneForNow(Exception):
    """
    Run out of Google API keys
    """
    pass

CACHES = {}
FAILED_GOOGLE_KEYS = {}
GOOGLE_KEYS = set([
    None,
    'AIzaSyByolaM8l2clFXLvBhaIRE3daYbFwK8l4Y',
    'AIzaSyAlZWYqyiQEa-FCgJ9Fo9b_GuceHR6yZVw',
    'AIzaSyAjuXgaxGocib54wjFz5AXyQ2sAGltgbsE',
])

def _sanitize_address(address):
    """
    return an ascii string
    """
    if isinstance(address, str):
        #won't have any weird characters, so just return
        return address
    try:
        #try to use pythons string conversion, this will work most of the time
        return str(address)
    except (UnicodeDecodeError, UnicodeEncodeError):
        #strip accents
        #http://stackoverflow.com/questions/517923/what-is-the-best-way-to-remove-accents-in-a-python-unicode-string
        return ''.join(c for c in unicodedata.normalize('NFD', address)
                       if unicodedata.category(c) != 'Mn')

def _google_lookup(address):
    for api_key in GOOGLE_KEYS:
        if FAILED_GOOGLE_KEYS.get(api_key):
            #This key was recently bad, skip it
            #It takes a long time to come back, so don't even bother doing
            #some fancy tricks with time
            continue
        try:
            kwargs = {'address': address}
            if api_key:
                kwargs['key'] = api_key
            #I'm not sure why __ works here, maybe b/c it's a module?
            res = google.__google_lookup(**kwargs)
            #return res even if it is None, it means google cannot parse/locate
            #the provided address
            return res
        except LookupError as exc:
            #google lookups exhausted for this key
            FAILED_GOOGLE_KEYS[api_key] = datetime.now()
    #Got through all keys without getting a good response from google
    raise DoneForNow('All keys exhausted')

def _geo_lookup(address):
    """
    Try using OSM & then Google.  OSM does not limit API calls, but it also
    does not have as much data.  So OSM lookups sometimes return None because
    they cannot locate a valid address.  Google, on the other hand, has TONS
    of data, but limits calls.  If we cannot find something with Google,
    assume the address is not valid.
    """
    try:
        res = osm.osm_lookup(address)
    except IndexError as exc:
        res = None
    if res is not None:
        return res

    #not found with OSM, try google
    return _google_lookup(address)


class StrMemo(object):
    """
    This caches returns from f
    """
    def __init__(self, db_filename, f):
        self.__db = dbm.open(db_filename, 'c')
        self.__f = f

    def __call__(self, arg):
        if not self.__db.has_key(arg):
            self.__db.setdefault(arg, pickle.dumps(self.__f(arg)))
        return pickle.loads(self.__db.get(arg))

    def force_google(self, arg):
        """
        Google has better results, force it sometimes
        """
        #google adds the 'status' key, so check if that is there or not
        if self.__db.has_key(arg):
            res = pickle.loads(self.__db.get(arg))
            if res and 'status' in res.get('raw', {}):
                return res
        #not google, force getting the key
        res = _google_lookup(arg)
        if self.__db.has_key(arg):
            del self.__db[arg]
        self.__db.setdefault(arg, pickle.dumps(res))
        return res


def lat_lon_lookup(address, cache_file=None, force_google=False):
    """
    cache file should be a string, it will create a file <file>.db

    This will look up an address using OSM and maybe Google.  If a cache_file
    (path) is provided - this will act as a cache for addresses.  Addresses
    that are in the file will be returned without hitting OSM/Google, and
    cache-misses will go to OSM/Google and add the results to the cache.
    """
    #remove non-ascii characters since they act funny on http calls
    address = _sanitize_address(address)
    if cache_file is not None:
        #Use the cache file provided
        if cache_file not in CACHES:
            CACHES[cache_file] = StrMemo(cache_file, _geo_lookup)
        geo_lookup_cache = CACHES[cache_file]
        if force_google:
            return geo_lookup_cache.force_google(address)
        else:
            return geo_lookup_cache(address)
    if force_google:
        return _google_lookup(address)
    else:
        return _geo_lookup(address)

