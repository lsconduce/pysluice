import math
from sluice.geometry import D2R, Point, Location, Box

def norm2lat(norm):
    y = norm + 0.5
    if y > 1.0:
        y = 1.0
    elif y < 0.0:
        y = 0.0
    rlat = math.atan(math.sinh(((2.0 * y) - 1.0) * math.pi))
    return rlat / D2R

def norm2lon(norm):
    x = norm + 0.5
    if x > 1.0:
        x = 1.0
    elif x < 0.0:
        x = 0.0
    return (2.0 * x - 1.0) * 180.0

MAX_VALID_LAT = norm2lat(1.0)
MAX_VALID_LON = 180.0

def lat2norm(lat):
    rlat = D2R*clamp_lat(lat)
    return (1.0 + math.log((math.sin(rlat) + 1.0) / math.cos(rlat)) / math.pi) / 2.0 - 0.5

def lon2norm(lon):
    return (clamp_lon(lon) / 180.0) / 2.0

def clamp_lat(lat):
    if lat > MAX_VALID_LAT:
        return MAX_VALID_LAT
    if lat < -1 * MAX_VALID_LAT:
        return -1 * MAX_VALID_LAT
    return lat

def clamp_lon(lon):
    if lon > MAX_VALID_LON:
        return MAX_VALID_LON
    if lon < -1 * MAX_VALID_LON:
        return -1 * MAX_VALID_LON
    return lon

def loc2norm(loc):
    return Point(lon2norm(loc.lon), lat2norm(loc.lat))

def norm2loc(norm):
    return Location(norm2lat(norm.y), norm2lon(norm.x))

class Mercator(Box):

    __slots__ = ('bl', 'tr', 'w', 'h', 'l0', 'y0', 'R')

    def __init__(self, bl=None, tr=None, w=1600):
        if bl is None:
            bl = Location(-1 * MAX_VALID_LAT, -1 * MAX_VALID_LON)
        if tr is None:
            tr = Location(MAX_VALID_LAT, MAX_VALID_LON)
        self.w = w
        self.setup(bl, tr, w)

    def location(self, l):
        if isinstance(l, Location):
            return l
        if isinstance(l, (list, tuple)):
            return Location(*l)
        if isinstance(l, dict):
            return Location(**l)
        raise ValueError("%s can't be converted to a Location" % l)

    def point(self, p):
        if isinstance(p, Point):
            return p
        if isinstance(p, (list, tuple)):
            return Point(*p)
        if isinstance(p, dict):
            return Point(**p)
        raise ValueError("%s can't be converted to a Point" % p)

    def setup(self, bl=None, tr=None, w=None):
        if bl is not None:
            self.bl = self.location(bl)
        if tr is not None:
            self.tr = self.location(tr)
        if self.w is not None:
            self.w = w
        self.nbl = loc2norm(self.bl)
        self.ntr = loc2norm(self.tr)
        self.nw = self.ntr.x - self.nbl.x
        self.nh = self.ntr.y - self.nbl.y
        self.scale = self.w / self.nw
        self.h = self.nh * self.scale
        #self.l0 = D2R * (self.bl.lon + self.tr.lon) / 2.0
        #self.R = self.w / (D2R * (self.tr.lon - self.bl.lon))
        #self.y0 = self.R * math.log(math.tan((math.pi/4) + (D2R*self.tr.lat/2)))
        #self.h = self.project((self.bl.lat, self.bl.lon))[1]

    def project(self, latlon):
        l = self.location(latlon)
        ##x = (self.R * (D2R*l.lon - self.l0)) - (self.w/2.0)
        #x = (self.w/2.0) + (self.R * (D2R*l.lon - self.l0))
        #y = self.y0 - self.R * math.log(math.tan((math.pi/4) + (D2R*l.lat/2)))
        #return Point(x, y)
        norm = loc2norm(latlon)
        x = norm.x - self.nbl.x
        y = self.ntr.y - norm.y
        return Point(x * self.scale, y * self.scale)

    def unproject(self, xy):
        p = self.point(xy)
        #lon = ((self.w/2.0 - (self.w - p.x)) / self.R + self.l0) / D2R
        #lat = 2 * (math.atan2(math.exp((self.y0 - p.y)/self.R), 1) - math.pi/4) / D2R
        #return Location(lat, lon)
        nx = self.nbl.x + (xy.x / self.scale)
        ny = self.ntr.y - (xy.y / self.scale)
        norm = Point(nx, ny)
        return norm2loc(norm)

    def lat2y(self, lat):
        return math.log(math.tan(math.pi/4+lat*D2R/2))/D2R

