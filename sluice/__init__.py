#!/usr/bin/env python

PYSLUICE_VERSION = '1.1.8'

APP = 'sluice'
PROTO = 'prot-spec v1.0'
UPDATE_LAG = 0.25
HEARTBEAT_POOL        = 'sluice-to-heart'
EDGE_REQUEST_POOL     = 'edge-to-sluice'
EDGE_RESPONSE_POOL    = 'sluice-to-edge'
NETWORK_REQUEST_POOL  = 'topo-to-sluice'
NETWORK_RESPONSE_POOL = 'sluice-to-topo'
OBSERVATION_POOL      = 'topo-to-sluice'
TEXTURE_REQUEST_POOL  = 'fluoro-to-sluice'
TEXTURE_RESPONSE_POOL = 'sluice-to-fluoro'


#keep this for now to keep things as backwards compatible as possible
import warnings

try:
    from sluice.api import Sluice as RealSluice
    class Sluice(RealSluice):
        def __init__(self, *args, **kwargs):
            warnings.warn("sluice.Sluice is deprecated, use sluice.api.Sluice", DeprecationWarning)
            super(Sluice, self).__init__(*args, **kwargs)
except ImportError:
    #make a sluice class that cannot be used
    class Sluice(object):
        def __init__(self, *args, **kwargs):
            raise ImportError('Use sluice.api.Sluice class')


