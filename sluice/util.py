def ucfirst(txt):
    if txt is None:
        return None
    words = list()
    for word in txt.split():
        if len(word) == 2 and word == word.upper():
            words.append(word)
        else:
            words.append('%s%s' % (word[0].upper(), word[1:].lower()))
    return ' '.join(words)
    #return ' '.join('%s%s' % (x[0].upper(), x[1:].lower()) for x in txt.split())

