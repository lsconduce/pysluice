import time, random
from sluice.types import v3float64
from plasma import Protein
from plasma import Hose
import sluice

APP = 'emcs'
PROTO = 'prot-2.0'

class Store(object):
    def __init__(self, pool_host='localhost'):
        self.pool_host = pool_host
        self.hose = Hose.participate('tcp://%s/store-req' % self.pool_host)

    def request(self, bl=[-90, -180], tr=[90, 180], curtime=None, types=[], attrs=[], pool='sluice-to-fluoro', descrips=[], template={}):
        if curtime is None:
            curtime = time.time()
        p = Protein([APP, PROTO, 'data', 'pass'], {
            'bl': v3float64(bl[0], bl[1], 0.0),
            'tr': v3float64(tr[0], tr[1], 0.0),
            'timestamp': curtime,
            'types': types,
            'attrs': attrs,
            'pool': pool,
            'descrips': descrips,
            'template': template
        })
        self.hose.deposit(p)

    def texture_request(self, bl, tr, curtime, fluoro):
        pool = sluice.TEXTURE_RESPONSE_POOL
        f_id = 0
        qid = fluoro.get('SlawedQID', [])
        for i, x in enumerate(qid):
            if i >= 6:
                break
            f_id = (f_id << 8) + x
        descrips = [sluice.APP, sluice.PROTO, 'request', 'texture', fluoro.get('SlawedQID'), fluoro.get('daemon')]
        template = {
            'bl': bl,
            'tr': tr,
            'name': 'image-%s' % hex(f_id),
            'time': curtime,
        }
        if abs(curtime - time.time()) <= 1.0:
            template['live'] = True
        for attr in fluoro.get('attributes', []):
            name = attr.get('name', '')
            if attr.get('selection-type', '') == 'inclusive':
                vals = list()
                for v in attr.get('contents', []):
                    if v.get('selected'):
                        vals.append(v.get('name', ''))
                if len(vals) > 0:
                    template[name] = vals
            else:
                for v in attr.get('contents', []):
                    if v.get('selected'):
                        template[name] = v.get('name', '')
                        break
        types = template.pop('entity-kinds', None)
        if types is not None:
            if not isinstance(types, list):
                types = [types,]
        else:
            types = []
        attrs = template.pop('entity-attributes', None)
        if attrs is not None:
            if not isinstance(attrs, list):
                attrs = [attrs,]
        else:
            attrs = []
        if 'style' in template:
            template['styles'] = template.pop('style')
        if 'styles' in template:
            template['style'] = ','.join(str(x) for x in template['styles'])
        return self.request(bl, tr, curtime, types, attrs, pool, descrips, template)

    def subscribe(self, ident, bl=[-90, -180], tr=[90, 180], curtime=None, rate=1.0, types=[], attrs=[], pool='sluice-pubsub', descrips=[]):
        if curtime is None:
            curtime = time.time()
        p = Protein([APP, PROTO, 'pubsub', 'add'], {
            'ident': ident,
            'bounds': {
                'bl': v3float64(bl[0], bl[1], 0.0),
                'tr': v3float64(tr[0], tr[1], 0.0),
            },
            'current-time': curtime,
            'time-rate': rate,
            'types': types,
            'attrs': attrs,
            'outpool': pool,
            'outdescr': descrips
        })
        self.hose.deposit(p)

    def unsubscribe(self, ident):
        p = Protein([APP, PROTO, 'pubsub', 'remove'], { 'ident': ident })
        self.hose.deposit(p)

    def resubscribe(self, ident, bl=None, tr=None, curtime=None, rate=None, types=None, attrs=None):
        ingests = { 'ident': ident }
        if bl is not None and tr is not None:
            ingests['bounds'] = {
                'bl': v3float64(bl[0], bl[1], 0.0),
                'tr': v3float64(tr[0], tr[1], 0.0)
            }
        if curtime is not None:
            ingests['current-time'] = curtime
        if rate is not None:
            ingests['time-rate'] = rate
        if types is not None:
            ingests['types'] = types
        if attrs is not None:
            ingests['attrs'] = attrs
        p = Protein([APP, PROTO, 'pubsub', 'update'], ingests)
        self.hose.deposit(p)

def gen_entities(kind, start, end):
    for i in xrange(start, end):
        yield {
            'kind': kind,
            'id': '%s-%s' % (kind, i),
            'timestamp': float(i - start),
            'attrs': {
                'name': '%s-%s' % (kind, i*i),
                'realtime': str(time.time())
            },
            'loc': v3float64(min(90, max(-90, random.gauss(0, 30))), min(180, max(-180, random.gauss(0, 60))), 0.0),
        }

if '__main__' == __name__:
    import sluice.api
    s = sluice.api.Sluice()
    print 'inject foo'
    s.inject_topology(list(gen_entities('foo', 0, 10)))
    print 'inject bar'
    s.inject_topology(list(gen_entities('bar', 12, 37)))
    st = Store()
    print 'request foo'
    st.request(types=['foo'], pool='sluice-test', descrips=['request', 'one'], template={ 'config': 'data' })
    time.sleep(2.0)
    print 'subscribe bar'
    st.subscribe('testsub', curtime=0.0, types=[], pool='sluice-test', descrips=['publish', 'one'])
    time.sleep(10.0)
    print 'unsubscribe bar'
    st.unsubscribe('testsub')
    print 'finished'

