import re

url_pat = re.compile(r'((www\.[\s]+)|(https?://[^\s]+))')
at_user_pat = re.compile(r'@[^\s]+') 
hashtag_pat = re.compile(r'#([^\s]+)')
whitespace_pat = re.compile(r'\s+')
doubles_pat = re.compile(r"(.)\1{1,}", re.DOTALL)
punct_pat = re.compile(r'["\?!;:\+\<\>,\._/\&\(\)\{\}\[\]\-]')
junk_pat = re.compile(r'[^A-Za-z0-9@#\s]')
word_pat = re.compile(r'^[A-Za-z][a-zA-Z0-9]*$')

def tweet_preprocessor(tweet):
    tweet = tweet.lower()
    ##Convert www.* or https?://* to URL
    #tweet = url_pat.sub('URL', tweet)
    #Remove URLs
    tweet = url_pat.sub('', tweet)
    ##Convert @username to AT_USER
    #tweet = at_user_pat.sub('AT_USER', tweet)
    #Remove user mentions
    tweet = at_user_pat.sub('', tweet)
    #Replace #word with two occurrences of word
    tweet = hashtag_pat.sub(r'\1 \1', tweet)
    #Replace punctuation with whitespace
    tweet = punct_pat.sub(' ', tweet)
    #trim quotes, punctuation
    tweet = tweet.strip("'")
    #remove non alphanumerics
    tweet = junk_pat.sub('', tweet)
    #Remove additional white spaces
    tweet = whitespace_pat.sub(' ', tweet)
    #look for 2 or more repetitions of character and replace with the character itself  
    tweet = doubles_pat.sub(r"\1\1", tweet)
    words = list()
    for word in tweet.split():
        #if word in stopwords:
        #    continue
        if word_pat.match(word):
            words.append(word)
    return ' '.join(words)

