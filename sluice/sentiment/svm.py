import sys, os
import cPickle as pickle
import numpy
import scipy.sparse
from sklearn import svm
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cross_validation import KFold, cross_val_score, train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sluice.sentiment.tweet import tweet_preprocessor

class SVM(object):

    def __init__(self, preprocessor=tweet_preprocessor, **kwargs):
        self.__preprocessor = preprocessor
        self.__vectorizer = TfidfVectorizer(min_df=1)
        self.__svm = svm.SVC(**kwargs)
        self.__class_dictionary = dict()
        self.__classes = list()
        self.__class_index = 0
        self.__training_data = dict()
        self.__training_features = None
        self.__training_classes = None

    def preprocess(self, doc):
        return self.__preprocessor(doc)

    #def vectorize(self, document, fixed=False):
    #    features = set()
    #    for token in self.__tokenizer(document):
    #        if token not in self.__dictionary:
    #            if fixed:
    #                continue
    #            self.__dictionary[token] = self.__dictionary_index
    #            self.__dictionary_index += 1
    #        features.add(self.__dictionary[token])
    #    return features

    def learn(self, classname, doc_id, document):
        if classname not in self.__class_dictionary:
            self.__classes.append(classname)
            self.__class_dictionary[classname] = self.__class_index
            self.__class_index += 1
        idx = self.__class_dictionary[classname]
        self.__training_data[doc_id] = (idx, document)

    def make_sparse(self, features):
        n = 0
        data = list()
        row = list()
        col = list()
        for f in features:
            for x in f:
                data.append(1)
                row.append(n)
                col.append(x)
            n += 1
        m = self.__dictionary_index
        return scipy.sparse.csr_matrix((data,(row,col)), shape=(n, m), dtype=numpy.float64)

    def get_training_features(self):
        for td in self.__training_data:
            for doc in td:
                yield doc

    def retrain(self):
        keys = self.__training_data.keys()
        classes = list(self.__training_data[x][0] for x in keys)
        corpus = list(self.preprocess(self.__training_data[x][1]) for x in keys)
        self.__training_features = self.__vectorizer.fit_transform(corpus)
        self.__training_classes = numpy.array(classes)
        self.__svm.fit(self.__training_features, self.__training_classes)

    def tune(self):
        tuned_parameters = [
            { 'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1,10,100,1000] },
            { 'kernel': ['linear'], 'C': [1,10,100,1000] }
        ]
        scores = ['precision', 'recall']
        X_train, X_test, y_train, y_test = train_test_split(self.__training_features, self.__training_classes, test_size=0.5, random_state=0)
        for score in scores:
            clf = GridSearchCV(svm.SVC(), tuned_parameters, cv=5, scoring=score)
            clf.fit(X_train, y_train)
            print 'best params set found on development set:'
            print clf.best_estimator_
            print 'grid scores on development set:'
            for params, mean_score, xscores in clf.grid_scores_:
                print '%0.3f (+/-%0.03f for %s' % (mean_score, xscores.std() / 2, params)
            print ''
            print 'detailed classification report:'
            y_true, y_pred = y_test, clf.predict(X_test)
            print classification_report(y_true, y_pred)
            print ''

    def validate(self, folds=5):
        kf = KFold(len(self.__training_classes), n_folds=folds, indices=True)
        scores = cross_val_score(self.__svm, self.__training_features, self.__training_classes, cv=kf)
        return (scores.mean(), scores.std() * 2, len(self.__training_data))

    def classify(self, document):
        features = self.__vectorizer.transform([self.preprocess(document)])
        #features = self.make_sparse([self.vectorize(document, True)])
        return self.__classes[self.__svm.predict(features)[0]]

    def save(self, fp=None):
        if fp is None:
            return pickle.dumps(self)
        if isinstance(fp, basestring):
            dn = os.path.dirname(fp)
            if dn and not os.path.exists(dn):
                os.makedirs(dn)
            fp = file(fp, 'w')
        pickle.dump(self, fp)

    @classmethod
    def load(cls, fp):
        if isinstance(fp, basestring):
            if len(fp) <= 255 and os.path.exists(fp):
                fp = file(fp)
            else:
                return pickle.loads(fp)
        return pickle.load(fp)

