import sys, os, re
import sluice
from sluice.sentiment.tweet import tweet_preprocessor

class AFINN(object):
    WORDS = None
    TOKENIZER = re.compile(r'(([ \t.,;\']+)|^|$)')

    def __init__(self, preprocessor=tweet_preprocessor):
        self.__preprocessor = preprocessor
        self.load_words()

    def load_words(self):
        if self.__class__.WORDS is None:
            fn = sluice.Sluice.get_share_file('AFINN-111.txt')
            words = dict()
            for line in file(fn):
                word, score = line.strip().split('\t')
                words[word] = int(score)
            self.__class__.WORDS = words

    def preprocess(self, txt):
        if self.__preprocessor is None:
            return txt
        return self.__preprocessor(txt)

    def classify(self, txt):
        n = 0
        m = 0
        words = self.TOKENIZER.split(self.preprocess(txt))
        for i, word in enumerate(words):
            if word in self.WORDS:
                n += self.WORDS[word]
                m += 1
            if i >= 1:
                word2 = ' '.join((words[i-1], word))
                if word2 in self.WORDS:
                    n += self.WORDS[word2]
                    m += 1
                if i >= 2:
                    word3 = ' '.join((words[i-2], words[i-1], word))
                    if word3 in self.WORDS:
                        n += self.WORDS[word3]
                        m += 1
        if m > 0:
            v = float(n) / float(m)
            if v <= -2:
                return 'Negative'
            elif v >= 2:
                return 'Positive'
            else:
                return 'Neutral'
        return 'Neutral'
        return 0.0

