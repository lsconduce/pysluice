"""
Make yaml io from plasma available from sluice
"""

from plasma.yamlio import (dump_yaml_protein, dump_yaml_proteins,
    parse_yaml_protein, parse_yaml_proteins, parse_yaml_slaw, dump_yaml)

