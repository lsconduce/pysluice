from twitter.oauth import OAuth
from twitter.stream import TwitterStream
from plasma import Hose
from plasma import Protein
from plasma.exceptions import PoolAwaitTimedoutException
from plasma.const import *
from plasma.yamlio import parse_yaml_proteins
from sluice.api import Sluice
import json, traceback

class TwitterDaemon(object):
    def __init__(self, pool_host='localhost', **kwargs):
        conf = dict(self.__load_config())
        conf.update(kwargs)
        self.pool_host = pool_host
        self.__token = conf['token']
        self.__token_secret = conf['token_secret']
        self.__consumer_key = conf['consumer_key']
        self.__consumer_secret = conf['consumer_secret']
        if conf.get('follow') is None:
            conf['follow'] = []
        self.__follow = set(x.strip('@') for x in conf['follow'])
        if conf.get('hashtags') is None:
            conf['hashtags'] = []
        self.__hashtags = set(x.strip('#') for x in conf['hashtags'])
        if conf.get('search') is None:
            conf['search'] = []
        self.__search = set(x.strip() for x in conf['search'])
        if isinstance(conf.get('classifier'), basestring):
            if conf['classifier'] == 'afinn':
                import sluice.sentiment.afinn
                conf['classifier'] = sluice.sentiment.afinn.AFINN()
            else:
                import sluice.sentiment.svm
                fn = Sluice.get_var_file(conf['classifier'])
                if fn:
                    conf['classifier'] = sluice.sentiment.svm.SVM.load(file(fn))
                    conf['classifier'].save_path = fn
                else:
                    fn = Sluice.get_var_file(conf['classifier'], True)
                    conf['classifier'] = sluice.sentiment.svm.SVM()
                    conf['classifier'].save_path = fn
        self.classifier = conf.get('classifier')
        self.lang = conf.get('lang', 'en')
        self.geo_only = bool(conf.get('geo_only', 1))

    def __load_config(self):
        fn = Sluice.get_etc_file('twitter-settings.protein')
        if not fn:
            raise Exception("no twitter-settings.protein found")
        return parse_yaml_proteins(file(fn)).next().ingests()

    def follow(self, tweeter):
        self.__follow.add(tweeter.strip('@'))

    def unfollow(self, tweeter):
        self.__follow.discard(tweeter.strip('@'))

    def hashtag(self, tag):
        self.__hashtags.add(tag.strip('#'))

    def unhashtag(self, tag):
        self.__hashtags.discard(tag.strip('#'))

    def search(self, phrase):
        self.__search.add(phrase.strip())

    def unsearch(self, phrase):
        self.__search.discard(phrase.strip())

    def track(self):
        items = list('@'+x for x in self.__follow)
        items += list('#'+x for x in self.__hashtags)
        items += self.__search
        if len(items) == 0:
            raise ValueError("nothing to track")
        return ','.join(items)


    def get_stream(self):
        auth = OAuth(self.__token, self.__token_secret, self.__consumer_key, self.__consumer_secret)
        stream = TwitterStream(auth=auth, timeout=60.0)
        return stream

    def run(self):
        track = self.track()
        stream = self.get_stream()
        #hose = Hose.participate('tcp://%s/twitter' % self.pool_host)
        hose = Hose.participate_creatingly('tcp://%s/twitter', 'mmap', { 'size': 100*1024*1024 })
        descrips = ['twitter', 'tweet', 'live']
        stream_iter = stream.statuses.filter(track=track)
        try:
            for tweet in stream_iter:
                if tweet.get('timeout'):
                    continue
                if self.geo_only and not tweet.get('geo'):
                    continue
                if self.lang is not None:
                    lang = tweet.get('lang', 'en')
                    if isinstance(self.lang, basestring):
                        if lang != self.lang:
                            continue
                    else:
                        if lang not in self.lang:
                            continue
                if self.classifier:
                    try:
                        tweet['class'] = self.classifier.classify(tweet['text'])
                    except Exception as e:
                        traceback.print_exc()
                        print json.dumps(tweet, indent=2, sort_keys=True)
                hose.deposit(Protein(descrips, dict(tweet)))
                if self.classifier:
                    ## check for classifier training
                    d = ['twitter', 'tweet', 'train']
                    while True:
                        try:
                            p = hose.await_probe_frwd(d, timeout=POOL_NO_WAIT)
                            if p.matches(['retrain']):
                                self.classifier.retrain()
                                if self.classifier.save_path:
                                    self.classifier.save(self.classifier.save_path)
                                mean, std, samples = self.classifier.validate()
                                out = Protein(['classifier', 'stats'], { 'mean': float(mean), 'std': float(std), 'samples': samples })
                                hose.deposit(out)
                            elif p.matches(['learn']):
                                self.classifier.learn(ing['class'], ing['id'], ing['text'])
                                if self.classifier.save_path:
                                    self.classifier.save(self.classifier.save_path)
                        except PoolAwaitTimedoutException:
                            break
        except KeyboardInterrupt:
            stream_iter.handle.fp.close()
            hose.withdraw()

if '__main__' == __name__:
    import optparse
    op = optparse.OptionParser()
    op.add_option('-H', '--host', dest='pool_host', default='localhost')
    opts, args = op.parse_args()
    t = TwitterDaemon(opts.pool_host)
    t.run()

