import urlparse, time
from sluice.types import v3float64
from plasma import Protein
from sluice.api import Sluice
from sluice.settings.fluoro.base import FluoroBase

class WebFluoro(FluoroBase):

    def __init__(self, name=None):
        self.__name = None
        self.__url = None
        self.__loc = None
        self.__pin = None
        self.__size = None

    def get_type(self):
        return 'web'

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = str(name)

    def get_url(self):
        return self.__url

    def set_url(self, url):
        url = str(url)
        parsed = urlparse.urlparse(url)
        if not parsed.scheme:
            raise Exception("not a fully specified url")
        self.__url = url

    def get_pin(self):
        return self.__pin

    def set_pin(self, lat, lon):
        self.__pin = (float(lat), float(lon))

    def get_loc(self):
        return self.__loc

    def set_loc(self, x, y):
        self.__loc = (float(x), float(y))

    def get_size(self):
        return self.__size

    def set_size(self, w, h):
        self.__size = (float(w), float(h))

    def load_template(self):
        pass
        #raise Exception("web fluoroscope cannot be loaded as a template")

    def instantiate(self, bl=None, tr=None, config=None):
        s = Sluice()
        if self.__pin:
            ## need to create a fake topo entity to bind to
            ent = {
                'kind': 'webby-pin',
                'id': 'webby-pin',
                'loc': v3float64(self.__pin[0], self.__pin[1], 0.0)
            }
            s.inject_topology([ent])
            ## need a better way to detect that sluice is aware of an entity
            time.sleep(0.1)
            s.request_localized_webpage('webby-pin', self.get_url(), self.get_size())
            time.sleep(0.1)
            s.remove_topology(['webby-pin'])
        else:
            s.request_webpage(self.get_name(), self.get_url(), size=self.get_size(), loc=self.get_loc())

    def export(self):
        return None

    def export_config(self):
        d = {
            'name': self.get_name(),
            'type': self.get_type(),
            'url': self.get_url(),
            'size': self.get_size(),
        }
        if self.__pin:
            d['pin'] = self.__pin
        if self.__loc:
            d['loc'] = self.__loc
        return d


