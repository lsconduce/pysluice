from base import FluoroBase
from atlas import AtlasFluoro
from network import NetworkFluoro
from texture import TextureFluoro, DecoratedTextureFluoro
from video import VideoFluoro
from web import WebFluoro

class Fluoro(object):

    @staticmethod
    def from_config(id, f):
        t = f.get('type')
        if t == 'network':
            if 'text' in f:
                return NetworkFluoro(id).import_config(f)
            else:
                return NetworkFluoro(id).import_protein(f)
        if t == 'texture':
            if 'text' in f:
                return TextureFluoro(id).import_config(f)
            else:
                return TextureFluoro(id).import_protein(f)
        if t == 'video':
            if 'text' in f:
                return VideoFluoro(id).import_config(f)
            else:
                return VideoFluoro(id).import_protein(f)
        if t == 'atlas':
            if 'background' in f:
                return AtlasFluoro(id).import_config(f)
            else:
                return AtlasFluoro(id).import_protein(f)
        raise Exception("unknown fluoro type %s" % t)
