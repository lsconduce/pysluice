import json, os, sys, itertools
from plasma import Protein
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
from sluice.api import Sluice
from sluice.settings.fluoro.base import FluoroBase

class VideoFluoro(FluoroBase):

    def __init__(self, *args, **kwargs):
        super(VideoFluoro, self).__init__(*args, **kwargs)
        self.__daemon = None

    def load_from_protein(self, p):
        if isinstance(p, basestring):
            fn = Sluice.get_etc_file(p)
            if fn is None:
                raise Exception("can't find fluoroscope protein %s" % p)
            p = parse_yaml_protein(file(fn))
        elif not isinstance(p, Protein):
            raise Exception("not a protein")
        if p.ingests().get('type') != 'video':
            raise Exception("not a video fluoroscope")
        return self.import_protein(p.ingests())

    def import_protein(self, conf):
        super(VideoFluoro, self).import_protein(conf)
        self.set_daemon(conf.get('daemon'))
        return self

    def import_config(self, conf):
        super(VideoFluoro, self).import_config(conf)
        self.set_daemon(conf.get('daemon'))
        return self

    def get_type(self):
        return 'video'

    def set_daemon(self, name):
        self.__daemon = str(name)

    def get_daemon(self):
        return self.__daemon

    def export(self):
        d = super(VideoFluoro, self).export()
        d['daemon'] = self.get_daemon()
        return d

    def export_config(self):
        d = super(VideoFluoro, self).export_config()
        d['daemon'] = self.get_daemon()
        return d

