import os, re
from copy import deepcopy
from plasma import Protein
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
from sluice import APP, PROTO
from sluice.api import Sluice
from sluice.settings.attributes import Attributes, Attribute

class FluoroBase(object):

    def __init__(self, id, *args, **kwargs):
        self.__id = id
        self.__attributes = Attributes()
        self.__name = None
        self.__display = None
        self.__icon = None

    def get_demo_id(self):
        return self.__id

    def import_protein(self, conf):
        #TODO: Figure out why import_protein and import_config are different
        self.set_name(conf.get('name'))
        self.set_display(conf.get('display-text'))
        self.set_icon(conf.get('icon'))
        self.get_attributes().load(conf.get('attributes'))
        return self

    def import_config(self, conf):
        self.set_name(conf.get('name'))
        self.set_display(conf.get('text'))
        self.set_icon(conf.get('icon'))
        attrs = self.get_attributes()
        for k, v in conf.iteritems():
            if isinstance(v, dict) and 'options' in v:
                if k not in attrs:
                    attrs.add(k, v['options'], v['value'])
                else:
                    attr = attrs.describe(k)
                    for opt in v['options']:
                        attr.add_option(opt['value'], opt['text'])
                    attr.set_value(v['value'])
        return self

    def load_template(self):
        s = Sluice()
        s.create_fluoroscope_template(self.export())

    def instantiate(self, bl, tr, config=None):
        inst = deepcopy(self)
        if config is not None:
            inst.configure(config)
        s = Sluice()
        s.create_fluoroscope_instance(inst.export(), bl, tr)

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = str(name)

    def get_display(self):
        return self.__display

    def set_display(self, text):
        if text is None:
            self.__display = None
        else:
            self.__display = str(text)

    def get_icon(self):
        return self.__icon

    def set_icon(self, icon):
        if icon is None:
            self.__icon = None
        else:
            fn = Sluice.get_share_file(icon)
            if not fn:
                raise Exception("icon not found")
            self.__icon = icon

    def get_attributes(self):
        return self.__attributes

    def configure(self, *args, **kwargs):
        self.__attributes.update(*args, **kwargs)

    def export(self):
        d = {
            'name': self.get_name(),
            'display-text': self.get_display(),
            'type': self.get_type(),
            'attributes': self.get_attributes().export()
        }
        if self.get_icon() is not None:
            d['icon'] = self.get_icon()
        return d

    def export_config(self):
        d = self.get_attributes().export_config()
        d.update({
            'name': self.get_name(),
            'text': self.get_display(),
            'type': self.get_type(),
        })
        if self.get_icon() is not None:
            d['icon'] = self.get_icon()
        return d

    def get_file(self):
        key = '_'.join(self.get_name().lower().split())
        key = re.sub(r'[^a-z0-9]+', '_', key)
        return os.path.join(self.get_demo_id(), key+'.protein')

    def save(self, fn=None):
        if fn is None:
            fn = self.get_file()
        fn = Sluice.get_etc_file(fn, True)
        config = self.export()
        p = Protein([APP, PROTO, 'request', 'new-fluoro-template'], config)
        txt = dump_yaml_protein(p)
        dn = os.path.dirname(fn)
        if not os.path.exists(dn):
            os.makedirs(dn)
        file(fn, 'w').write(txt)
        return fn

