import json, os, sys, itertools
from plasma import Protein
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
from sluice.api import Sluice
from sluice.settings.fluoro.base import FluoroBase
from sluice.settings.attributes import Attribute

class TextureFluoro(FluoroBase):
    def __init__(self, *args, **kwargs):
        super(TextureFluoro, self).__init__(*args, **kwargs)
        self.__daemon = None
        self.__update_frequency = None
        self.__verbose_request = False
        self.__ignore_movement = False
        self.__image_is_static = False
        attrs = self.get_attributes()
        attrs.add(Attribute('opacity'))
        attrs.add(Attribute('style', True))
        attrs.add(Attribute('texture-wait'))
        self.add_opacity(1.0)
        self.add_wait(5.0)

    def load_from_protein(self, p):
        if isinstance(p, basestring):
            fn = Sluice.get_etc_file(p)
            if fn is None:
                raise Exception("can't find fluoroscope protein %s" % p)
            p = parse_yaml_protein(file(fn))
        elif not isinstance(p, Protein):
            raise Exception("not a protein")
        if p.ingests().get('type') != 'texture':
            raise Exception("not a texture fluoroscope")
        return self.import_protein(p.ingests())

    def import_protein(self, conf):
        if conf.get('include-data'):
            if not isinstance(self, DecoratedTextureFluoro):
                return DecoratedTextureFluoro(self.get_demo_id()).import_protein(conf)
        super(TextureFluoro, self).import_protein(conf)
        self.set_daemon(conf.get('daemon'))
        self.set_verbose(conf.get('verbose-request'))
        self.set_no_move(conf.get('ignore-movement'))
        self.set_static(conf.get('image-is-static'))
        self.set_update_frequency(conf.get('update-frequency'))
        return self

    def import_config(self, conf):
        if conf.get('include-data'):
            if not isinstance(self, DecoratedTextureFluoro):
                return DecoratedTextureFluoro(self.get_demo_id()).import_config(conf)
        super(TextureFluoro, self).import_config(conf)
        self.set_daemon(conf.get('daemon'))
        self.set_verbose(conf.get('verbose-request'))
        self.set_no_move(conf.get('ignore-movement'))
        self.set_static(conf.get('image-is-static'))
        self.set_update_frequency(conf.get('update-frequency'))
        return self

    def get_type(self):
        return 'texture'

    def set_daemon(self, name):
        self.__daemon = str(name)

    def get_daemon(self):
        return self.__daemon

    def set_verbose(self, v):
        self.__verbose_request = bool(v)

    def get_verbose(self):
        return self.__verbose_request

    def set_no_move(self, v):
        self.__ignore_movement = bool(v)

    def get_no_move(self):
        return self.__ignore_movement

    def set_static(self, v):
        self.__image_is_static = bool(v)

    def get_static(self):
        return self.__image_is_static

    def set_update_frequency(self, v):
        if v is None:
            self.__update_frequency = None
        else:
            self.__update_frequency = float(v)

    def get_update_frequency(self):
        return self.__update_frequency

    def add_style(self, key, name):
        attr = self.get_attributes().describe('style')
        attr.add_option(key, name)

    def set_style(self, val):
        self.get_attributes()['style'] = val

    def add_opacity(self, v):
        attr = self.get_attributes().describe('opacity')
        cur = attr.value()
        vals = set(float(k) for k in attr.keys())
        for k in vals:
            del(attr[k])
        vals.add(float(v))
        for k in sorted(vals):
            attr.add_option(str(k), '%d%%' % (k * 100))
        attr.set_value(cur)

    def set_opacity(self, v):
        self.get_attributes()['opacity'] = v

    def add_wait(self, v):
        attr = self.get_attributes().describe('texture-wait')
        cur = attr.value()
        vals = set(float(k) for k in attr.keys())
        for k in vals:
            del(attr[k])
        vals.add(float(v))
        for k in sorted(vals):
            attr.add_option(str(k), str(k))
        attr.set_value(cur)

    def export(self):
        d = super(TextureFluoro, self).export()
        d['daemon'] = self.get_daemon()
        if self.get_verbose():
            d['verbose-request'] = True
        if self.get_no_move():
            d['ignore-movement'] = True
        if self.get_static():
            d['image-is-static'] = True
        if self.get_update_frequency():
            d['update-frequency'] = self.get_update_frequency()
        return d

    def export_config(self):
        d = super(TextureFluoro, self).export_config()
        d['daemon'] = self.get_daemon()
        if self.get_verbose():
            d['verbose-request'] = True
        if self.get_no_move():
            d['ignore-movement'] = True
        if self.get_static():
            d['image-is-static'] = True
        if self.get_update_frequency():
            d['update-frequency'] = self.get_update_frequency()
        return d

class DecoratedTextureFluoro(TextureFluoro):
    def __init__(self, *args, **kwargs):
        super(DecoratedTextureFluoro, self).__init__(*args, **kwargs)
        attrs = self.get_attributes()
        attrs.add(Attribute('entity-kinds', True))
        attrs.add(Attribute('entity-attributes'))

    def add_kind(self, kind, name=None):
        if name is None:
            name = kind
        attr = self.get_attributes().describe('entity-kinds')
        if kind not in attr:
            attr.add_option(kind, name)

    def add_attribute(self, attribute, name=None):
        if name is None:
            name = attribute
        attr = self.get_attributes().describe('entity-attributes')
        if attribute not in attr:
            attr.add_option(attribute, name)

    def export(self):
        d = super(DecoratedTextureFluoro, self).export()
        d['include-data'] = True
        return d

    def export_config(self):
        d = super(DecoratedTextureFluoro, self).export_config()
        d['include-data'] = True
        return d

