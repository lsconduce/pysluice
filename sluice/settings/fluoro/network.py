import json, os, sys, itertools
from collections import OrderedDict
from plasma import Protein
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
from sluice.api import Sluice
from sluice.settings.fluoro.base import FluoroBase
from sluice.color import RGBA
from sluice.settings.attributes import Attribute

class NetworkFluoro(FluoroBase):

    def __init__(self, *args, **kwargs):
        super(NetworkFluoro, self).__init__(*args, **kwargs)
        self.__shaders = { }
        self.__pigments = OrderedDict()
        self.__inculcators = OrderedDict()
        self.__hover = Hover()
        self.__labels = { }
        self.__disabled_kinds = set()
        self.__filters = OrderedDict()
        self.set_default_shader('point', 'network.frag', 'network.vert')
        self.set_default_shader('path', 'ribbons.frag', 'marching_ribbons.vert')

    def get_type(self):
        return 'network'

    def load_from_protein(self, p):
        if isinstance(p, basestring):
            fn = Sluice.get_etc_file(p)
            if fn is None:
                raise Exception("can't find fluoroscope protein %s" % p)
            p = parse_yaml_protein(file(fn))
        elif not isinstance(p, Protein):
            raise Exception("not a protein")
        if p.ingests().get('type') != 'network':
            raise Exception("not a network fluoroscope")
        return self.import_protein(p.ingests())

    def import_protein(self, conf):
        super(NetworkFluoro, self).import_protein(conf)
        for k, v in conf.get('default_shaders', {}).iteritems():
            self.set_default_shader(k, **v)
        for pig in conf.get('pigments', []):
            self.add_pigment(pig)
        h = conf.get('hover')
        if h:
            for label in h.get('labels', []):
                self.add_label(label)
            h.pop('labels', None)
            self.update_hover(**h)
        attrs = self.get_attributes()
        for k in self.kinds():
            self.disable_kind(k)
        if 'enabled-kinds' in attrs:
            for k in attrs['enabled-kinds']:
                self.enable_kind(k)
        if 'dynamic-filters-whitelist' in attrs:
            for js, name in attrs.describe('dynamic-filters-whitelist').iteritems():
                self.add_filter(name, js)
        return self

    def import_config(self, conf):
        super(NetworkFluoro, self).import_config(conf)
        self.update_hover(**conf.get('hover'))
        for k, label in conf.get('labels', {}).iteritems():
            self.set_label(k, label)
        for pig in conf.get('pigments', []):
            self.add_pigment(pig)
        for k in conf.get('disabled-kinds', []):
            self.disable_kind(k)
        for name, js in conf.get('filters', {}).iteritems():
            self.add_filter(name, js)
        return self

    def kinds(self):
        kinds = set(self.__labels.keys())
        for pig in self.__pigments.itervalues():
            for inc in pig.get_inculcators():
                for k in inc.get_kinds():
                    kinds.add(k)
        return list(kinds)

    def disable_kind(self, kind):
        self.__disabled_kinds.add(kind)

    def enable_kind(self, kind):
        self.__disabled_kinds.discard(kind)

    def set_default_shader(self, type, fragment, vertex):
        if type not in ('path', 'point'):
            raise Exception("unknown shader type %s" % type)
        self.__shaders[type] = Shader(fragment, vertex)

    def get_shaders(self):
        return self.__shaders

    def add_pigment(self, pig):
        if isinstance(pig, Pigment):
            self.__pigments[pig.get_name()] = pig
        elif isinstance(pig, dict):
            pig = Pigment(self.get_demo_id(), **pig)
            self.__pigments[pig.get_name()] = pig
        else:
            raise Exception("not a pigment definition")

    def get_pigments(self):
        return self.__pigments

    def get_pigment(self, name):
        return self.__pigments.get(name)

    def set_pigments(self, pigs):
        self.__pigments = OrderedDict()
        for pig in pigs:
            self.add_pigment(pig)

    def update_hover(self, **kwargs):
        for k, v in kwargs.iteritems():
            m = getattr(self.__hover, 'set_%s' % k.replace('-', '_'), None)
            if m is not None:
                m(v)

    def add_label(self, d):
        self.set_label(d['kind'], d)

    def set_label(self, kind, *args, **kwargs):
        if len(args) == 1 and isinstance(args[0], Label):
            self.__labels[kind] = args[0]
            return
        elif len(args) == 1 and isinstance(args[0], dict):
            kwargs = args[0]
        l = Label()
        if 'map' in kwargs:
            for k, v in kwargs['map'].iteritems():
                if v is None:
                    l.skip(k)
                else:
                    l.rename(k, v)
        else:
            for k, v in kwargs.get('rename', {}).iteritems():
                l.rename(k, v)
            for k in kwargs.get('skip', []):
                l.skip(k)
        l.set_name(kwargs.get('name', kwargs.get('display')))
        self.__labels[kind] = l

    def add_filter(self, name, js):
        self.__filters[name] = js

    def get_filters(self):
        return self.__filters

    def export(self):
        attrs = self.get_attributes()
        attrs.pop('enabled-kinds', None)
        attrs.pop('enable-menu', None)
        attrs.pop('dynamic-filters-whitelist', None)
        ek = Attribute('enabled-kinds', True)
        for k in self.kinds():
            l = self.__labels.get(k)
            if l:
                name = l.get_name()
            else:
                name = k
            if name is None:
                name = k
            ek.add_option(k, name, k not in self.__disabled_kinds)
        attrs.add(ek)
        em = Attribute('enable-menu')
        em.add_option(True, 'Enable', True)
        em.add_option(False, 'Disable', False)
        attrs.add(em)
        if len(self.__filters):
            dfw = Attribute('dynamic-filters-whitelist', True)
            for k, f in self.__filters.iteritems():
                dfw.add_option(f, k)
            attrs.add(dfw)
        d = super(NetworkFluoro, self).export()
        d['default_shaders'] = dict((k, v.export()) for k, v in self.__shaders.iteritems())
        d['kinds'] = self.kinds()
        d['pigments'] = []
        for k, v in self.__pigments.iteritems():
            p = v.export()
            p['name'] = k
            d['pigments'].append(p)
        d['hover'] = self.__hover.export()
        d['hover']['labels'] = []
        for k, v in self.__labels.iteritems():
            l = v.export()
            l['kind'] = k
            d['hover']['labels'].append(l)
        return d

    def export_config(self):
        attrs = self.get_attributes()
        attrs.pop('enabled-kinds', None)
        attrs.pop('enable-menu', None)
        attrs.pop('dynamic-filters-whitelist', None)
        d = super(NetworkFluoro, self).export_config()
        d['pigments'] = []
        for k, v in self.__pigments.iteritems():
            p = v.export_config()
            p['name'] = k
            d['pigments'].append(p)
        d['labels'] = dict((k, v.export_config()) for k, v in self.__labels.iteritems())
        d['hover'] = self.__hover.export_config()
        filters = self.get_filters()
        if len(filters):
            d['filters'] = filters
        return d

class Inculcator(object):
    def __init__(self, **kwargs):
        self.__name = None
        self.__kinds = list()
        self.__properties = dict()
        self.__jstest = None

    def set_name(self, name):
        self.__name = str(name)

    def get_name(self):
        return self.__name

    def set_kinds(self, kinds):
        self.__kinds = []
        for k in kinds:
            self.add_kind(k)

    def add_kind(self, kind):
        self.__kinds.append(str(kind))

    def get_kinds(self):
        return self.__kinds

    def set_attrs(self, attrs):
        self.__properties = {}
        for k, v in attrs.iteritems():
            self.add_attr(k, v)

    def add_attr(self, k, v):
        self.__properties[str(k)] = str(v)

    def get_attrs(self):
        return self.__properties

    def set_test(self, js):
        self.__jstest = str(js)

    def get_test(self):
        return self.__jstest

    def export(self, pigment):
        name = self.get_name()
        if name is None:
            name = pigment.get_name()
        d = {
            'name': name,
            'kinds': self.get_kinds(),
            'pigment': pigment.get_name()
        }
        attrs = self.get_attrs()
        if len(attrs):
            d['properties'] = attrs
        js = self.get_test()
        if js:
            d['jstest'] = js
        return d

    def export_config(self):
        d = { 'kinds': self.get_kinds() }
        attrs = self.get_attrs()
        if len(attrs):
            d['attrs'] = attrs
        js = self.get_test()
        if js:
            d['test'] = js
        return d

class Pigment(object):
    def __init__(self, id, **kwargs):
        self.__id = id
        self.__name = None
        self.__type = None
        self.__size = None
        self.__color = None
        self.__min_zoom = None
        self.__icon = None
        self.__match = []
        self.update(**kwargs)
        for inc in kwargs.get('match', []):
            self.add_inculcator(inc)

    def update(self, **kwargs):
        for k, v in kwargs.iteritems():
            m = getattr(self, 'set_%s' % k.replace('-', '_'), None)
            if m is not None:
                m(v)

    def get_demo_id(self):
        return self.__id

    def set_name(self, name):
        self.__name = str(name)

    def get_name(self):
        return self.__name

    def set_min_zoom(self, zoom):
        self.__min_zoom = float(zoom)

    def get_min_zoom(self):
        return self.__min_zoom

    def set_type(self, type):
        self.__type = type

    def get_type(self):
        return self.__type

    def set_icon(self, icon):
        fn = Sluice.get_share_file(icon)
        if fn is None:
            raise Exception("image %s not found" % icon)
        self.__icon = icon

    def get_icon(self):
        return self.__icon

    def set_size(self, size):
        if isinstance(size, (int, long, float)):
            self.__size = float(size)
        elif isinstance(size, SineFloat):
            self.__size = size
        #elif isinstance(size, DecayFloat):
        #    self.__size = size
        elif isinstance(size, dict):
            if size.get('type') == 'SoftFloat':
                self.__size = float(size.get('value'))
            elif 'frequency' in size:
                self.__size = SineFloat(size['frequency'], size['center'], size['amplitude'], size.get('phase', 0.0))
            elif 'freq' in size:
                self.__size = SineFloat(size['freq'], size['center'], size['amp'], size.get('phase', 0.0))
            #elif 'asymptote' in size:
            #    self.__size = DecayFloat(size['initial'], size['asymptote'], size['halflife'])
            #elif 'start' in size:
            #    self.__size = DecayFloat(size['start'], size['end'], size['halflife'])
            else:
                raise Exception("not a size definition %s" % size)
        else:
            raise Exception("not a size definition %s" % size)

    def get_size(self):
        return self.__size

    def set_color(self, color):
        if isinstance(color, SoftColor):
            self.__color = color
        elif isinstance(color, SineColor):
            self.__color = color
        #elif isinstance(color, DecayColor):
        #    self.__color = color
        elif isinstance(color, (int, long, float)):
            self.__color = SoftColor(color)
        elif isinstance(color, list):
            color = list(float(x) for x in color)
            self.__color = SoftColor(*color)
        elif isinstance(color, dict):
            if color.get('type') == 'SoftColor':
                self.__color = SoftColor(*color['value'])
            elif color.get('type') == 'SineColor':
                self.__color = SineColor(color.get('frequency'), color.get('center'), color.get('amplitude'), color.get('phase'))
            #elif color.get('type') == 'DecayColor':
            #    self.__color = DecayColor(color.get('initial'), color.get('asymptote'), color.get('halflife'))
            elif color.get('freq'):
                self.__color = SineColor(color.get('freq'), color.get('center'), color.get('amp'), color.get('phase'))
            #elif color.get('halflife'):
            #    self.__color = DecayColor(color.get('start'), color.get('end'), color.get('halflife'))
            else:
                raise Exception("not a color definiton")
        else:
            raise Exception("not a color definition")

    def get_color(self):
        return self.__color

    def add_inculcator(self, *args, **kwargs):
        if len(args) == 1 and isinstance(args[0], Inculcator):
            self.__match.append(args[0])
            return args[0]
        if len(args) == 1 and isinstance(args[0], dict):
            kwargs = args[0]
        inc = Inculcator()
        if 'name' in kwargs:
            inc.set_name(kwargs['name'])
        if 'kinds' in kwargs:
            inc.set_kinds(kwargs['kinds'])
        if 'properties' in kwargs:
            inc.set_attrs(kwargs['properties'])
        if 'attrs' in kwargs:
            inc.set_attrs(kwargs['attrs'])
        if 'jstest' in kwargs:
            inc.set_test(kwargs['jstest'])
        if 'test' in kwargs:
            inc.set_test(kwargs['test'])
        self.__match.append(inc)
        return inc

    def get_inculcators(self):
        return self.__match

    def export(self):
        d = {
            'name': self.get_name(),
            'type': self.get_type()
        }
        min_zoom = self.get_min_zoom()
        if min_zoom:
            d['min-zoom'] = min_zoom
        icon = self.get_icon()
        if icon:
            d['icon'] = icon
        color = self.get_color()
        if color:
            d['color'] = color.export()
        size = self.get_size()
        if size:
            if isinstance(size, float):
                d['size'] = size
            else:
                d['size'] = size.export()
        return d

    def export_config(self):
        d = {
            'name': self.get_name(),
            'type': self.get_type(),
            'match': list(inc.export_config() for inc in self.get_inculcators())
        }
        min_zoom = self.get_min_zoom()
        if min_zoom:
            d['min-zoom'] = min_zoom
        icon = self.get_icon()
        if icon:
            d['icon'] = icon
        color = self.get_color()
        if color:
            d['color'] = color.export_config()
        size = self.get_size()
        if size:
            if isinstance(size, float):
                d['size'] = size
            else:
                d['size'] = size.export_config()
        return d

class Shader(object):
    def __init__(self, fragment, vertex):
        self.__fragment = None
        self.__vertex = None
        self.set_fragment(fragment)
        self.set_vertex(vertex)

    def set_fragment(self, fragment):
        fn = Sluice.get_etc_file(os.path.join('glsl', fragment))
        if fn is None:
            raise Exception("can't find fragment %s" % fragment)
        self.__fragment = str(fragment)

    def get_fragment(self):
        return self.__fragment

    def set_vertex(self, vertex):
        fn = Sluice.get_etc_file(os.path.join('glsl', vertex))
        if fn is None:
            raise Exception("can't find vertex %s" % vertex)
        self.__vertex = str(vertex)

    def get_vertex(self):
        return self.__vertex

    def export(self):
        return {
            'fragment': self.get_fragment(),
            'vertex': self.get_vertex()
        }

    def export_config(self):
        return self.export()


class SineFloat(object):
    def __init__(self, freq, center, amp, phase=0.0):
        self.__freq = None
        self.__center = None
        self.__amp = None
        self.__phase = None
        if freq is not None:
            self.set_frequency(freq)
        if center is not None:
            self.set_center(center)
        if amp is not None:
            self.set_amplitude(amp)
        if phase is not None:
            self.set_phase(phase)

    def get_frequency(self):
        return self.__freq

    def set_frequency(self, freq):
        self.__freq = float(freq)

    def get_center(self):
        return self.__center

    def set_center(self, center):
        self.__center = float(center)

    def get_amplitude(self):
        return self.__amp

    def set_amplitude(self, amp):
        self.__amp = float(amp)

    def get_phase(self):
        return self.__phase

    def set_phase(self, phase):
        self.__phase = float(phase)

    def export(self):
        return {
            'type': 'SineFloat',
            'frequency': self.get_frequency(),
            'center': self.get_center(),
            'amplitude': self.get_amplitude(),
            'phase': self.get_phase()
        }

    def export_config(self):
        return {
            'freq': self.get_frequency(),
            'center': self.get_center(),
            'amp': self.get_amplitude(),
            'phase': self.get_phase()
        }

#class DecayFloat(object):
#    pass

class SoftColor(RGBA):
    def __init__(self, *args):
        args = list(float(x) for x in args)
        if len(args) == 1:
            super(SoftColor, self).__init__(args[0], args[0], args[0], 1.0)
        elif len(args) == 2:
            super(SoftColor, self).__init__(args[0], args[0], args[0], args[1])
        elif len(args) == 3:
            super(SoftColor, self).__init__(args[0], args[1], args[2], 1.0)
        else:
            super(SoftColor, self).__init__(args[0], args[1], args[2], args[3])

    def as_list(self):
        if self.r == self.g == self.b:
            if self.a == 1.0:
                return [self.r]
            return [self.r, self.a]
        if self.a == 1.0:
            return [self.r, self.g, self.b]
        return [self.r, self.g, self.b, self.a]

    def export(self):
        return {
            'type': 'SoftColor',
            'value': self.as_list()
        }

    def export_config(self):
        return self.as_list()

class SineColor(SineFloat):
    def __init__(self, *args, **kwargs):
        self.__center = None
        self.__amp = None
        super(SineColor, self).__init__(*args, **kwargs)

    def _parse_color(self, *args):
        if len(args) == 1 and isinstance(args[0], list):
            #if this is a list wrapped in a tuple, unwrap it
            args = args[0]

        if len(args) == 1 and isinstance(args[0], SoftColor):
            return args[0]
        else:
            return SoftColor(*args)

    def get_center(self):
        return self.__center

    def set_center(self, *args):
        self.__center = self._parse_color(*args)

    def get_amplitude(self):
        return self.__amp

    def set_amplitude(self, *args):
        self.__amp = self._parse_color(*args)

    def export(self):
        return {
            'type': 'SineColor',
            'frequency': self.get_frequency(),
            'center': self.get_center().as_list(),
            'amplitude': self.get_amplitude().as_list(),
            'phase': self.get_phase()
        }

    def export_config(self):
        return {
            'freq': self.get_frequency(),
            'center': self.get_center().as_list(),
            'amp': self.get_amplitude().as_list(),
            'phase': self.get_phase()
        }

class Hover(object):
    def __init__(self):
        self.__max_found = 256
        self.__min_zoom = 20.0
        self.__glyph = 'sluice/sharkmouth.png'

    def get_max_found(self):
        return self.__max_found

    def set_max_found(self, n):
        if n is None:
            n = 256
        self.__max_found = int(n)

    def get_min_zoom(self):
        return self.__min_zoom

    def set_min_zoom(self, z):
        if z is None:
            z = 1.0
        self.__min_zoom = float(z)

    def get_glyph(self):
        return self.__glyph

    def set_glyph(self, icon):
        if icon is None:
            icon = 'sluice/sharkmouth.png'
        fn = Sluice.get_share_file(icon)
        if fn is None:
            raise Exception("icon %s not found" % icon)
        self.__glyph = str(icon)

    def export(self):
        return {
            'max-found': self.get_max_found(),
            'min-zoom': self.get_min_zoom(),
            'glyph': self.get_glyph(),
        }

    def export_config(self):
        return {
            'max-found': self.get_max_found(),
            'min-zoom': self.get_min_zoom(),
            'glyph': self.get_glyph()
        }

class Label(object):
    def __init__(self):
        self.__name = None
        self.__map = { }

    def set_name(self, name):
        self.__name = str(name)

    def get_name(self):
        return self.__name

    def rename(self, attr, name):
        self.__map[str(attr)] = str(name)

    def skip(self, attr):
        self.__map[str(attr)] = None

    def get_rename(self):
        return dict((k, v) for k, v in self.__map.iteritems() if v is not None)

    def get_skip(self):
        return list(k for k, v in self.__map.iteritems() if v is None)

    def export(self):
        return {
            'display': self.get_name(),
            'rename': self.get_rename(),
            'skip': self.get_skip()
        }

    def export_config(self):
        return {
            'name': self.get_name(),
            'map': self.__map
        }

