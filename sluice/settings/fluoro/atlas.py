import json, os, sys, itertools
from copy import deepcopy
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
from sluice.api import Sluice
from sluice.settings.fluoro.base import FluoroBase
from sluice.settings.attributes import Attribute
from sluice.settings.layers import StaticLayers
from sluice.settings.tiles import Tiles

class AtlasFluoro(FluoroBase):

    def __init__(self, *args, **kwargs):
        super(AtlasFluoro, self).__init__(*args, **kwargs)
        self.__layers = StaticLayers(self.get_demo_id())
        self.__tiles = Tiles(self.get_demo_id())
        attr = Attribute('alpha-per-second')
        attr.add_option(1.0, 'One second fade in', True)
        attr.add_option(2.0, 'Half second fade in')
        attr.add_option(0.5, 'Two second fade in')
        attr.add_option(4.0, 'Quarter second fade in')
        self.get_attributes().add(attr)

    def load_from_protein(self, p):
        if isinstance(p, basestring):
            fn = Sluice.get_etc_file(p)
            if fn is None:
                raise Exception("can't find map protein %s" % p)
            p = parse_yaml_protein(file(fn))
        return self.import_protein(p.ingests())

    def import_protein(self, conf):
        super(AtlasFluoro, self).import_protein(conf)
        if 'layers-path' in conf:
            try:
                #The map points to the static layer so pass the path
                self.__layers.load_from_protein(conf['layers-path'])
            except:
                pass
        attrs = self.get_attributes()
        layers = attrs.pop('layers', None)
        if layers is not None:
            for l in layers.value():
                self.enable_layer(l)
        self.__tiles.load_from_protein('tiled-config.protein')
        bg = attrs.pop('background', None)
        if bg is not None:
            for k, v in bg.iteritems():
                if k not in self.__tiles:
                    self.__tiles.add(k, name=v, image=k)
                else:
                    self.__tiles[k].set_name(v)
            self.__tiles.choose(bg.value())
        return self

    def import_config(self, conf):
        #TODO: Why is this different than import protein? I would perfer
        #import protein loaded a protein and called import_config
        super(AtlasFluoro, self).import_config(conf)
        tiles = self.get_backgrounds()
        for k, v in conf.get('background', {}).iteritems():
            tiles.add(k, **v)
            if v.get('default'):
                tiles.choose(k)
        layers = self.get_layers()
        for k, v in conf.get('layers', {}).iteritems():
            layers.add_layer(k, v)
        if 'alpha-per-second' in conf:
            attr = self.get_attributes().describe('alpha-per-second')
            for k in attr.keys():
                del(attr[k])
            for opt in conf['alpha-per-second']['options']:
                attr.add_option(opt['value'], opt['text'])
            attr.set_value(conf['alpha-per-second']['value'])
        return self

    def load_template(self):
        pass
        #raise Exception("atlas cannot be loaded as a template")

    def instantiate(self, qid, config=None):
        inst = deepcopy(self)
        if config is not None:
            inst.configure(config)
        f = inst.export()
        f['SlawedQID'] = qid
        s = Sluice()
        s.configure_fluoroscope(f)

    def name_layer(self, k, name):
        l = self.__layers.get(k)
        if l is not None:
            l.set_name(name)

    def enable_layer(self, k):
        l = self.__layers.get(k)
        if l is not None:
            l.enable()

    def disable_layer(self, k):
        l = self.__layers.get(k)
        if l is not None:
            l.disable()

    def set_name(self, *args, **kwargs):
        #raise Exception("You may not set the name of the atlas")
        pass

    def get_name(self):
        return 'Map'

    def get_type(self):
        return 'atlas'

    def has_layers(self):
        return self.__layers.has_layers()

    def get_layers(self):
        return self.__layers

    def get_backgrounds(self):
        return self.__tiles

    def name_background(self, k, name):
        t = self.__tiles.get(k)
        if t is not None:
            t.set_name(name)

    def export(self):
        attrs = self.get_attributes()
        attrs.add(self.get_backgrounds().attribute())
        if self.has_layers():
            attrs.add(self.get_layers().attribute())
        d = super(AtlasFluoro, self).export()
        if self.has_layers():
            d['layers-path'] = self.get_layers().get_file()
        return d

    def export_config(self):
        d = super(AtlasFluoro, self).export_config()
        d['background'] = self.get_backgrounds().export_config()
        d['layers'] = self.get_layers().export_config()
        return d

    def save(self):
        self.get_layers().save()
        self.get_backgrounds().save()
        return super(AtlasFluoro, self).save()

