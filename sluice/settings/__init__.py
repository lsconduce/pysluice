import json, os
import os.path
import signal as sig
from collections import OrderedDict
from plasma import Protein
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
from sluice import Sluice, APP, PROTO
from sluice.settings.attributes import Attributes, Attribute
from sluice.settings.layers import StaticLayers, StaticLayer
from sluice.settings.tiles import Tiles, TilePolicy
from sluice.settings.datasets import Dataset
from sluice.settings.daemons import Daemon
from sluice.settings.bookmarks import Bookmark
from sluice.settings.layouts import Layout
from sluice.settings.fluoro import *
import warnings

class Settings(object):

    def __init__(self, id, config=None):
        self.__id = id
        self.__map = None
        self.__scopes = []
        self.__time_formats = []
        self.__windshield_max_items = None
        self.__feldstream_path = '/opt/oblong/sluice-64-2/bin/feldstream'
        self.__zoom = OrderedDict()
        self.__bookmarks = OrderedDict()
        self.__layouts = dict()
        self.__daemons = OrderedDict(Daemon.standard_set(id))
        self.__datasets = dict()
        self.__jsplasma_apps = dict()
        self.__sluice_tabs = dict()
        self.__config = config
        self.__settings = None
        #TODO: make config required
        if config:
            self.import_config(config)
            if self.get_map() is None or len(self.get_templates()) == 0:
                #config.json does not have enough info to run sluice
                settings_path = self._get_settings_path()
                if settings_path:
                    p = parse_yaml_protein(open(settings_path, 'r'))
                    settings_conf = p.ingests()
                    self.import_protein(settings_conf)

    def get_demo_id(self):
        return self.__id

    def get_file(self):
        return self._get_settings_file()

    def _get_settings_file(self):
        #TODO: merge this code with code from treatment
        if self.__config:
            #TODO: In the future, config will be required to be set
            settings_file = self.__config.get('settings')
            if settings_file is None:
                settings_file = os.path.join(id, 'sluice-settings.protein')
        else:
            #TODO: Remove this after config is required in the __init__
            settings_file = os.path.join(self.get_demo_id(), 'sluice-settings.protein')
        return settings_file

    def _get_settings_path(self):
        return Sluice.get_etc_file(self._get_settings_file())

    def inculcators_file(self, from_settings=None):
        if from_settings:
            settings = from_settings
        else:
            settings = self.get_settings()
        return settings.get('data-definition', {}).get('inculcators')

    def get_settings(self):
        if not self.__settings:
            settings_path = Sluice.get_etc_file(self._get_settings_file())
            full_settings_path = Sluice.get_etc_file(settings_path)
            if full_settings_path is None:
                raise Exception("Can't find sluice settings file")
            pro = parse_yaml_protein(open(full_settings_path, 'r'))
            self.__settings = pro.ingests()
        return self.__settings

    def load_from_protein(self, p=None):
        if p is None:
            settings_conf = self.get_settings()
        else:
            settings_conf = p.ingests()
        return self.import_protein(settings_conf)

    def import_protein(self, settings_conf):
        self.__settings = settings_conf
        self.set_map(settings_conf.get('map'))
        for f in settings_conf.get('scopes', []):
            self.add_template(f)
        for tf in settings_conf.get('time-formats', []):
            self.add_time_format(tf)
        if 'windshield-max-items' in settings_conf:
            self.set_windshield_max_items(settings_conf['windshield-max-items'])
        try:
            if 'feldstream-path' in settings_conf:
                self.set_feldstream_path(settings_conf['feldstream-path'])
        except ValueError:
            pass
        for zl in settings_conf.get('data-definition', {}).get('zoom-levels', []):
            self.add_zoom_level(zl.get('name'), zl.get('visible'), zl.get('group'), zl.get('granularity'))
        #This is the only place we read the inculcators file *not* from self.__settings
        fn = Sluice.get_etc_file(self.inculcators_file(from_settings=settings_conf))
        if fn is None:
            raise Exception("no inculcators")
        p = parse_yaml_protein(file(fn))
        for inc in p.ingests().get('inculcators', []):
            for f in self.__scopes:
                if f.get_type() == 'network':
                    pig = f.get_pigment(inc['pigment'])
                    if pig is not None:
                        pig.add_inculcator(inc)
        return self

    def load_from_config(self, config=None):
        warnings.warn('load_from_config is no longer supported, instead pass config to __init__', DeprecationWarning)
        if not isinstance(config, dict):
            if isinstance(config, basestring):
                if config.endswith('.json'):
                    fn = config
                    if not os.path.exists(fn):
                        fn = Sluice.get_etc_file(os.path.join(self.get_demo_id(), config))
                        if fn is None:
                            fn = Sluice.get_etc_file(config)
                    if fn is None:
                        raise Exception("Can't find %s" % config)
                    config = json.load(file(fn))
                else:
                    config = json.loads(config)
            if config is None:
                #print 'looking for %s config in %s' % (self.get_demo_id(), os.getenv('OB_ETC_PATH'))
                fn = Sluice.get_etc_file(os.path.join(self.get_demo_id(), 'config.json'))
                if fn is None:
                    fn = Sluice.get_etc_file(self.get_demo_id()+'.json')
                if fn is None and os.path.exists('config.json'):
                    fn = 'config.json'
                if fn is None:
                    raise Exception("can't find config.json")
                config = json.load(file(fn))
        return self.import_config(config)

    def import_config(self, conf):
        """
        This should not be called directly, just from the __init__
        """
        if 'map' in conf:
            self.set_map(conf['map'])
        for f in conf.get('scopes', []):
            self.add_template(f)
        for tf in conf.get('time-formats', []):
            self.add_time_format(tf)
        if 'windshield-max-items' in conf:
            self.set_windshield_max_items(conf['windshield-max-items'])
        try:
            if 'feldstream-path' in conf:
                self.set_feldstream_path(conf['feldstream-path'])
        except ValueError:
            pass

        def userpath(x):
            base = os.getenv('TREATMENT_SRC', os.getenv('TREATMENT_HOME', '.'))
            return os.path.abspath(os.path.join(base, os.path.expanduser(x)))

        for k, v in conf.get('daemons', {}).iteritems():
            self.add_daemon(k, **v)
        for k, v in conf.get('datasets', {}).iteritems():
            self.add_dataset(k, **v)
        for k, v in conf.get('bookmarks', {}).iteritems():
            self.add_bookmark(k, v)
        for k, v in conf.get('layouts', {}).iteritems():
            self.add_layout(k, v)
        for k, v in conf.get('jsplasma-apps', {}).iteritems():
            path = userpath(v.get('path'))
            self.add_jsplasma_app(k, path, v.get('app'))
        for k, v in conf.get('sluice-tabs', {}).iteritems():
            path = v.get('path')
            self.add_sluice_tab(k, path, v.get('route'))

    def set_map(self, f):
        if isinstance(f, AtlasFluoro):
            self.__map = f
        elif isinstance(f, dict):
            conf = f
            f = AtlasFluoro(self.get_demo_id())
            if 'background' in conf:
                f.import_config(conf)
            else:
                f.import_protein(conf)
            self.__map = f
        elif isinstance(f, basestring):
            fn = f
            f = AtlasFluoro(self.get_demo_id())
            f.load_from_protein(fn)
            self.__map = f
        else:
            raise ValueError("not an atlas fluoro")
        return self.__map

    def get_map(self):
        return self.__map

    def get_templates(self):
        return self.__scopes

    def add_template(self, f):
        if isinstance(f, AtlasFluoro):
            raise ValueError("AtlasFluoro's must be set via set_map()")
        if isinstance(f, WebFluoro):
            raise ValueError("WebFluoro's cannot be added as templates")
        if isinstance(f, FluoroBase):
            self.__scopes.append(f)
        elif isinstance(f, dict):
            return self.add_template(Fluoro.from_config(self.get_demo_id(), f))
        elif isinstance(f, basestring):
            if f.endswith('.protein') or f.endswith('.yaml'):
                fn = Sluice.get_etc_file(f)
                p = parse_yaml_protein(file(fn))
                return self.add_template(Fluoro.from_config(self.get_demo_id(), p.ingests()))
            else:
                module = __import__(f, fromlist=['configure'])
                return self.add_template(module.configure(self.get_demo_id()))
        else:
            raise ValueError("not a fluoroscope")

    def add_time_format(self, fmt):
        if not isinstance(fmt, basestring):
            raise ValueError("time format must be a string")
        self.__time_formats.append(fmt)

    def set_windshield_max_items(self, n):
        self.__windshield_max_items = int(n)

    def set_feldstream_path(self, path):
        if path.startswith('./') or path.startswith('../'):
            path = os.path.abspath(path)
        elif not path.startswith('/'):
            for dn in os.getenv('PATH').split(os.path.pathsep):
                fn = os.path.join(dn, path)
                if os.path.exists(fn) and os.access(fn, os.X_OK):
                    path = fn
                    break
        #if not os.path.exists(path):
        #    raise Exception("%s does not exist" % path)
        #if not os.access(path, os.X_OK):
        #    raise Exception("%s is not executable" % path)
        self.__feldstream_path = path

    def add_zoom_level(self, name, visible=0.0, group=False, granularity=None):
        zl = {
            'visible': float(visible),
        }
        if group:
            zl['group'] = True
        if granularity is not None:
            zl['granularity'] = float(granularity)
        self.__zoom[str(name)] = zl

    def get_jsplasma_apps(self):
        return self.__jsplasma_apps

    def add_jsplasma_app(self, key, path, app):
        self.__jsplasma_apps[key] = {
            'path': path,
            'app': app
        }

    def get_sluice_tabs(self):
        return self.__sluice_tabs

    def add_sluice_tab(self, key, path, route):
        self.__sluice_tabs[key] = {
            'path': path,
            'route': route
        }

    def export(self):
        d = {
            'map': self.__map.get_file(),
            'scopes': list(f.get_file() for f in self.__scopes),
            'time-formats': self.__time_formats,
            'data-definition': {
                'inculcators': self.inculcators_file()
            }
        }
        if self.__windshield_max_items:
            d['windshield-max-items'] = self.__windshield_max_items
        if self.__feldstream_path:
            d['feldstream-path'] = self.__feldstream_path
        if len(self.__zoom) > 0:
            d['data-definition']['zoom-levels'] = \
                list(dict(name=k, **v) for k, v in self.__zoom.iteritems())
        return d

    def export_config(self):
        d = {
            'map': self.__map.export_config(),
            'scopes': list(f.export_config() for f in self.__scopes),
            'time-formats': self.__time_formats,
        }
        if len(self.__daemons) > 0:
            d['daemons'] = dict((k, v.export_config()) for k, v in self.__daemons.iteritems())
        if len(self.__datasets) > 0:
            d['datasets'] = dict((k, v.export_config()) for k, v in self.__datasets.iteritems())
        if len(self.__bookmarks) > 0:
            d['bookmarks'] = dict((k, v.export_config()) for k, v in self.__bookmarks.iteritems())
        if len(self.__layouts) > 0:
            d['layouts'] = dict((k, v.export_config()) for k, v in self.__layouts.iteritems())
        if self.__windshield_max_items:
            d['windshield-max-items'] = self.__windshield_max_items
        if self.__feldstream_path:
            d['feldstream-path'] = self.__feldstream_path
        if len(self.__zoom) > 0:
            d['data-definition']['zoom-levels'] = \
                list(dict(name=k, **v) for k, v in self.__zoom.iteritems())
        d['jsplasma-apps'] = self.get_jsplasma_apps()
        d['sluice-tabs'] = self.get_sluice_tabs()
        return d


    def add_daemon(self, key, *args, **kwargs):
        if len(args) == 1 and isinstance(args[0], Daemon):
            d = args[0]
        else:
            d = Daemon(self.get_demo_id(), *args, **kwargs)
        self.__daemons[key] = d
        return d

    def get_daemons(self):
        return self.__daemons

    def add_dataset(self, key, *args, **kwargs):
        if len(args) == 1 and isinstance(args[0], Dataset):
            d = args[0]
        else:
            d = Dataset(self.get_demo_id(), *args, **kwargs)
        self.__datasets[key] = d
        return d

    def get_datasets(self):
        return self.__datasets

    def load_bookmarks(self):
        #This is not specified in the sluice-settings - maybe this
        #path should come from a config file (tho this feature
        #is really mostly unused).
        fn = os.path.join(self.get_demo_id(), 'bookmarks.protein')
        fn = Sluice.get_var_file(fn)
        if fn is None:
            fn = os.path.join('sluice', 'bookmarks.protein')
            fn = Sluice.get_var_file(fn)
        if fn is None:
            raise Exception("no bookmarks found")
        for bm in parse_yaml_protein(file(fn)):
            key = '_'.join(bm['name'].lower().split())
            self.add_bookmark(key, bm)

    def add_bookmark(self, key, bm):
        if isinstance(bm, dict):
            if 'level' in bm:
                bm['zoom'] = bm.pop('level')
            bm = Bookmark(**bm)
        self.__bookmarks[key] = bm
        return bm

    def load_layouts(self):
        dn = os.path.join(self.get_demo_id(), 'mummies')
        dn = Sluice.get_var_file(dn)
        if dn is None:
            dn = os.path.join('sluice', 'mummies')
            dn = Sluice.get_var_file(dn)
        if dn is None:
            raise Exception("no mummies found")
        for name in os.listdir(dn):
            if name.endswith('.protein'):
                fn = os.path.join(dn, name)
                name = ' '.join(os.path.splitext(name)[0].split('_'))
                key = '_'.join(name.lower().split())
                layout = Layout.from_mummy(self.get_demo_id(), fn)
                self.add_layout(key, layout)

    def add_layout(self, key, layout):
        if isinstance(layout, dict):
            ## TODO
            l = Layout(layout['name'])
            l.configure_map(layout['map']['config'])
            l.zoom_to(layout['map']['lat'], layout['map']['lon'], layout['map']['zoom'])
            for scope in layout['scopes']:
                try:
                    l.add_fluoro(scope['name'], scope['bl'], scope['tr'], scope['config'])
                except Exception as e:
                    import traceback
                    traceback.print_exc()
                    print 'scope = %s' % scope
            if layout.get('default'):
                l.enable()
            layout = l
        self.__layouts[key] = layout
        return layout

    def get_layouts(self):
        return self.__layouts

    def save(self):
        self.__map.save()
        self.save_inculcators()
        for scope in self.__scopes:
            scope.save()
        fn = Sluice.get_etc_file(self.get_file(), True)
        p = Protein(['app-settings', 'sluice'], self.export())
        txt = dump_yaml_protein(p)
        dn = os.path.dirname(fn)
        if not os.path.exists(dn):
            os.makedirs(dn)
        file(fn, 'w').write(txt)
        return fn

    def generate_inculcators_protein(self):
        incs = list()
        seen = dict()
        for f in self.__scopes:
            if f.get_type() == 'network':
                for pig in f.get_pigments().itervalues():
                    for inc in pig.get_inculcators():
                        if pig.get_name() in seen:
                            inc.set_name('%s_%d' % (pig.get_name(), seen[pig.get_name()]))
                            seen[pig.get_name()] += 1
                        else:
                            seen[pig.get_name()] = 2
                        incs.append(inc.export(pig))
        p = Protein([APP, PROTO, 'inculcators', 'change'], { 'inculcators': incs })
        return p

    def save_inculcators(self):
        fn = Sluice.get_etc_file(self.inculcators_file(), True)
        p = self.generate_inculcators_protein()
        txt = dump_yaml_protein(p)
        dn = os.path.dirname(fn)
        if not os.path.exists(dn):
            os.makedirs(dn)
        file(fn, 'w').write(txt)
        return fn

