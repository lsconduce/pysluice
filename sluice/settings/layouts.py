import os
from copy import deepcopy
from numpy import array
from plasma.yamlio import parse_yaml_protein
from sluice import Sluice
from sluice.settings.fluoro import *
from sluice.mummy import GeoQuad
from sluice.settings.fluoro import *

class Layout(object):
    def __init__(self, name, map=None, scopes=None):
        self.__enabled = False
        self.__name = name
        self.__map = {
            'name': 'Map',
            'lat': 0.0,
            'lon': 0.0,
            'zoom': 1.0,
            'config': dict()
        }
        self.__scopes = list()
        self.__windshield = list()
        self.set_name(name)
        if map is not None:
            self.configure_map(map)
        if scopes is not None:
            for scope in scopes:
                self.configure_fluoro(scope)

    def enable(self):
        self.__enabled = True

    def disable(self):
        self.__enabled = False

    def get_enabled(self):
        return self.__enabled

    def set_name(self, name):
        self.__name = name

    def get_name(self):
        return self.__name

    def zoom_to(self, lat, lon, zoom):
        self.__map['lat'] = float(lat)
        self.__map['lon'] = float(lon)
        self.__map['zoom'] = float(zoom)

    def configure_map(self, conf):
        if isinstance(conf, AtlasFluoro):
            attrs = conf.get_attributes()
            attrs.add(conf.get_backgrounds().attribute())
            if conf.has_layers():
                attrs.add(conf.get_layers().attribute())
            conf = attrs.export_values()
        self.__map['config'].update(conf)

    def add_fluoro(self, name, bl, tr, conf=None):
        if isinstance(name, WebFluoro):
            self.__scopes.append(name.export_config())
            return None
        if isinstance(conf, FluoroBase):
            conf = conf.get_attributes().export_values()
        if conf is None:
            conf = { }
        if bl is not None:
            bl = list(float(x) for x in bl)
        if tr is not None:
            tr = list(float(x) for x in tr)
        scope = {
            'name': name,
            'bl': bl,
            'tr': tr,
            'config': conf
        }
        self.__scopes.append(scope)

    def add_windshield(self, f):
        self.__windshield.append(f.export_config())

    @classmethod
    def from_mummy(cls, id, fn):
        #print 'loading mummy %s' % fn
        name = os.path.splitext(os.path.split(fn)[1])[0].replace('_', ' ')
        self = cls(name)
        p = parse_yaml_protein(file(fn))
        scopes = list(x.right for x in p.ingests()['wide-screen']['fluoroscopes'])
        atlasgq = None
        for scope in scopes:
            if scope.get('class-name') == 'WebFluoroscope':
                f = WebFluoro(scope.get('name'))
                gq = GeoQuad(scope['geo-quad'])
                f.set_url(scope.get('url'))
                pin = scope.get('pins')[0].get('cxcy')
                loc = gq.loc(array([pin[0], pin[1], 0.0]))
                f.set_pin(loc[0], loc[1])
                f.set_size(scope.get('width', {}).get('cur-val'),
                           scope.get('height', {}).get('cur-val'))
                self.add_fluoro(f, None, None)
            elif scope.get('class-name') == 'StaticImageFluoroscope' and 'configuration' not in scope:
                continue
            else:
                f = Fluoro.from_config(id, scope.get('configuration'))
                if isinstance(f, AtlasFluoro):
                    self.configure_map(f)
                    gq = GeoQuad(scope['geo-quad'])
                    atlasgq = scope['geo-quad']
                    lat, lon = gq.loc()
                    self.zoom_to(lat, lon, gq.zoom())
                else:
                    gq = GeoQuad(scope['geo-quad'], atlasgq)
                    self.add_fluoro(f.get_name(), gq.bl(), gq.tr(), f)
        if 'windshield' in p.ingests():
            scopes = list(x.right for x in p.ingests()['windshield']['asset-bucket'])
            for scope in scopes:
                if scope.get('class-name') == 'WebFluoroscope':
                    f = WebFluoro(scope.get('name'))
                    #f.set_windshield(True)
                    loc = scope.get('loc', {}).get('cur-val')
                    f.set_loc(loc[0], loc[1])
                    f.set_size(scope.get('width', {}).get('cur-val'),
                               scope.get('height', {}).get('cur-val'))
                    f.set_url(scope.get('url'))
                    self.add_windshield(f)
                else:
                    ## ?
                    raise Exception("unexpected %s on the windshield" % scope.get('class-name'))
        return self

    def export(self):
        pass

    def export_config(self):
        d = {
            'name': self.__name,
            'map': self.__map,
            'scopes': self.__scopes
        }
        if len(self.__windshield):
            d['windshield'] = self.__windshield
        return d

    def load(self, settings, map_qid):
        s = Sluice()
        settings.get_map().instantiate(map_qid, self.__map['config'])
        for fconf in self.__scopes:
            if fconf.get('type') == 'web':
                print 'loading web fluoro %s' % fconf
                f = WebFluoroscope(fconf['name'])
                f.set_url(fconf['url'])
                f.set_pin(fconf['pin'])
                f.set_size(*fconf['size'])
                f.instantiate()
            else:
                found = False
                for f in settings.get_templates():
                    if f.get_name() == fconf['name']:
                        f = deepcopy(f)
                        f.configure(fconf['config'])
                        f.instantiate(fconf['bl'], fconf['tr'])
                        #s.create_fluoroscope_instance(f.export(), fconf['bl'], fconf['tr'])
                        found = True
                        break
                if not found:
                    print "can't find template for %s in %s" % (fconf, list(f.get_name() for f in settings.get_templates()))
        for fconf in self.__windshield:
            if fconf['type'] == 'web':
                f = WebFluoroscope(fconf['name'])
                f.set_url(fconf['url'])
                f.set_loc(fconf['loc'])
                f.set_size(*fconf['size'])
                f.instantiate()
        s.request_zoom_loc(self.__map['lat'], self.__map['lon'], self.__map['zoom'])

