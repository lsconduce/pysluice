import os, urlparse
from collections import OrderedDict
from plasma import Protein
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
from sluice import Sluice
from sluice.settings.attributes import Attribute

class Tiles(object):
    def __init__(self, id):
        self.__id = id
        self.__fetch_threads = 16
        self.__cache_threads = 16
        self.__cache_base = '~/tmp/cache'
        self.__request_pool = 'tcp://localhost/tiles-req'
        self.__policies = OrderedDict()
        self.__selected = None

    def get_demo_id(self):
        return self.__id

    def __delitem__(self, k):
        del(self.__policies[k])
        if self.__selected == k:
            if len(self.__policies) > 0:
                self.__selected = self.__policies.keys()[0]
            else:
                self.__selected = None

    def __contains__(self, key):
        return key in self.__policies

    def __getitem__(self, key):
        return self.__policies[key]

    def get_file(self):
        #This is the 1 file that is not specified in sluice-settings or config.json
        return os.path.join(self.get_demo_id(), 'tiled-config.protein')

    def load_from_protein(self, fn=None):
        if fn is None:
            fn = Sluice.get_etc_file(os.path.join(self.get_demo_id(), 'tiled-config.protein'))
        else:
            fn = Sluice.get_etc_file(fn)
        if fn is None:
            fn = Sluice.get_etc_file('tiled-config.protein')
        if fn is None:
            raise Exception("couldn't find tiled config")
        tiles = parse_yaml_protein(file(fn)).ingests()
        self.set_fetch_threads(tiles.get('fetch-threads'))
        self.set_cache_threads(tiles.get('cache-threads'))
        self.set_cache_base(tiles.get('cache-base'))
        self.set_request_pool(tiles.get('request-pool'))
        for policy in tiles.get('tile-policies', []):
            self.add(policy.get('mapid'), **policy)

    def get_fetch_threads(self):
        return self.__fetch_threads

    def get_cache_threads(self):
        return self.__cache_threads

    def get_cache_base(self):
        return self.__cache_base

    def get_request_pool(self):
        return self.__request_pool

    def set_fetch_threads(self, n):
        if n is not None:
            self.__fetch_threads = int(n)

    def set_cache_threads(self, n):
        if n is not None:
            self.__cache_threads = int(n)

    def set_cache_base(self, path):
        if path is not None:
            self.__cache_base = str(path)

    def set_request_pool(self, pool):
        if pool is not None:
            self.__request_pool = str(pool)

    def add(self, key, name=None, *args, **kwargs):
        if name is None:
            name = key
        policy = TilePolicy(name, *args, **kwargs)
        self.__policies[key] = policy
        if len(self.__policies) == 1:
            self.__selected = key

    def choose(self, key):
        if key not in self.__policies:
            raise KeyError(key)
        self.__selected = key

    def export(self):
        return {
            'fetch-threads': self.get_fetch_threads(),
            'cache-threads': self.get_cache_threads(),
            'cache-base': self.get_cache_base(),
            'request-pool': self.get_request_pool(),
            'tile-policies': list(p.export(k) for k, p in self.__policies.iteritems() if p.get_type() != 'image')
        }

    def export_config(self):
        d = dict((k, p.export_config()) for k, p in self.__policies.iteritems())
        if self.__selected in d:
            d[self.__selected]['default'] = True
        return d

    def attribute(self):
        attr = Attribute('background')
        for k, p in self.__policies.iteritems():
            if p.get_type() == 'image':
                attr.add_option(p.get_image(), p.get_name(), k == self.__selected)
            else:
                attr.add_option(k, p.get_name(), k == self.__selected)
        return attr

    def save(self):
        fn = Sluice.get_etc_file(self.get_file(), True)
        p = Protein(['tiled', 'config'], self.export())
        txt = dump_yaml_protein(p)
        dn = os.path.dirname(fn)
        if not os.path.exists(dn):
            os.makedirs(dn)
        file(fn, 'w').write(txt)
        return fn

class TilePolicy(object):
    __URLTYPE = 'standard'
    __POOLTYPE = 'forward'
    __IMAGETYPE = 'image'
    __FORMATS = {
        'png': 'png',
        'jpg': 'jpeg',
        'jpeg': 'jpeg',
        'tif': 'tif',
        'tiff': 'tiff',
        'gif': 'gif'
    }

    def __init__(self, name=None, url=None, pool=None, image=None, **kwargs):
        self.__name = None
        self.__url = None
        self.__pool = None
        self.__image = None
        self.__type = None
        self.set_name(name)
        self.set_image(image)
        self.set_pool(pool)
        self.set_url(url)
        self.__max_z = kwargs.get('max-z')

    def get_name(self):
        return self.__name

    def set_name(self, name):
        if name is not None:
            self.__name = str(name)

    def get_type(self):
        return self.__type

    def get_url(self):
        if self.__type == self.__URLTYPE:
            return self.__url
        raise Exception("Not a URL type")

    def set_url(self, url):
        if url is not None:
            self.__url = str(url)
            self.__type = self.__URLTYPE

    def get_pool(self):
        if self.__type == self.__POOLTYPE:
            return self.__pool
        raise Exception("Not a pool type")

    def set_pool(self, pool):
        if pool is not None:
            self.__pool = str(pool)
            self.__type = self.__POOLTYPE

    def get_image(self):
        if self.__type == self.__IMAGETYPE:
            return self.__image
        raise Exception("Not an image type")

    def set_image(self, img):
        if img is not None:
            fn = Sluice.get_share_file(img)
            if fn is None:
                raise Exception("image not found")
            self.__image = img
            self.__type = self.__IMAGETYPE

    def get_max_z(self):
        return self.__max_z

    def get_format(self):
        if self.__type == self.__IMAGETYPE:
            ext = self.get_image().rsplit('.', 1)[-1]
            return self.__FORMATS.get(ext)
        if self.__type == self.__URLTYPE:
            ext = urlparse.urlparse(self.get_url()).path.rsplit('.')[-1]
            return self.__FORMATS.get(ext)
        else:
            return 'png'

    def get_cache(self):
        return '$BASE/$MAPID/$Z/$X/$Z_$X_$Y.%s' % self.get_format()

    def export(self, mapid):
        if self.get_type() == self.__IMAGETYPE:
            return None
        d = { 'mapid': mapid, 'type': self.get_type() }
        if self.get_type() == self.__URLTYPE:
            d['url'] = self.get_url()
            d['image-format'] = self.get_format()
            d['cache'] = self.get_cache()
        elif self.get_type() == self.__POOLTYPE:
            d['pool'] = self.get_pool()
        if self.__max_z:
            d['max-z'] = self.__max_z
        return d

    def export_config(self):
        d = { 'name': self.get_name() }
        if self.get_type() == self.__URLTYPE:
            d['url'] = self.get_url()
        elif self.get_type() == self.__POOLTYPE:
            d['pool'] = self.get_pool()
        elif self.get_type() == self.__IMAGETYPE:
            d['image'] = self.get_image()
        return d

