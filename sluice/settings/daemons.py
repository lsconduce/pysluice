import os, subprocess, re
import signal as sig
from sluice import Sluice

class Daemon(object):
    __SCRIPTTYPE = 'script'
    __PYTHONTYPE = 'python'
    __RUBYTYPE = 'ruby'
    __JAVATYPE = 'java'

    def __init__(self, id, name, script=None, python=None, ruby=None, java=None, args=None, default=False, signal='TERM'):
        self.__id = id
        self.__name = None
        self.__type = None
        self.__script = None
        self.__python = None
        self.__ruby = None
        self.__java = None
        self.__args = []
        self.__enabled = None
        self.__signal = None
        self.__proc = None
        self.set_name(name)
        self.set_java(java)
        self.set_ruby(ruby)
        self.set_python(python)
        self.set_script(script)
        self.set_signal(signal)
        if args is not None:
            for arg in args:
                self.add_arg(arg)
        if default:
            self.enable()

    @classmethod
    def standard_set(cls, id):
        return {
            'tiled': cls.tiled(id),
            'mummy': cls.mummy_reader(id)
        }

    @classmethod
    def tiled(cls, id):
        fn = Sluice.get_etc_file(os.path.join(id, 'tiled-config.protein'))
        if fn is None:
            fn = Sluice.get_etc_file('tiled-config.protein')
        if fn is None:
            raise Exception("tiled config not found")
        args = [{ 'name': 'config', 'short': '-c', 'default': fn }]
        return cls(id, 'Tile Daemon', script='tiled', args=args, default=True)

    @classmethod
    def mummy_reader(cls, id):
        dn = Sluice.get_var_file(os.path.join('sluice', 'mummies'))
        args = [{ 'name': 'Mummy dir', 'short': '-m', 'default': dn }]
        return cls(id, 'Mummy Reader', script='mummy_reader', args=args, default=True)

    def get_demo_id(self):
        return self.__id

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = str(name)

    def is_script(self):
        return self.__type == self.__SCRIPTTYPE

    def is_python(self):
        return self.__type == self.__PYTHONTYPE

    def is_ruby(self):
        return self.__type == self.__RUBYTYPE

    def is_java(self):
        return self.__type == self.__JAVATYPE

    def set_script(self, script):
        if script is not None:
            found = False
            script = os.path.expanduser(script)
            if script.startswith('./'):
                if os.path.exists(script) and os.access(script, os.X_OK):
                    found = True
            elif script.startswith('/') or script.startswith('../'):
                script = os.path.abspath(script)
                if os.path.exists(script) and os.access(script, os.X_OK):
                    found = True
            else:
                for dn in os.getenv('PATH').split(os.path.pathsep):
                    fn = os.path.join(dn, script)
                    if os.path.exists(fn) and os.access(fn, os.X_OK):
                        found = True
                        break
            if not found:
                raise Exception("executable %s not found in path" % script)
            self.__script = str(script)
            self.__type = self.__SCRIPTTYPE

    def set_python(self, python):
        if python is not None:
            self.__python = str(python)
            self.__type = self.__PYTHONTYPE

    def set_ruby(self, ruby):
        if ruby is not None:
            self.__ruby = ruby
            self.__type = self.__RUBYTYPE

    def set_java(self, java):
        if java is not None:
            self.__java = java
            self.__type = self.__JAVATYPE

    def add_arg(self, arg):
        if isinstance(arg, DaemonArg):
            self.__args.append(arg)
            return arg
        elif isinstance(arg, dict):
            arg = DaemonArg(**arg)
            self.__args.append(arg)
            return arg
        elif isinstance(arg, basestring):
            while arg.startswith('-'):
                arg = arg[1:]
            if len(arg) == '':
                raise Exception("bare argument")
            if len(arg) == 1:
                arg = { 'short': arg, 'name': arg }
            else:
                arg = { 'long': arg, 'name': arg }
            arg = DaemonArg(**arg)
            self.__args.append(arg)
            return arg
        else:
            raise ValueError("not an argument")

    def get_args(self):
        return self.__args

    def enable(self):
        self.__enabled = True

    def disable(self):
        self.__enabled = False

    def get_enabled(self):
        return self.__enabled

    def set_signal(self, signal):
        if not hasattr(sig, 'SIG'+signal):
            raise Exception("no such signal %s" % signal)
        self.__signal = signal

    def get_signal(self):
        return self.__signal

    def export(self):
        if not self.get_enabled():
            return None
        cmd = []
        if self.__type == self.__SCRIPTTYPE:
            if not self.__script.startswith('/'):
                for dn in os.getenv('PATH').split(os.path.pathsep):
                    scr = os.path.join(dn, self.__script)
                    if os.path.exists(scr) and os.access(scr, os.X_OK):
                        cmd = [scr]
                if len(cmd) == 0:
                    raise Exception("script %s not found in PATH" % self.__script)
            else:
                cmd = [self.__script]
        elif self.__type == self.__PYTHONTYPE:
            cmd = ['python', '-m', self.__python]
        elif self.__type == self.__RUBYTYPE:
            for p in os.getenv('RUBYLIB').split(os.path.pathsep):
                script = os.path.join(p, self.__ruby)
                if not script.endswith('.rb'):
                    script += '.rb'
                if os.path.exists(script):
                    cmd = ['ruby', '-rubygems', script]
                    break
            if len(cmd) == 0:
                raise Exception("ruby script %s not found" % self.__ruby)
        elif self.__type == self.__JAVATYPE:
            cmd = ['java', '-Xms128m', '-Xmx256m', '-jar', self.__java]
        else:
            raise Exception("unknown daemon type")
        for arg in self.get_args():
            cmd += arg.export()
        return cmd

    def export_config(self):
        d = { 'name': self.get_name() }
        if self.__type == self.__SCRIPTTYPE:
            d['script'] = self.__script
        elif self.__type == self.__PYTHONTYPE:
            d['python'] = self.__python
        elif self.__type == self.__RUBYTYPE:
            d['ruby'] = self.__ruby
        elif self.__type == self.__JAVATYPE:
            d['java'] = self.__java
        if self.get_enabled():
            d['default'] = True
        if len(self.__args) > 0:
            d['args'] = list(arg.export_config() for arg in self.__args)
        return d

    def logfile(self, logdir):
        key = '_'.join(self.get_name().lower().split())
        logfn = os.path.join(logdir, key+'.log')
        logdn = os.path.dirname(logfn)
        if not os.path.exists(logdn):
            os.makedirs(logdn)
        return logfn

    def run_logged(self, logdir):
        if self.__proc:
            raise Exception("already running")
        cmd = self.export()
        if not cmd:
            return None
        logfn = self.logfile(logdir)
        logf = file(logfn, 'a', 0)
        logf.write('%s\n' % ' '.join(cmd))
        logf.write(''.join('%s=%s\n' % (k, v) for k, v in os.environ.iteritems()))
        p = subprocess.Popen(cmd, stdout=logf, stderr=logf)
        self.__proc = p
        return p

    def get_pid(self):
        if self.__proc:
            return self.__proc.pid
        return None

    def kill(self):
        if self.__proc:
            signal = getattr(sig, 'SIG'+self.get_signal())
            os.kill(self.__proc.pid, signal)
            os.waitpid(self.__proc.pid, 0)
            self.__proc = None

USE_DEFAULT = '##USE DEFAULT##'
NO_VALUE   =  '##NO VALUE##'
UNSET = None

class DaemonArg(object):

    def __init__(self, name, short=None, long=None, default=None, password=False, boolean=False):
        self.__name = None
        self.__short = None
        self.__long = None
        self.__default = None
        self.__password = False
        self.__boolean = bool(boolean)
        self.__value = None
        self.set_name(name)
        self.set_short(short)
        self.set_long(long)
        self.set_default(default)
        self.set_password(password)
        if self.__default is None:
            self.set_value(NO_VALUE)
        else:
            self.set_value(USE_DEFAULT)

    def set_name(self, name):
        self.__name = str(name)

    def get_name(self):
        return self.__name

    def set_short(self, short):
        if short is not None:
            short = str(short)
            while short.startswith('-'):
                short = short[1:]
            short = '-'+short[0]
            self.__short = short
        else:
            self.__short = None

    def get_short(self):
        return self.__short

    def set_long(self, lng):
        if lng is not None:
            lng = str(lng)
            if not lng.startswith('-'):
                lng = '--'+lng
            self.__long = lng
        else:
            self.__long = None

    def get_long(self):
        return self.__long

    def set_default(self, val):
        self.__default = self.__allowed_val(val)

    def get_default(self):
        return self.__default

    def set_value(self, val=USE_DEFAULT):
        self.__value = self.__allowed_val(val)

    def get_value(self):
        if self.__value == NO_VALUE:
            return None
        elif self.__value == USE_DEFAULT:
            return self.__sub_value(self.__default)
        return self.__sub_value(self.__value)

    def __sub_value(self, val):
        def repl(m):
            var = m.group(1)
            v = os.getenv(var)
            if v is None or ':' in v:
                print 'not substituting $%s (%s)' % (var, v)
                return '$'+var
            return v
        val = re.sub(r'\$([A-Za-z0-9_]+)', repl, val)
        if val.startswith('$') and '/' in val:
            var, xval = val[1:].split('/', 1)
            if var in os.environ:
                path = Sluice.get_path_file(os.environ[var], xval)
                if path is not None:
                    return path
                else:
                    print '%s not found in %s' % (xval, var)
            else:
                print 'unknown var %s' % var
        return val

    def unset_value(self):
        self.__value = UNSET

    def __allowed_val(self, val):
        if self.__boolean:
            return bool(val)
        if val is None:
            return None
        elif isinstance(val, (basestring, int, long, float)):
            return val
        elif isinstance(val, list):
            retval = []
            for v in vals:
                if isinstance(v, (basestring, int, long, float)):
                    retval.append(v)
                else:
                    raise ValueError("values may only be strings or numbers")
            return retval
        else:
            raise ValueError("values may only be strings or numbers, or lists of such")

    ## password just indicates that the UI should star it out
    def set_password(self, v):
        self.__password = bool(v)

    def get_password(self):
        return self.__password

    def get_boolean(self):
        return self.__boolean

    def export(self):
        if self.__value == UNSET or self.__value is None:
            return []
        flag = self.__long
        if flag is None:
            flag = self.__short
        if self.__boolean:
            if self.__value:
                return [flag]
            return []
        val = self.get_value()
        if val is None:
            return [self.__name]
        #if self.__value == USE_DEFAULT:
        #    val = self.__default
        #else:
        #    val = self.__value
        if isinstance(val, list):
            args = []
            for v in self.__value:
                args += [flag, v]
            return args
        return [flag, val]

    def export_config(self):
        d = { 'name': self.get_name() }
        if self.__short is not None:
            d['short'] = self.__short
        if self.__long is not None:
            d['long'] = self.__long
        if self.__default is not None:
            d['default'] = self.__default
        if self.__password is not None:
            d['password'] = self.__password
        if self.__boolean is not None:
            d['boolean'] = self.__boolean
        return d

