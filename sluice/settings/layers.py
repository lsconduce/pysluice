import os
from copy import deepcopy
from collections import OrderedDict
from plasma import Protein
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
from sluice import Sluice
from sluice.settings.attributes import Attribute

class StaticLayers(object):
    def __init__(self, id, layers=None):
        self.__id = id
        self.__file = None
        self.__layers = OrderedDict()
        if layers:
            for key, layer in layers.iteritems():
                self.add_layer(key, layer)

    def get_demo_id(self):
        return self.__id

    def __contains__(self, k):
        return k in self.__layers

    def __getitem__(self, k):
        return self.__layers[k]

    def __setitem__(self, k, v):
        return self.add_layer(k, v)

    def __delitem__(self, k):
        del(self.__layers[k])

    def __len__(self):
        return len(self.__layers)

    def get(self, k, default=None):
        return self.__layers.get(k, default)

    def get_file(self):
        if self.__file is None:
            #default path if none provided
            fn = os.path.join(self.get_demo_id(), 'static-layers.protein')
        else:
            fn = self.__file
        static_path = Sluice.get_etc_file(fn)
        if static_path is not None:
            return fn
        #the default if all else fails
        return 'static-layers.protein'

    def load_from_protein(self, static_file=None):
        """
        Loads config from a file and sets the static file
        """
        #TODO: Make static_file a required input
        if static_file is None:
            static_file = self.get_file()
        static_path = Sluice.get_etc_file(static_file)
        if static_path is None:
            raise Exception("static layers not found")
        if self.__file is None:
            #we are loading something, so that is our file
            self.__file = static_file
        p = parse_yaml_protein(open(static_path, 'r'))
        for k, v in p.ingests().iteritems():
            self.add_layer(k, v)

    def has_layers(self):
        return len(self.__layers) > 0

    def add_layer(self, key, layer):
        if 'name' not in layer:
            words = list()
            for word in key.split('_'):
                if len(word) <= 2:
                    words.append(word.upper())
                else:
                    words.append(word[0].upper()+word[1:])
            layer['name'] = ' '.join(words)
        if 'bl' not in layer:
            layer['bl'] = [layer['lat_min'], layer['lon_min']]
        if 'tr' not in layer:
            layer['tr'] = [layer['lat_max'], layer['lon_max']]
        if 'image' not in layer:
            layer['image'] = layer['image_file']
        if 'curtain_min_zoom_level' not in layer:
            layer['curtain_min_zoom_level'] = None
        if 'curtain_color' not in layer:
            layer['curtain_color'] = None
        if 'curtain_fade_time' not in layer:
            layer['curtain_fade_time'] = None
        l = StaticLayer(self.get_demo_id(),
                        layer['name'], layer['image'],
                        layer['bl'], layer['tr'],
                        layer.get('default'),
                        layer['curtain_min_zoom_level'],
                        layer['curtain_color'],
                        layer['curtain_fade_time'])
        self.__layers[key] = l
        return l

    def attribute(self):
        attr = Attribute('layers', True)
        for k, l in self.__layers.iteritems():
            attr.add_option(k, l.get_name(), l.enabled())
        return attr

    def export(self):
        return dict((k, l.export()) for k, l in self.__layers.iteritems())

    def export_config(self):
        return dict((k, l.export_config()) for k, l in self.__layers.iteritems())

    def save(self):
        fn = Sluice.get_etc_file(self.get_file(), True)
        p = Protein(['sluice', 'config', 'layers'], self.export())
        txt = dump_yaml_protein(p)
        dn = os.path.dirname(fn)
        if not os.path.exists(dn):
            os.makedirs(dn)
        file(fn, 'w').write(txt)
        return self.get_file()

    def load(self):
        s = Sluice()
        s.remove_all_static_layers()
        for k, l in self.__layers.iteritems():
            bl, tr = l.get_bounds()
            s.add_static_layer(k, l.get_image(), bl, tr,
                l.curtain_min_zoom_level(), l.curtain_color(),
                l.curtain_fade_time())

class StaticLayer(object):

    def __init__(self, id, name, image, bl, tr, enabled=False,
                 curtain_min_zoom_level=None, curtain_color=None,
                 curtain_fade_time=None):
        self.__id = id
        self.set_name(name)
        self.set_image(image)
        self.set_bounds(bl, tr)
        self.__curtain_min_zoom_level = curtain_min_zoom_level
        self.__curtain_color = curtain_color
        self.__curtain_fade_time = curtain_fade_time
        self.__enabled = bool(enabled)

    def get_demo_id(self):
        return self.__id

    def set_name(self, name):
        self.__name = str(name)

    def get_name(self):
        return self.__name

    def enable(self):
        self.__enabled = True

    def disable(self):
        self.__enabled = False

    def enabled(self):
        return self.__enabled

    def curtain_min_zoom_level(self):
        return self.__curtain_min_zoom_level

    def curtain_color(self):
        return self.__curtain_color

    def curtain_fade_time(self):
        return self.__curtain_fade_time

    def set_image(self, image):
        fn = str(image)
        if Sluice.get_share_file(fn) is not None:
            self.__image = fn
            return fn
        fn = os.path.join(self.get_demo_id(), str(image))
        if Sluice.get_share_file(fn) is not None:
            self.__image = fn
            return fn
        raise Exception("image %s not found" % image)

    def get_image(self):
        return self.__image

    def __make_latlon(self, pt):
        v = None
        if isinstance(pt, (list, tuple)) and len(pt) == 2:
            v = [float(pt[0]), float(pt[1])]
        elif isinstance(pt, dict):
            if 'lat' in pt and 'lon' in pt:
                v = [float(pt['lat']), float(pt['lon'])]
            elif 'lat' in pt and 'lng' in pt:
                v = [float(pt['lat']), float(pt['lng'])]
        elif hasattr(pt, 'lat') and hasattr(pt, 'lon'):
            lat = pt.lat
            lon = pt.lon
            if hasattr(lat, '__call__'):
                lat = lat()
            if hasattr(lon, '__call__'):
                lon = lon()
            v = [float(lat), float(lon)]
        else:
            raise Exception("not a lat/lon point")
        if v[0] < -90.0 or v[0] > 90.0:
            raise Exception("lat not between -90 and 90 degrees")
        if v[1] < -360.0 or v[1] > 360.0:
            raise Exception("lat not between -360 and 360 degrees")
        return v

    def set_bounds(self, bl, tr):
        self.__bl = self.__make_latlon(bl)
        self.__tr = self.__make_latlon(tr)

    def get_bounds(self):
        return self.__bl, self.__tr

    def export(self):
        return {
            'image_file': self.__image,
            'lat_min': self.__bl[0],
            'lat_max': self.__tr[0],
            'lon_min': self.__bl[1],
            'lon_max': self.__tr[1],
            'curtain_min_zoom_level': self.__curtain_min_zoom_level,
            'curtain_color': self.__curtain_color,
            'curtain_fade_time': self.__curtain_fade_time
        }

    def export_config(self):
        return deepcopy({
            'name': self.__name,
            'image': self.__image,
            'bl': self.__bl,
            'tr': self.__tr,
            'curtain_min_zoom_level': self.__curtain_min_zoom_level,
            'curtain_color': self.__curtain_color,
            'curtain_fade_time': self.__curtain_fade_time,
            'default': self.__enabled
        })

