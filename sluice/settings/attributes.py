import itertools
from collections import OrderedDict

class Attributes(object):
    def __init__(self, attrs=None):
        self.__attrs = dict()
        self.load(attrs)

    def __repr__(self):
        return self.__attrs.__repr__()

    def __delitem__(self, k):
        del(self.__attrs[k])

    def __contains__(self, k):
        return k in self.__attrs

    def __iter__(self):
        return iter(self.__attrs)

    def __len__(self):
        return len(self.__attrs)

    def __getitem__(self, k):
        return self.__attrs[k].value()

    def __setitem__(self, k, v):
        ## may raise a KeyError
        self.__attrs[k].set_value(v)

    def get(self, k, default=None):
        v = self.__attrs.get(k)
        if v is None:
            return default
        return v.value()

    def describe(self, k):
        return self.__attrs[k]

    def pop(self, *args, **kwargs):
        return self.__attrs.pop(*args, **kwargs)

    def popitem(self, *args, **kwargs):
        return self.__attrs.popitem(*args, **kwargs)

    def keys(self):
        return self.__attrs.keys()

    def iterkeys(self):
        return self.__attrs.iterkeys()

    def get_attributes(self):
        return self.__attrs.values()

    def values(self):
        return list(attr.value() for attr in self.__attrs.itervalues())

    def itervalues(self):
        for attr in self.__attrs.itervalues():
            yield attr.value()

    def items(self):
        return zip(self.keys(), self.values())

    def iteritems(self):
        return itertools.izip(self.iterkeys(), self.itervalues())

    def load(self, attrs):
        if isinstance(attrs, list):
            ## fluoro config protein
            for attr in attrs:
                self.add(attr)
        elif isinstance(attrs, dict):
            ## demo config json
            for name, attr in attrs.iteritems():
                self.add(name, attr['options'], attr['value'])

    def update(self, *args, **kwargs):
        ## like dict.update()
        if len(args) == 1:
            d = args[0]
            if hasattr(d, 'keys'):
                for k in d:
                    attr = self.__attrs.get(k)
                    if attr is not None:
                        attr.set_value(d[k])
            else:
                for (k, v) in d:
                    attr = self.__attrs.get(k)
                    if attr is not None:
                        attr.set_value(v)
        for k, v in kwargs.iteritems():
            attr = self.__attrs.get(k)
            if attr is not None:
                attr.set_value(v)

    def add(self, attr, options=None, value=None):
        if isinstance(attr, Attribute):
            if value is not None:
                attr.set_value(value)
            self.__attrs[attr.get_name()] = attr
        elif isinstance(attr, basestring):
            name = attr
            attr = Attribute(name, isinstance(value, list), options)
            if value is not None:
                attr.set_value(value)
            self.__attrs[attr.get_name()] = attr
        elif isinstance(attr, dict):
            name = attr.get('name')
            options = attr.get('contents', [])
            attr = Attribute(name, attr.get('selection-type') == 'inclusive')
            for opt in options:
                attr.add_option(opt.get('name'), opt.get('display-text', opt.get('name')), opt.get('selected', False))
            if value is not None:
                attr.set_value(value)
            self.__attrs[attr.get_name()] = attr
        else:
            raise Exception("attr must be a string or a dict")

    def export(self):
        return list(a.export() for a in sorted(self.get_attributes(), key=lambda x: x.get_name()))

    def export_config(self):
        return dict((a.get_name(), a.export_config()) for a in self.get_attributes())

    def export_values(self):
        return dict((k, v) for k, v in self.iteritems())

class Attribute(object):
    def __init__(self, name, inclusive=False, options=None):
        self.__name = str(name)
        self.__options = OrderedDict()
        if inclusive:
            self.__value = set()
        else:
            self.__value = None
        if options is not None:
            for opt in options:
                self.add_option(opt)

    def __delitem__(self, k):
        del(self.__options[k])
        if self.inclusive():
            self.__value.discard(k)
        elif self.__value == k:
            self.set_value(k)

    def __len__(self):
        return len(self.__options)

    def __getitem__(self, k):
        return self.__options[k]

    def __setitem__(self, k, v):
        k = self.__canonical_value(k)
        self.__options[k] = str(v)

    def __iter__(self):
        return iter(self.__options)

    def __contains__(self, k):
        return k in self.__options

    def keys(self):
        return self.__options.keys()

    def iterkeys(self):
        return self.__options.iterkeys()

    def values(self):
        return self.__options.values()

    def itervalues(self):
        return self.__options.itervalues()

    def items(self):
        return self.__options.items()

    def iteritems(self):
        return self.__options.iteritems()

    def pop(self, k):
        if k in self.__options:
            del(self.__options[k])

    def inclusive(self):
        return isinstance(self.__value, set)

    def get_name(self):
        return self.__name

    def set_name(self, val):
        self.__name = str(val)

    def __canonical_value(self, v):
        if v is None:
            return None
        if isinstance(v, (int, long)):
            return int(v)
        if isinstance(v, bool):
            return bool(v)
        if isinstance(v, float):
            return float(v)
        if isinstance(v, basestring):
            return unicode(v)
        return str(v)

    def add_option(self, val, text=None, selected=False):
        if isinstance(val, dict):
            if text is None:
                found = False
                for k in ('text', 'display-text', 'value', 'name'):
                    if k in val and val.get(k) is not None:
                        text = str(val[k])
                        found = True
                        break
                if not found:
                    raise Exception("no value or display text provided")
            if 'selected' in val:
                selected = val['selected']
            found = False
            for k in ('value', 'name'):
                if k in val and val.get(k) is not None:
                    val = val[k]
                    found = True
                    break
            if not found:
                raise Exception("no value provided")
            val = self.__canonical_value(val)
            self.__options[val] = text
            if selected:
                self.select_value(val)
        elif isinstance(val, (basestring, int, float, bool)):
            val = self.__canonical_value(val)
            if text is None:
                text = str(val)
            self.__options[val] = text
            if selected or (not self.inclusive() and len(self.__options) == 1):
                self.select_value(val)
        else:
            raise Exception("unacceptable value")

    def set_value(self, val):
        if self.inclusive():
            if not isinstance(val, (set, list, tuple)):
                val = [val]
            self.__value = set()
            for v in val:
                v = self.__canonical_value(v)
                if v in self.__options:
                    self.__value.add(v)
        else:
            val = self.__canonical_value(val)
            if val in self.__options:
                self.__value = val
            else:
                if len(self.__options) == 0:
                    self.__value = None
                else:
                    self.__value = self.__options.keys()[0]

    def select_value(self, val):
        if self.inclusive():
            val = self.__canonical_value(val)
            if val in self.__options:
                self.__value.add(val)
        else:
            self.set_value(val)

    def deselect_value(self, val):
        if self.inclusive():
            self.__value.discard(self.__canonical_value(val))
        else:
            self.set_value(self.__options.keys()[0])

    def value(self):
        if self.inclusive():
            return list(v for v in self.__options.keys() if v in self.__value)
        else:
            return self.__value

    def export(self):
        vals = self.value()
        if self.inclusive():
            seltype = 'inclusive'
        else:
            seltype = 'exclusive'
            vals = set([vals])
        return {
            'name': self.__name,
            'selection-type': seltype,
            'contents': list(
                { 'name': k, 'display-text': v, 'selected': (k in vals) }
                for k, v in self.__options.iteritems()
            )
        }

    def export_config(self):
        opts = list()
        for k, v in self.__options.iteritems():
            opts.append({ 'value': k, 'text': v })
        return { 'options': opts, 'value': self.value() }

