from sluice import Sluice

class Bookmark(object):
    def __init__(self, name, lat, lon, zoom):
        self.__name = None
        self.__lat = None
        self.__lon = None
        self.__zoom = None
        self.set_name(name)
        self.set_lat(lat)
        self.set_lon(lon)
        self.set_zoom(zoom)

    def set_name(self, name):
        self.__name = str(name)

    def get_name(self):
        return self.__name

    def set_lat(self, lat):
        lat = float(lat)
        if lat < -90 or lat > 90:
            raise ValueError("latitude must be between -90 and 90 degrees")
        self.__lat = lat

    def get_lat(self):
        return self.__lat

    def set_lon(self, lon):
        self.__lon = float(lon)

    def get_lon(self):
        return self.__lon

    def set_zoom(self, zoom):
        zoom = float(zoom)
        if zoom < 1:
            raise ValueError("zoom must be at least 1.0")
        self.__zoom = zoom

    def get_zoom(self):
        return self.__zoom

    def export(self):
        return {
            'name': self.get_name(),
            'lat': self.get_lat(),
            'lon': self.get_lon(),
            'level': self.get_zoom()
        }

    def export_config(self):
        return {
            'name': self.get_name(),
            'lat': self.get_lat(),
            'lon': self.get_lon(),
            'zoom': self.get_zoom()
        }

    def load(self):
        Sluice().request_zoom_loc(self.get_lat(), self.get_lon(), self.get_zoom())

