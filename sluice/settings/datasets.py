import os, subprocess
from sluice import Sluice, NETWORK_REQUEST_POOL

class Dataset(object):

    def __init__(self, id, name, topo=None, obs=None, default=False):
        self.__id = id
        self.__name = None
        self.__topo = []
        self.__obs = []
        self.__enabled = False
        self.set_name(name)
        if topo is not None:
            if isinstance(topo, list):
                for t in topo:
                    self.add_topo(t)
            else:
                self.add_topo(topo)
        if obs is not None:
            if isinstance(obs, list):
                for o in obs:
                    self.add_obs(o)
            else:
                self.add_obs(obs)
        if default:
            self.enable()

    def get_demo_id(self):
        return self.__id

    def set_name(self, name):
        self.__name = str(name)

    def get_name(self):
        return self.__name

    def add_topo(self, fn):
        self.__topo.append(self.__find_data(fn))

    def get_topo(self):
        return self.__topo

    def add_obs(self, fn):
        self.__obs.append(self.__find_data(fn))

    def get_obs(self):
        return self.__obs

    def __find_data(self, fn):
        if not fn.startswith('/'):
            if not fn.endswith('.protein') and not fn.endswith('.prot') and not fn.endswith('.yaml'):
                fn += '.protein'
            ffn = fn
            path = Sluice.get_share_file(ffn)
            if path is None:
                ffn = os.path.join(self.get_demo_id(), fn)
                path = Sluice.get_share_file(ffn)
            if path is None:
                ffn = os.path.join(self.get_demo_id(), 'data', fn)
                path = Sluice.get_share_file(ffn)
            if path is None:
                ffn = os.path.join(self.get_demo_id(), 'data', 'topo', fn)
                path = Sluice.get_share_file(ffn)
            if path is None:
                ffn = os.path.join(self.get_demo_id(), 'data', 'obs', fn)
                path = Sluice.get_share_file(ffn)
            if path is None:
                raise Exception("topo file %s not found" % fn)
            fn = ffn
        elif not os.path.exists(fn):
            raise Exception("topo file %s not found" % fn)
        return fn

    def enable(self):
        self.__enabled = True

    def disable(self):
        self.__enabled = False

    def get_enabled(self):
        return self.__enabled

    def export(self):
        if self.get_enabled():
            return self.__topo + self.__obs
        return []

    def export_config(self):
        return {
            'name': self.get_name(),
            'default': self.get_enabled(),
            'topo': self.__topo,
            'obs': self.__obs
        }

    def load(self):
        #print 'loading dataset %s' % self.get_name()
        for fn in self.export():
            fn = Sluice.get_share_file(fn)
            #print 'poke %s %s' % (NETWORK_REQUEST_POOL, fn)
            cmd = ['poke', NETWORK_REQUEST_POOL, fn]
            p = subprocess.Popen(cmd)
            p.communicate()
            #os.waitpid(p.pid, 0)



