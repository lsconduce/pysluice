#!/usr/bin/env python

import datetime, time
from copy import deepcopy
from sluice.api import Sluice
from plasma.yamlio import parse_yaml_proteins

DEBUG = False

class DBWrapper(object):
    def __init__(self, settings='database-settings.protein', **kwargs):
        fn = Sluice.get_etc_file(settings)
        self.__kwargs = dict()
        if fn:
            data = parse_yaml_proteins(file(fn)).next()
            for k,v in data.ingests().iteritems():
                self.__kwargs[k] = v
        self.__kwargs.update(kwargs)
        self.__engine = self.__kwargs.pop('engine', 'mysql')
        self.__last_call = None
        self.__db = None
        self.__cur = None
        self.__max_idle = 300
        self.connect()

    def connect(self):
        if self.__last_call is None or self.__db is None or self.__cur is None:
            return self.__connect()
        if time.time() - self.__last_call >= self.__max_idle:
            try:
                self.__db.close()
            except:
                pass
            self.__db = None
            self.__cur = None
            self.__last_call = None
            return self.__connect()
        return self.__cur

    def __connect(self):
        if self.__engine == 'mysql':
            return self.__connect_mysql()
        if self.__engine == 'postgres':
            return self.__connect_postgres()

    def __connect_mysql(self):
        import MySQLdb, MySQLdb.cursors
        kwargs = self.__kwargs
        self.__db = MySQLdb.connect(charset='utf8', **kwargs)
        self.__cur = self.__db.cursor(MySQLdb.cursors.DictCursor)
        self.__last_call = time.time()
        return self.__cur

    def __connect_postgres(self):
        import psycopg2, psycopg2.extras
        kwargs = deepcopy(self.__kwargs)
        if 'db' in kwargs:
            kwargs['database'] = kwargs.pop('db')
        if 'passwd' in kwargs:
            kwargs['password'] = kwargs.pop('passwd')
        self.__db = psycopg2.connect(**kwargs)
        self.__cur = self.__db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.__last_call = time.time()
        return self.__cur

    def commit(self):
        return self.__db.commit()

    def rollback(self):
        return self.__db.rollback()

    def query(self, sql, *args):
        cur = self.connect()
        if DEBUG:
            print sql
        cur.execute(sql, args)
        return cur

    def quote(self, v):
        if v is None:
            return 'NULL'
        if isinstance(v, (int, long, float)):
            return str(v)
        if isinstance(v, datetime.datetime):
            return self.quote(v.strftime('%Y-%m-%d %H:%M:%S'))
        if isinstance(v, datetime.date):
            return self.quote(v.strftime('%Y-%m-%d'))
        if isinstance(v, basestring):
            return "'%s'" % self.__db.escape_string(v.encode('utf-8'))
        if isinstance(v, (list, tuple)):
            return ','.join(self.quote(x) for x in v)
        return self.quote(u'%s' % v)

    def op__eq(self, k, v):
        return '%s = %s' % (k, self.quote(v))

    def op_ne(self, k, v):
        return '%s != %s' % (k, self.quote(v))

    def op__gt(self, k, v):
        return '%s > %s' % (k, self.quote(v))

    def op__gte(self, k, v):
        return '%s >= %s' % (k, self.quote(v))

    def op__lt(self, k, v):
        return '%s < %s' % (k, self.quote(v))

    def op__lte(self, k, v):
        return '%s <= %s' % (k, self.quote(v))

    def op__in(self, k, v):
        if not isinstance(v, (list, tuple)):
            v = [v]
        return '%s IN (%s)' % (k, ','.join(self.quote(x) for x in v))

    def op__notin(self, k, v):
        if not isinstance(v, (list, tuple)):
            v = [v]
        return '%s NOT IN (%s)' % (k, ','.join(self.quote(x) for x in v))

    def op__startswith(self, k, v):
        return '%s LIKE %s' % (k, self.quote(v+'%'))

    def op__notstartswith(self, k, v):
        return '%s NOT LIKE %s' % (k, self.quote(v+'%'))

    def op__endswith(self, k, v):
        return '%s LIKE %s' % (k, self.quote('%'+v))

    def op__notendswith(self, k, v):
        return '%s NOT LIKE %s' % (k, self.quote('%'+v))

    def op__isnull(self, k, v):
        if v:
            return '%s IS NULL' % k
        return '%s IS NOT NULL' % k

    def agg__count(self, k, t=None):
        if not k:
            k = '*'
        if t:
            k = '%s.%s' % (k, t)
        return 'COUNT(%s)' % k

    def agg__sum(self, k, t=None):
        return 'SUM(%s)' % k

    def agg__max(self, k):
        if t:
            k = '%s.%s' % (k, t)
        return 'MAX(%s)' % k

    def agg__min(self, k):
        if t:
            k = '%s.%s' % (k, t)
        return 'MIN(%s)' % k

    def make_select(self, columns, groupby=None, orderby=None, limit=None, offset=None, **kwargs):
        select = list()
        tables = list()
        joins = list()
        for table, cols in columns.iteritems():
            if table == '__raw':
                for name, raw in cols.iteritems():
                    select.append('%s AS %s' % (raw, name))
            else:
                if isinstance(table, tuple):
                    prefix = table[1]
                else:
                    prefix = table
                join = list()
                for col in cols:
                    if isinstance(col, tuple):
                        if '__' in col[0]:
                            (x, agg) = col[0].rsplit('__', 1)
                            if hasattr(self, 'agg__%s' % agg):
                                select.append('%s AS %s' % (getattr(self, 'agg__%s'% agg)(x, prefix)), col[1])
                            else:
                                select.append('%s.%s AS %s' % (prefix, col[0], col[1]))
                        else:
                            select.append('%s.%s AS %s' % (prefix, col[0], col[1]))
                    elif isinstance(col, dict):
                        for k, v in col.iteritems():
                            if isinstance(v, dict):
                                for c, r in v.iteritems():
                                    join.append('%s.%s = %s.%s' % (prefix, c, k, r))
                            else:
                                join.append('%s.%s = %s.%s' % (prefix, v, k, v))
                    else:
                        if '__' in col:
                            (x, agg) = col.rsplit('__', 1)
                            if hasattr(self, 'agg__%s' % agg):
                                select.append(getattr(self, 'agg__%s'% agg)(x, prefix))
                            else:
                                select.append('%s.%s' % (prefix, col))
                        else:
                            select.append('%s.%s' % (prefix, col))
                if join:
                    if isinstance(table, tuple):
                        joins.append(' LEFT OUTER JOIN %s AS %s ON %s' % (table[0], table[1], ' AND '.join(join)))
                    else:
                        joins.append(' LEFT OUTER JOIN %s ON %s' % (table, ' AND '.join(join)))
                else:
                    if isinstance(table, tuple):
                        tables.append('%s AS %s' % (table[0], table[1]))
                    else:
                        tables.append(table)
        where = self.make_where_clause(**kwargs)
        groupby = self.make_group_clause(groupby)
        having = self.make_having_clause(**kwargs)
        orderby = self.make_order_clause(orderby)
        limit = self.make_limit_clause(limit, offset)
        sql = "SELECT %s FROM %s" % (','.join(select), ','.join(tables))
        if joins:
            sql += ''.join(joins)
        if where:
            sql += where
        if groupby:
            sql += groupby
        if having:
            sql += having
        if orderby:
            sql += orderby
        if limit:
            sql += limit
        return sql

    def make_where_clause(self, **kwargs):
        ands = list()
        for k, v in kwargs.iteritems():
            if k == '__where':
                ands.append(v)
                continue
            if k == '__having':
                continue
            parts = k.split('__')
            op = 'op__%s' % parts[-1]
            if hasattr(self, op):
                parts.pop()
            else:
                op = 'op__eq'
            agg = 'agg__%s' % parts[-1]
            if hasattr(self, agg):
                continue
            ands.append(getattr(self, op)('.'.join(parts), v))
        if not ands:
            return None
        return ' WHERE %s' % ' AND '.join(ands)

    def make_having_clause(self, **kwargs):
        ands = list()
        for k, v in kwargs.iteritems():
            if k == '__where':
                continue
            if k == '__having':
                ands.append(v)
                continue
            parts = k.split('__')
            op = 'op__%s' % parts[-1]
            if hasattr(self, op):
                parts.pop()
            else:
                op = 'op__eq'
            agg = 'agg__%s' % parts[-1]
            if hasattr(self, agg):
                parts.pop()
                k = getattr(self, agg)('.'.join(parts))
            else:
                continue
            ands.append(getattr(self, op)(k, v))
        if not ands:
            return None
        return ' HAVING %s' % ' AND '.join(ands)

    def make_group_clause(self, groupby):
        if not groupby:
            return None
        if not isinstance(groupby, (list, tuple)):
            groupby = [groupby,]
        return ' GROUP BY %s' % ','.join(groupby)

    def make_order_clause(self, orderby):
        if not orderby:
            return None
        if not isinstance(orderby, (list, tuple)):
            orderby = [orderby,]
        cols = []
        for col in orderby:
            if col.startswith('-'):
                cols.append('% DESC' % col[1:])
            else:
                cols.append('%s ASC' % col)
        return ' ORDER BY %s' % ','.join(cols)

    def make_limit_clause(self, limit, offset):
        if not limit:
            return None
        if offset:
            return ' LIMIT %d OFFSET %d' % (limit, offset)
        return ' LIMIT %d' % limit

    def get_cols(self, cols, obj):
        if not cols:
            if isinstance(obj, dict):
                cols = obj.keys()
            elif isinstance(obj, list):
                pass
            elif isinstance(obj, namedtuple):
                cols = obj._adsict().keys()
            else:
                cols = list()
                for k, v in obj.__dict__.iteritems():
                    if not v.startswith('_') and not hasattr(v, '__call__'):
                        cols.append(k)
        return cols

    def get_vals(self, cols, obj):
        if isinstance(obj, dict):
            args = list(obj.get(x) for x in cols)
        elif isinstance(obj, list):
            if cols:
                args = list()
                for i in xrange(len(cols)):
                    if i >= len(obj):
                        args.append(None)
                    else:
                        args.append(obj[i])
        else:
            args = list(getattr(obj, col, None) for col in cols)
        return args

    def insert(self, table, cols, obj):
        cols = self.get_cols(cols, obj)
        if cols:
            sql = "INSERT INTO %s (%s) VALUES(%s)" % (table, ', '.join(cols), ', '.join('%s' for x in cols))
        else:
            sql = "INSERT INTO %s VALUES(%s)" % (table, ', '.join(cols), ', '.join('%s' for x in xrange(len(cols))))
        if self.__engine == 'postgres':
            sql += ' RETURNING id'
        args = self.get_vals(cols, obj)
        cur = self.query(sql, *args)
        if self.__engine == 'mysql':
            return self.__db.insert_id()
        elif self.__engine == 'postgres':
            return cur.fetchone()[0]

    def update(self, table, idcols, cols, obj):
        cols = self.get_cols(cols, obj)
        where = self.get_cols(idcols, obj)
        sql = "UPDATE %s SET %s WHERE %s" % (table, ', '.join('%s = %%s' % col for col in cols), ' AND '.join('%s = %%s' % col for col in where))
        vals = self.get_vals(cols+where, obj)
        return self.query(sql, *vals)

    def load_sales(self, **kwargs):
        sql = """
SELECT s.ID_SALES AS id,
       t.TIME_DATE AS timestamp,
       p.PRICE_DOLLARS AS Product_Price,
       IF(p.PRODUCT_WEIGHT_UNITS LIKE 'oz%', p.PRODUCT_WEIGHT/16.0, p.PRODUCT_WEIGHT) AS Product_Weight,
       c.ID_CUSTOMER AS custid,
       g.LATITUDE AS lat,
       g.LONGITUDE AS lng
  FROM fact_sales s
  LEFT OUTER JOIN product p ON s.ID_PRODUCT = p.ID_PRODUCT
  LEFT OUTER JOIN customer c ON s.ID_CUSTOMER = c.ID_CUSTOMER
  LEFT OUTER JOIN age a ON c.ID_AGE = a.ID_AGE
  LEFT OUTER JOIN education e ON c.ID_EDUCATION = e.ID_EDUCATION
  LEFT OUTER JOIN income i ON c.ID_INCOME = i.ID_INCOME
  LEFT OUTER JOIN industry n ON c.ID_INDUSTRY = n.ID_INDUSTRY
  LEFT OUTER JOIN marital_status m ON c.ID_MARITAL_STATUS = m.ID_MARITAL_STATUS
  LEFT OUTER JOIN occupation o ON c.ID_OCCUPATUON = o.ID_OCCUPATION
  LEFT OUTER JOIN geography g ON c.ID_GEOGRAPHY = g.ID_GEOGRAPHY
  LEFT OUTER JOIN time t on s.ID_TIME = t.ID_TIME
  WHERE g.STATECODE = 'CA'
 LIMIT 1000
        """
        for row in self.query(sql):
            yield Sale(**row)

