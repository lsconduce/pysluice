from copy import deepcopy
import math
from collections import namedtuple
from itertools import izip

D2R = math.pi / 180
EARTH_RADIUS = 6378.1 ## Earth radius, in kilometers
INFINITY = float('inf')

def make_point(v):
    if isinstance(v, Point):
        return v
    if isinstance(v, (list, tuple)) and len(v) >= 2:
        return Point(v[0], v[1])
    if isinstance(v, dict) and 'x' in v and 'y' in v:
        return Point(v['x'], v['y'])
    if hasattr(v, 'x') and hasattr(v, 'y'):
        return Point(v.x, v.y)
    raise ValueError("Can't convert %s to a Location" % v)

def make_location(v):
    if isinstance(v, Location):
        return v
    if isinstance(v, (list, tuple)) and len(v) >= 2:
        return Location(v[0], v[1])
    if isinstance(v, dict) and 'lat' in v and ('lon' in v or 'lng' in v):
        return Location(v['lat'], v.get('lon', v.get('lng')))
    if hasattr(v, 'lat'):
        if hasattr(v, 'lon'):
            return Location(v.lat, v.lon)
        if hasattr(v, 'lng'):
            return Location(v.lat, v.lng)
    raise ValueError("Can't convert %s to a Location" % v)

def make_box(v):
    if isinstance(v, Box):
        return v
    if isinstance(v, (list, tuple)):
        if len(v) >= 4:
            return Box(Location(v[0], v[1]), Location(v[2], v[3]))
        if len(v) >= 2:
            return Box(make_location(v[0]), make_location(v[1]))
    if isinstance(v, dict):
        t = v.get('top', v.get('t'))
        b = v.get('bottom', v.get('bot', v.get('b')))
        l = v.get('left', v.get('l'))
        r = v.get('right', v.get('r'))
        if t is not None and b is not None and l is not None and r is not None:
            return Box(Location(b, l), Location(t, r))
        if 'bl' in v and 'tr' in v:
            return Box(make_locaiton(v['bl']), make_location(v['tr']))
    raise ValueError("Can't convert %s to a Box")

class Coordinate(object):

    def __getitem__(self, i):
        if isinstance(i, (int, long)):
            if 0 <= i <= 1:
                return getattr(self, self.__slots__[i])
            raise IndexError()
        return getattr(self, i)

    def __getslice__(self, i, j):
        return list(self[x] for x in xrange(i, j))

    def __len__(self):
        return 2

    def __str__(self):
        return '%s(%s=%s, %s=%s)' % (self.__class__.__name__, self.__slots__[0], self[0], self.__slots__[1], self[1])

    def __repr__(self):
        return str(self)

    def centroid(self):
        return self

class Point(Coordinate):
    __slots__ = ('x', 'y', 'lat', 'lon', 'lng')

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.lat = y
        self.lon = x
        self.lng = x

    def __add__(self, p):
        return Point(self.x + p.x, self.y+p.y)

    def destination(self, angle, distance):
        dx = distance * math.cos(angle*D2R)
        dy = distance * math.sin(angle*D2R)
        return self + Point(dx, dy)

    def get_bounding_rectangle(self, distance):
        t = self.y - distance
        b = self.y + distance
        l = self.x - distance
        r = self.x + distance
        return Box(Point(l, b), Point(r, t))

class Location(Coordinate):
    __slots__ = ('lat', 'lon', 'lng', 'x', 'y')

    def __init__(self, lat, lon):
        self.lat = lat
        self.lon = lon
        self.lng = lon
        self.x = lon
        self.y = lat

    def destination(self, bearing, distance, r=EARTH_RADIUS):
        lat1 = D2R * self.lat
        lon1 = D2R * self.lon
        b = D2R * bearing
        lat2 = math.asin(math.sin(lat1)*math.cos(distance/r) + math.cos(lat1)*math.sin(distance/r)*math.cos(b))
        lon2 = lon1 + math.atan2(math.sin(b)*math.sin(distance/r)*math.cos(lat1), math.cos(distance/r)-math.sin(lat1)*math.sin(lat2))
        return Location(lat2 / D2R, lon2 / D2R)

    def get_bounding_rectangle(self, distance):
        t = self.destination(0, distance)
        b = self.destination(180, distance)
        l = self.destination(-90, distance)
        r = self.destination(90, distance)
        return Box(Location(b.lat, l.lon), Location(t.lat, r.lon))

    @classmethod
    def from_shapefile(cls, sf_point):
        return cls(sf_point[1], sf_point[0])

class Edge(Coordinate):
    __slots__ = ('p', 'q', 'p1', 'p2')

    def __init__(self, p, q):
        self.p = p
        self.q = q
        self.p1 = p
        self.p2 = q

    def __neg__(self):
        return Edge(self.q, self.p)

    def __mul__(self, v):
        if isinstance(self.p, Location):
            r = self.p.destination(self.bearing(), v * self.distance())
            return self.__class__(self.p, r)
        if isinstance(self.p, Point):
            r = self.p.destination(self.angle(), v * self.length())
            return self.__class__(self.p, r)
        print '%s not a Location or a Point' % self.p.__class__.__name__
        return NotImplemented

    def centroid(self):
        x = (self.p.x + self.q.x) / 2.0
        y = (self.p.y + self.q.y) / 2.0
        if isinstance(self.p, Location):
            return Location(y, x)
        return Point(x, y)

    def box(self):
        t = max(self.p.y, self.q.y)
        b = min(self.p.y, self.q.y)
        l = min(self.p.x, self.q.x)
        r = max(self.p.x, self.q.x)
        if isinstance(self.p, Location):
            return Box(Location(b, l), Location(t, r))
        return Box(Point(l,b), Point(r,t))

    def slope(self):
        if(self.p.lon == self.q.lon):
            return INFINITY
        return (self.p.lat - self.q.lat) / (self.p.lon - self.q.lon)

    def intercept(self):
        m = self.slope()
        if m == INFINITY:
            return None
        return self.p.lat - (m * self.p.lon)

    def angle(self):
        return math.atan2(self.q.y - self.p.y, self.q.x - self.p.x) / D2R

    def bearing(self):
        lat1 = D2R * self.p.lat
        lat2 = D2R * self.q.lat
        lon1 = D2R * self.p.lon
        lon2 = D2R * self.q.lon
        dlat = lat2 - lat1
        dlon = lon2 - lon1
        y = math.sin(dlon) * math.cos(lat2)
        x = math.cos(lat1)*math.sin(lat2) - math.sin(lat1)*math.cos(lat2)*math.cos(dlon)
        return math.atan2(y, x) / D2R

    def length(self):
        dx = self.q.x - self.p.x
        dy = self.q.y - self.p.y
        return math.sqrt(dx*dx + dy*dy)

    def distance(self, r=EARTH_RADIUS):
        lat1 = D2R * self.p.lat
        lat2 = D2R * self.q.lat
        lon1 = D2R * self.p.lon
        lon2 = D2R * self.q.lon
        dlat = lat2 - lat1
        dlon = lon2 - lon1
        a = math.sin(dlat/2) * math.sin(dlat/2) + math.sin(dlon/2) * math.sin(dlon/2) * math.cos(lat1) * math.cos(lat2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        return r * c ## kilometers

    def orientation(self, loc):
        p = self.p
        q = self.p
        r = loc
        v = (q.lat - p.lat) * (r.lon - q.lon) - (q.lon - p.lon) * (r.lat - q.lat)
        if val == 0:
            return 0 # colinear
        if val > 0:
            return 1 # clockwise
        return -1 # counterclockwise

    def overlap(self, p):
        m = self.slope()
        box = self.box()
        if m == INFINITY:
            if p.x != self.p.x:
                return False
            return box.bl.y <= p.y <= box.tr.y
        if self.orientation(p) != 0:
            return False
        return (box.bl.y <= p.y <= box.tr.y) and (box.bl.x <= p.x <= box.tr.x)

    def intersects(self, other):
        ## http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
        o1 = self.orientation(other.p)
        o2 = self.orientation(other.q)
        o3 = other.orientation(self.p)
        o4 = other.orientation(self.q)
        if o1 != o2 and o3 != o4:
            return True
        if 0 == o1 == o2 == o3 == o4:
            ## points colinear, check if they overlap
            x1 = self.p.lon
            x2 = self.q.lon
            x3 = min(other.p.lon, other.q.lon)
            x4 = max(other.p.lon, other.q.lon)
            if (x3 <= x1 <= x4) or (x3 <= x2 <= x4):
                y1 = self.p.lat
                y2 = self.q.lat
                y3 = min(other.p.lat, other.q.lat)
                y4 = max(other.p.lat, other.q.lat)
                return (y3 <= y2 <= y4) or (y3 <= y1 <= y4)
        return False

    @classmethod
    def from_shapefile(cls, p, q):
        return cls(Location.from_shapefile(p), Location.from_shapefile(q))

class Box(Coordinate):
    __slots__ = ('bl', 'tr')

    def __init__(self, bl, tr):
        self.bl = bl
        self.tr = tr

    def centroid(self):
        x = (self.bl.x + self.tr.x) / 2.0
        y = (self.bl.y + self.tr.y) / 2.0
        if isinstance(self.tr, Location):
            return Location(y, x)
        return Point(x, y)

    def overlap(self, other):
        if isinstance(other, (Point, Location)):
            return self.__overlap_location(other)
        if isinstance(other, Box):
            return self.__overlap_box(other)
        if isinstance(other, Edge):
            return self.__overlap_edge(other)
        if isinstance(other, (Polygon, MultiPolygon)):
            return other.overlap(self)
        raise ValueError("Can't overlap a box with %s" % other)

    def __overlap_box(self, box):
        if (box.bl.lat >= self.bl.lat and box.bl.lat <= self.tr.lat) or \
                (box.tr.lat >= self.bl.lat and box.tr.lat <= self.tr.lat):
            if (box.bl.lon >= self.bl.lon and self.bl.lon <= self.trl.lon) or \
                    (box.tr.lon >= self.bl.lon and box.tr.lon < self.tr.lon):
                return True
            if box.bl.lon < self.bl.lon and box.tr.lon > self.tr.lon:
                return True
        if (box.bl.lon >= self.bl.lon and box.bl.lon <= self.tr.lon) or \
                (box.tr.lon >= self.bl.lon and box.tr.lon <= self.tr.lon):
            if (box.bl.lat >= self.bl.lat and box.bl.lat <= self.tr.lat) or \
                    (box.tr.lat >= self.bl.lat and box.tr.lat <= self.tr.lat):
                return True
            if box.bl.lat < self.bl.lat and box.tr.lat > self.tr.lat:
                return True
        if box.bl.lat <= self.bl.lat and box.tr.lat >= self.tr.lat:
            if box.bl.lon <= self.bl.lon and box.tr.lon >= self.tr.lon:
                return True
        return False

    def __overlap_location(self, loc):
        if loc.lat >= self.bl.lat and loc.lat <= self.tr.lat:
            if loc.lon >= self.bl.lon and loc.lon <= self.tr.lon:
                return True
        return False

    def __overlap_edge(self, edge):
        if self.overlap(edge.p):
            return True
        if self.overlap(edge.q):
            return True
        if edge.intersects(Edge(self.bl, Location(self.bl.lat, self.tr.lon))):
            return True
        if edge.intersects(Edge(Location(self.bl.lat, self.tr.lon), self.tr)):
            return True
        if edge.intersects(Edge(self.tr, Location(self.tr.lat, self.bl.lon))):
            return True
        if edge.intersects(Edge(Location(self.tr.lat, self.bl.lon), self.bl)):
            return True
        return False

    @classmethod
    def from_bbox(cls, bbox):
        return cls((bbox[1], bbox[0]), (bbox[3], bbox[2]))
 
    @classmethod
    def from_shape(cls, shape):
        return cls.from_bbox(shape.bbox)

    def expand(self, other):
        if isinstance(other, (Point, Location)):
            return self.__expand_location(other)
        if isinstance(other, Box):
            return self.__expand_box(other)
        raise ValueError("Can't expand a box with %s" % other)

    def __expand_location(self, loc):
        t = max(self.tr.lat, loc.lat)
        b = min(self.bl.lat, loc.lat)
        l = min(self.bl.lon, loc.lon)
        r = max(self.bl.lon, loc.lon)
        if isinstance(self.tr, Location):
            return Box(Location(b,l), Location(t,r))
        return Box(Point(l,b), Point(r, t))

    def __expand_box(self, box):
        t = max(self.tr.lat, box.tr.lat)
        b = min(self.bl.lat, box.bl.lat)
        l = min(self.bl.lon, box.bl.lon)
        r = max(self.tr.lon, box.tr.lon)
        if isinstance(self.tr, Location):
            return Box(Location(b,l), Location(t,r))
        return Box(Point(l,b), Point(r, t))

class Polygon(object):
    def __init__(self, vertices=None):
        self.vertices = list()
        self._box = None
        if vertices is not None:
            for v in vertices:
                self.append(v)

    def __str__(self):
        return '%s(%d)' % (self.__class__.__name__, len(self))

    def __len__(self):
        return len(self.vertices)

    def kind(self):
        return self.vertices[0].__class__

    def area(self):
        for edge in self.iteredges():
            xy1 = edge.p.x * edge.q.y
            xy2 = edge.p.y * edge.q.x
            xy = xy1 - xy2
            a += xy
        return a/2.0

    def centroid(self):
        cx = 0
        cy = 0
        a = 0
        for edge in self.iteredges():
            xy1 = edge.p.x * edge.q.y
            xy2 = edge.p.y * edge.q.x
            xy = xy1 - xy2
            a += xy
            x = edge.p.x + edge.q.x
            y = edge.p.y + edge.q.y
            cx += (x * xy)
            cy += (y * xy)
        a /= 2.0
        cx /= (6 * a)
        cy /= (6 * a)
        if self.kind() == Location:
            return Location(cy, cx)
        return Point(cx, cy)

    def append(self, v):
        l = make_location(v)
        if self._box is None:
            self._box = Box(l, l)
        else:
            self._box = self._box.expand(l)
        self.vertices.append(l)

    def box(self):
        return self._box

    def itervertices(self):
        return iter(self.vertices)

    def iteredges(self):
        for i in xrange(1, len(self.vertices)):
            yield Edge(self.vertices[i-1], self.vertices[i])
        yield Edge(self.vertices[-1], self.vertices[0])

    def overlap(self, other):
        if isinstance(other, (Point, Location)):
            return self.__overlap_location(other)
        if isinstance(other, Box):
            return self.__overlap_box(other)
        if isinstance(other, Edge):
            return self.__overlap_edge(other)
        raise ValueError("Can't overlap a polygon with %s" % other)

    def __overlap_box(self, box):
        if not self.box().overlap(box):
            return False
        for edge in self.iteredges():
            if box.overlap(edge):
                return True
        return False

    def __overlap_location(self, loc):
        if not self.box().overlap(loc):
            return False
        inside = False
        ## http://www.ariel.com.au/a/python-point-int-poly.html
        for edge in self.iteredges():
            if loc.lat > min(edge.p.lat, edge.q.lat):
                if loc.lat <= max(edge.p.lat, edge.q.lat):
                    if loc.lon <= max(edge.p.lon, edge.q.lon):
                        if edge.p.lat != edge.q.lat:
                            xinters = (loc.lat - edge.p.lat) * (edge.q.lon - edge.p.lon) / (edge.q.lat - edge.p.lat) + edge.p.lon
                        if edge.p.lon == edge.q.lon or loc.lon <= xinters:
                            inside = not inside
        return inside

    def __overlap_edge(self, edge):
        if self.overlap(edge.p):
            return True
        if self.overlap(edge.q):
            return True
        for e in self.iteredges():
            if e.intersects(edge):
                return True
        return False

    def __overlap_polygon(self, poly):
        if not self.box().overlap(poly.box()):
            return False
        for p in self.itervertices():
            if poly.overlap(p):
                return True
        for p in poly.itervertices():
            if self.overlap(p):
                return True
        for e in self.iteredges():
            if poly.overlap(e):
                return True

class PolygonShape(Polygon):
    def __init__(self, shape, start=0, end=None):
        self.shape = shape
        self.start = start
        if end is None:
            self.end = len(shape.points)
        else:
            self.end = end

    def __len__(self):
        return self.end - self.start

    def kind(self):
        return Location

    def box(self):
        viter = self.itervertices()
        v = viter.next()
        box = Box(v, v)
        for v in viter:
            box = box.expand(v)
        return box

    def itervertices(self):
        for i in xrange(self.start, self.end):
            yield Location.from_shapefile(self.shape.points[i])

    def iteredges(self):
        for i in xrange(start, end-1):
            yield Edge.from_shapefile(self.shape.points[i],
                                      self.shape.points[i+1])
        yield Edge.from_shapefile(self.shape.points[end-1],
                   self.shape.points[start])

class MultiPolygon(object):
    def __init__(self, parts=None):
        self.parts = list()
        self._box = None
        if parts is not None:
            for p in parts:
                self.append(p)

    def __str__(self):
        return '%s(%s)' % (self.__class__.__name__, len(self))

    def __len__(self):
        return len(self.parts)

    def kind(self):
        return self.parts[0].kind()

    def area(self):
        a = 0
        for p in self.iterparts():
            a += p.area()

    def centroid(self):
        max_a = 0
        c = None
        for p in self.iterparts():
            a = p.area()
            if a > max_a:
                max_a = a
                c = p.centroid()
        return c

    def append(self, p):
        if not isinstance(p, Polygon):
            p = Polygon(p)
        if self._box is None:
            self._box = p.box()
        else:
            self._box = self._box.expand(p.box())
        self.parts.append(p)

    def box(self):
        return self._box

    def iterparts(self):
        return iter(self.parts)

    def overlap(self, other):
        box = self.box()
        if not box.overlap(other):
            return False
        for part in self.iterparts():
            if part.overlap(other):
                return True
        return False

class MultiPolygonShape(MultiPolygon):
    def __init__(self, shape):
        self.shape = shape

    def __len__(self):
        return len(self.shape.parts)

    def kind(self):
        return Location

    def box(self):
        return Box.from_shape(self.shape)

    def iterparts(self):
        x = self.shape.parts.tolist()
        x.append(len(self.shape.points))
        for start, end in izip(x, x[1:]):
            yield PolygonShape(self.shape, start, end)

