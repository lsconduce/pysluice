import os, yaml, math, gzip
from cStringIO import StringIO
import cairo
import traceback
from PIL import Image

try:
    from sluice import rsvg
    rsvg_import_error = None
except ImportError as e:
    rsvg = None
    rsvg_import_error = e

from sluice.geometry import Location, Point, Box, Edge, Polygon, MultiPolygon
from sluice.mercator import Mercator
from sluice import TEXTURE_RESPONSE_POOL, APP, PROTO
from sluice.api import Sluice

class Texture(Mercator):

    __slots__ = ('bl', 'tr', 'w', 'h', 'l0', 'y0', 'R', 'img', 'ctx')

    def __init__(self, *args, **kwargs):
        super(Texture, self).__init__(*args, **kwargs)
        self.img = None
        self.ctx = None

    def init_canvas(self):
        self.img = cairo.ImageSurface(cairo.FORMAT_ARGB32, int(self.w), int(self.h))
        self.ctx = cairo.Context(self.img)

    def draw_arrow(self, loc, angle, color, size=10):
        self.ctx.save()
        self.ctx.set_source_rgba(*color.with_alpha(1.0))
        self.ctx.set_line_width(1)
        x, y = self.project(loc)
        x1 = x - (size * math.sin(angle * math.pi / 180.0) / 2)
        x2 = x + (size * math.sin(angle * math.pi / 180.0) / 2)
        y1 = y + (size * math.cos(angle * math.pi / 180.0) / 2)
        y2 = y - (size * math.cos(angle * math.pi / 180.0) / 2)
        self.ctx.move_to(x1, y1)
        self.ctx.line_to(x2, y2)
        self.ctx.stroke()
        x3 = x2 - (size * math.sin((angle - 30) * math.pi / 180.0) / 3)
        x4 = x2 - (size * math.sin((angle + 30) * math.pi / 180.0) / 3)
        y3 = y2 + (size * math.cos((angle - 30) * math.pi / 180.0) / 3)
        y4 = y2 + (size * math.cos((angle + 30) * math.pi / 180.0) / 3)
        self.ctx.move_to(x2, y2)
        self.ctx.line_to(x3, y3)
        self.ctx.stroke()
        self.ctx.move_to(x2, y2)
        self.ctx.line_to(x4, y4)
        self.ctx.stroke()
        self.ctx.restore()

    def draw_spot(self, loc, color, size=None):
        if self.ctx is None:
            self.init_canvas()
        if size is None:
            size = self.w / 100.0
        radial = cairo.RadialGradient(size/2.0, size/2.0, 2, size/2.0, size/2.0, size)
        radial.add_color_stop_rgba(0, *color)
        radial.add_color_stop_rgba(1, color[0], color[1], color[2], 0)
        x, y = self.project(loc)
        x -= size/2.0
        y -= size/2.0
        self.ctx.save()
        self.ctx.translate(x, y)
        self.ctx.rectangle(0, 0, size, size)
        #self.ctx.rectangle(-size, -size, size, size)
        #self.ctx.clip()
        #self.ctx.set_source_rgb(1, 0, 0)
        self.ctx.set_source(radial)
        self.ctx.mask(radial)
        self.ctx.restore()

    def draw_line(self, edge, color, size=1.0):
        if not self.ctx:
            self.init_canvas()
        self.ctx.save()
        self.ctx.set_source_rgba(*color.with_alpha(1.0))
        self.ctx.set_line_width(size)
        x, y = self.project(edge.p)
        self.ctx.move_to(x, y)
        x, y = self.project(edge.q)
        self.ctx.line_to(x, y)
        self.ctx.stroke()
        self.ctx.restore()

    def draw_box(self, box, line_color=None, fill_color=None, size=1.0):
        if not self.ctx:
            self.init_canvas()
        self.ctx.save()
        if fill_color is not None:
            self.ctx.set_source_rgba(*fill_color.with_alpha(0.7))
            r, t = self.mercator.project(box.tr)
            l, b = self.mercator.project(box.bl)
            self.ctx.rectangle(l, t, r-l, b-t)
            self.ctx.fill()
        if line_color is not None:
            self.ctx.set_source_rgba(*line_color.with_alpha(1.0))
            self.ctx.set_line_width(size)
            r, t = self.mercator.project(box.tr)
            l, b = self.mercator.project(box.bl)
            self.ctx.rectangle(l, t, r-l, b-t)
            self.ctx.stroke()
        self.ctx.restore()

    def draw_polygon(self, poly, line_color=None, fill_color=None, size=1.0):
        if self.ctx is None:
            self.init_canvas()
        self.ctx.save()
        if self.fill_color is not None:
            self.ctx.new_path()
            self.ctx.set_source_rgba(*fill_color.with_alpha(0.7))
            verts = poly.itervertices()
            x, y = self.project(verts.next())
            self.ctx.move_to(x, y)
            for v in verts:
                x, y = self.project(v)
                self.ctx.line_to(x, y)
            self.ctx.close_path()
            self.ctx.fill()
        if self.line_color is not None:
            self.ctx.new_path()
            self.ctx.set_source_rgba(*line_color.with_alpha(1.0))
            self.ctx.set_line_width(size)
            verts = poly.itervertices()
            x, y = self.project(verts.next())
            self.ctx.move_to(x, y)
            for v in verts:
                x, y = self.project(v)
                self.ctx.line_to(x, y)
            self.ctx.close_path()
            self.ctx.stroke()
        self.ctx.restore()

    def draw(self, shape, line_color=None, fill_color=None, label=None):
        if not self.ctx:
            self.init_canvas()
        if isinstance(shape, Location):
            self.draw_spot(shape, fill_color)
        elif isinstance(shape, Edge):
            self.draw_line(shape, line_color)
        elif isinstance(shape, Box):
            self.draw_box(shape, line_color, fill_color)
        elif isinstance(shape, Polygon):
            self.draw_polygon(shape, line_color, fill_color)
        elif isinstance(shape, MultiPolygon):
            for p in shape.iterparts:
                self.draw_polygon(p, line_color, fill_color)
        if label is not None:
            self.draw_text(shape, label, line_color)

    def draw_text(self, shape, txt, color=None, font='DINOT'):
        if self.ctx is None:
            self.init_canvas()
        if color is None:
            color = BLACK
        self.ctx.save()
        try:
            self.ctx.set_source_rgba(*color.with_alpha(1.0))
            self.ctx.select_font_face(font)
            self.ctx.set_font_size(self.w / 80.0)
            (txb, tyb, tw, th, txa, tya) = self.ctx.text_extents(txt)
            cx, cy = self.project(shape.centroid())
            cx -= tw/2.0
            self.ctx.move_to(cx, cy)
            self.ctx.show_text(txt)
        except:
            pass
        self.ctx.restore()

    def png(self):
        data = StringIO()
        if isinstance(self.img, Image.Image):
            self.img.save(data, 'PNG')
        else:
            self.img.write_to_png(data)
        data.seek(0)
        return data

    def generate(self, p):
        edge = Edge(self.bl, self.tr)
        self.draw_text(edge, self.__class__.__name__)

    def empty(self):
        self.img = Image.new('RGBA', (int(self.w), int(self.h)))

class TextureFluoro(Sluice):
    name = 'Generic'
    texture_class = Texture

    edge_listen = False
    network_listen = False
    texture_debug = False

    def await(self, names=None):
        ## texture fluoros only need to await on sluice-to-fluoro
        ## and can probe on descrips
        hose = self.get_hose(TEXTURE_RESPONSE_POOL)
        return hose.await_probe_frwd([APP, PROTO, 'request', 'texture', self.name])

    def handle_texture_request(self, p):
        try:
            bl = Location(*p.ingests()['bl'])
            tr = Location(*p.ingests()['tr'])
            w, h = p.ingests()['px']
            if w <= 0 or h <= 0:
                # bad dimensions, ignore
                return False
            texture = self.texture_class(bl, tr, w)
            if 'disabled' in p.ingests()['style'].split(','):
                texture.empty()
            else:
                texture.generate(p)
            data = texture.png()
            if self.texture_debug:
                outfn = '/tmp/%s.png' % self.name
                print 'saving texture to %s' % outfn
                open(outfn, 'w').write(data.read())
            data.seek(0)
            self.send_texture(p, data, px=[int(texture.w), int(texture.h)])
            return True
        except KeyboardInterrupt:
            raise
        except Exception as e:
            traceback.print_exc()
            return False

