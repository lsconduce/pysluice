from cStringIO import StringIO
from copy import deepcopy
import math
from itertools import izip

import warnings
warnings.warn(DeprecationWarning("sluice.mapping module is deprecated.  Please see sluice.geometry, sluice.mercator and sluice.texture for the methods defined here"))

D2R = math.pi / 180
R = 6378.1 ## Earth radius, in kilometers


class Cartography(object):

    def __init__(self):
        self.min_lat =  -89.999
        self.max_lat =   89.999
        self.min_lng = -180.0
        self.max_lng =  180.0
        self.w = 1600
        self.img = None
        self.ctx = None
        self.setup_mercator()

    def set_box(self, bl, tr):
        self.min_lat = bl[0]
        self.min_lng = bl[1]
        self.max_lat = tr[0]
        self.max_lng = tr[1]
        self.setup_mercator()

    def set_width(self, w):
        self.w = w
        self.setup_mercator()

    def get_shapes(self, sfr, filter_method):
        for s, r in izip(sfr.iterShapes(), sfr.iterRecords()):
            if filter_method(self, s, r):
                yield (s, r)

    def shape_bbox_to_bltr(self, bbox):
        return ((bbox[1], bbox[0]), (bbox[3], bbox[2]))

    def box_overlap(self, bl, tr):
        if (bl[0] >= self.min_lat and bl[0] <= self.max_lat) or \
                (tr[0] >= self.min_lat and tr[0] <= self.max_lat):
            if (bl[1] >= self.min_lng and bl[1] <= self.max_lng) or \
                    (tr[1] >= self.min_lng and tr[1] <= self.max_lng):
                return True
            if bl[1] < self.min_lng and tr[1] > self.max_lng:
                return True
        if (bl[1] >= self.min_lng and bl[1] <= self.max_lng) or \
                (tr[1] >= self.min_lng and tr[1] <= self.max_lng):
            if (bl[0] >= self.min_lat and bl[0] <= self.max_lat) or \
                    (tr[0] >= self.min_lat and tr[0] <= self.max_lat):
                return True
            if bl[0] < self.min_lat and tr[0] > self.max_lat:
                return True
        if bl[0] <= self.min_lat and tr[0] >= self.max_lat:
            if bl[1] <= self.max_lng or tr[1] >= self.min_lng:
                return True
        return False

    def point_overlap(self, p):
        if p[0] >= self.min_lat and p[0] <= self.max_lat:
            if p[1] >= self.min_lng and p[1] <= self.max_lng:
                return True
        return False

    def shape_part_ranges(self, shape):
        parts = deepcopy(shape.parts.tolist())
        parts.append(len(shape.points))
        return izip(parts, parts[1:])

    def inside(self, p, shape):
        ## http://www.ariel.com.au/a/python-point-int-poly.html
        bl, tr = self.shape_bbox_to_bltr(shape.bbox)
        if p[0] < bl[0] or p[0] > tr[0] or p[1] < bl[1] or p[1] > tr[1]:
            return False
        for start, end in self.shape_part_ranges(shape):
            n = end - start
            inside = False
            p1x, p1y = shape.points[start]
            for i in xrange(1, n+1):
                p2x, p2y = shape.points[start + (i % n)]
                if p[0] > min(p1y, p2y):
                    if p[0] <= max(p1y, p2y):
                        if p[1] <= max(p1x, p2x):
                            if p1y != p2y:
                                xinters = (p[0]-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                            if p1x == p2x or p[1] <= xinters:
                                inside = not inside
                p1x, p1y = p2x, p2y
            if inside:
                return True
        return False

    def get_shape_for_point(self, p, sf):
        i = -1
        for s in sf.iterShapes():
            i += 1
            if self.inside(p, s):
                return (i, s, sf.record(i))
        return (None, None, None)

    def poly_overlap(self, shape):
        bl, tr = self.shape_bbox_to_bltr(shape.bbox)
        if not self.box_overlap(bl, tr):
            return False
        for start, end in self.shape_part_ranges(shape):
            n = end - start
            for i in xrange(n+1):
                x1, y1 = shape.points[start + (i % n)]
                x2, y2 = shape.points[start + ((i+1) % n)]
                if self.line_overlap((y1, x1), (y2, x2)):
                    return True
        return False

    def line_overlap(self, p1, p2):
        if self.point_overlap(p1) or self.point_overlap(p2):
            ## one of the endpoints is inside the box
            return True
        if p1[0] < self.min_lat and p2[0] < self.min_lat:
            ## both endpoints below the box
            return False
        if p1[0] > self.max_lat and p2[0] > self.max_lat:
            ## both endpoints above the box
            return False
        if p1[1] < self.min_lng and p2[1] < self.min_lng:
            ## both endpoints left of box
            return False
        if p1[1] > self.max_lng and p2[1] > self.max_lng:
            ## both endpoints right of box
            return False
        if p1[1] == p2[1]:
            ## vertical line
            ## if we've gotten this far, line must cross box
            return True
        if p1[0] == p2[0]:
            ## horizontal line
            ## if we've gotten this far, line must cross box
            return True
        m = (p1[0] - p2[0]) / float(p1[1] - p2[1])
        c = p1[0] - (m * p1[1])
        x1 = (self.min_lat - c) / m
        x2 = (self.max_lat - c) / m
        if x1 >= self.min_lng and x1 <= self.max_lng:
            return True
        if x2 >= self.min_lng and x2 <= self.max_lng:
            return True
        return False

    def setup_mercator(self):
        l0 = D2R * (self.min_lng + self.max_lng) / 2.0
        R = self.w / (D2R * (self.max_lng - self.min_lng))
        y0 = R * math.log(math.tan((math.pi/4) + (D2R*self.max_lat/2)))
        self.l0 = l0
        self.R = R
        self.y0 = y0
        self.h = int(self.mercator((self.min_lat, self.min_lng))[1])
        #print "min_lat = %s; min_lng = %s; max_lat = %s; max_lng = %s" % (self.min_lat, self.min_lng, self.max_lat, self.max_lng)
        #print "w = %s; h = %s; l0 = %s; R = %s; y0 = %s" % (self.w, self.h, self.l0, self.R, self.y0)
        #print 'h = %s' % self.h

    def mercator(self, p):
        x = (self.w/2.0) - (self.R * (D2R*p[1] - self.l0))
        
        th = (math.pi/4) + (D2R*p[0]/2)
        t = math.tan(th)
        l = math.log(t)
        y = self.R * math.log(math.tan((math.pi/4) + (D2R*p[0]/2)))
        return self.w - x, self.y0 - y

    def init_canvas(self):
        self.img = cairo.ImageSurface(cairo.FORMAT_ARGB32, self.w, self.h)
        self.ctx = cairo.Context(self.img)

    def draw_spot(self, lat, lng, color, size):
        radial = cairo.RadialGradient(size/2, size/2, 2, size/2, size/2, size)
        radial.add_color_stop_rgba(0, *color)
        radial.add_color_stop_rgba(1, color[0], color[1], color[2], 0)
        x, y = self.mercator((lat, lng))
        x -= size/2
        y -= size/2
        print "%s spot at (%s, %s)" % (size, x, y)
        self.ctx.save()
        self.ctx.translate(x, y)
        self.ctx.rectangle(0, 0, size, size)
        #self.ctx.rectangle(-size, -size, size, size)
        #self.ctx.clip()
        #self.ctx.set_source_rgb(1, 0, 0)
        self.ctx.set_source(radial)
        self.ctx.mask(radial)
        self.ctx.restore()

    def draw(self, color, shape, label=None):
        if not self.ctx:
            self.init_canvas()
        for start, end in self.shape_part_ranges(shape):
            self.ctx.set_source_rgba(color[0]/255.0, color[1]/255.0, color[2]/255.0, 0.7)
            self.ctx.new_path()
            lng, lat = shape.points[start]
            x, y = self.mercator((lat, lng))
            self.ctx.move_to(x, y)
            for lng, lat in shape.points[start+1:end]:
                x, y = self.mercator((lat, lng))
                self.ctx.line_to(x, y)
            self.ctx.close_path()
            self.ctx.fill()
            self.ctx.set_source_rgba(color[0]/255.0, color[1]/255.0, color[2]/255.0, 1.0)
            self.ctx.new_path()
            lng, lat = shape.points[start]
            x, y = self.mercator((lat, lng))
            self.ctx.move_to(x, y)
            for lng, lat in shape.points[start+1:end]:
                x, y = self.mercator((lat, lng))
                self.ctx.line_to(x, y)
            self.ctx.close_path()
            self.ctx.stroke()
        if label:
            try:
                c = self.centroid(shape)
                self.ctx.set_source_rgba(0, 0, 0, 1.0)
                self.ctx.select_font_face('DINOT')
                self.ctx.set_font_size(self.w/80)
                (txb, tyb, tw, th, txa, tya) = self.ctx.text_extents(label)
                #c = ((shape.bbox[1]+shape.bbox[3])/2.0, (shape.bbox[0]+shape.bbox[2])/2.0)
                cx, cy = self.mercator(c)
                cx -= tw/2.0
                self.ctx.move_to(cx, cy)
                self.ctx.show_text(label)
            except:
                pass

    def png(self):
        data = StringIO()
        self.img.write_to_png(data)
        data.seek(0)
        return data

    def centroid(self, shape):
        max_a = 0
        c = None
        for start, end in self.shape_part_ranges(shape):
            n = end - start
            a = 0
            cx = 0
            cy = 0
            for i in xrange(n-1):
                xy1 = shape.points[start+i][0] * shape.points[start+i+1][1]
                xy2 = shape.points[start+i+1][0] * shape.points[start+i][1]
                xy = xy1 - xy2
                x = shape.points[start+i][0] + shape.points[start+i+1][0]
                y = shape.points[start+i][1] + shape.points[start+i+1][1]
                a += xy
                cx += (x * xy)
                cy += (y * xy)
            if a == 0:
                continue
            a /= 2.0
            cx /= (6 * a)
            cy /= (6 * a)
            if abs(a) > max_a:
                max_a = abs(a)
                c = (cy, cx)
        return c

    def bearing(self, p1, p2):
        lat1 = D2R * p1[0]
        lat2 = D2R * p2[0]
        lng1 = D2R * p1[1]
        lng2 = D2R * p2[1]
        dlat = lat2 - lat1
        dlng = lng2 - lng1
        y = math.sin(dlng) * math.cos(lat2)
        x = math.cos(lat1)*math.sin(lat2) - math.sin(lat1)*math.cos(lat2)*math.cos(dlng)
        return math.atan2(y, x) / D2R

    def distance(self, p1, p2):
        lat1 = D2R * p1[0]
        lat2 = D2R * p2[0]
        lng1 = D2R * p1[1]
        lng2 = D2R * p2[1]
        dlat = lat2 - lat1
        dlng = lng2 - lng1
        a = math.sin(dlat/2) * math.sin(dlat/2) + math.sin(dlng/2) * math.sin(dlng/2) * math.cos(lat1) * math.cos(lat2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        return R * c ## kilometers

    def destination(self, p1, bearing, distance):
        lat1 = p1[0] * math.pi / 180.0
        lng1 = p1[1] * math.pi / 180.0
        b = D2R * bearing
        lat2 = math.asin(math.sin(lat1)*math.cos(distance/R) + math.cos(lat1)*math.sin(distance/R)*math.cos(b))
        lng2 = lng1 + math.atan2(math.sin(b)*math.sin(distance/R)*math.cos(lat1), math.cos(distance/R)-math.sin(lat1)*math.sin(lat2))
        return (lat2 / D2R, lng2 / D2R)

    def get_bounding_rectangle(self, p, distance):
        p1 = self.destination(p, 0, distance)
        p2 = self.destination(p, 90, distance)
        dlat = p1[0] - p[0]
        dlng = p2[1] - p[1]
        if dlng < 0:
            dlng += 360.0
        elif dlng >= 360:
            dlng -= 360
        bl = (p[0] - dlat, p[1] - dlng)
        tr = (p[0] + dlat, p[1] + dlng)
        return (bl, tr)

