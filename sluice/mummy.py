import pprint, json, math, os
from sluice.types import *
from plasma.yamlio import parse_yaml_protein
import numpy
from numpy import array
from numpy.matrixlib import matrix
from sluice.api import Sluice
from sluice.screens import Screens

s = None
screens = None

def init_screens():
    global s
    global screens
    if s is None:
        s = Sluice()
    if screens is None:
        screens = Screens.load()

class FluoroConfig(object):
    def __init__(self, name):
        self.name = name
        self.__config = obmap({})
        self.__bl = numeric_array([-85.0, -180.0], float64)
        self.__tr = numeric_array([ 85.0,  180.0], float64)

    def to_json(self, degrade):
        return {
            'name': self.name,
            'config': self.__config.to_json(degrade),
            'bl': self.__bl.to_json(degrade),
            'tr': self.__tr.to_json(degrade)
        }

    def configure(self, **kwargs):
        self.__config.update(kwargs)

    def set_bounds(self, bl, tr):
        self.__bl = numeric_array(bl, float64)
        self.__tr = numeric_array(tr, float64)

class GeoQuad(object):
    def __init__(self, gq, atlas=None):
        init_screens()
        self.__cx = gq['loc']['cur-val'][0]
        self.__cy = gq['loc']['cur-val'][1]
        self.__h = gq['height']['cur-val']
        self.__w = gq['width']['cur-val']
        self.__tm = matrix(gq['uv-xform']['mat'].to_json(True))
        if atlas:
            ## WHY MUST THIS BE SO DIFFICULT?
            #self.__align(atlas)
            self.__tm = matrix(atlas['uv-xform']['mat'].to_json(True))
        #self.__tm = matrix([
        #    [0.010904347826086938, 0.0, 0.0, 0.0],
        #    [0.0, 0.0019999999999999996, 0.0, 0.0],
        #    [0.0, 0.0, 1.0, 0.0],
        #    [-0.32794092197199465, 0.10053562574927719, 0.0, 1.0]
        #])
        self.__gq = { 'loc': gq['loc']['cur-val'].to_json(True), 'width': gq['width']['cur-val'].to_json(True), 'height': gq['height']['cur-val'].to_json(True), 'mat': gq['uv-xform']['mat'].to_json(True), 'imat': gq['uv-xform']['imat'].to_json(True) }

    def loc(self, v=None):
        if v is None:
            v = array([self.__cx, self.__cy, 0.0])
        return self.__vect_to_latlon(v)

    def bl(self):
        blmm = array([self.__cx - self.__w/2.0, self.__cy - self.__h/2.0, 0.0])
        blll = self.__vect_to_latlon(blmm)
        return blll

    def tr(self):
        trmm = array([self.__cx + self.__w/2.0, self.__cy + self.__h/2.0, 0.0])
        trll = self.__vect_to_latlon(trmm)
        return trll

    def bounds(self):
        return {
            'bl': self.bl(),
            'tr': self.tr()
        }

    def __vect_to_latlon(self, v):
        uv = self.__v3_to_uv(v)
        cc = self.__uv_to_carto(uv)
        ll = self.__carto_to_latlon(cc)
        return (float64(ll[0]), float64(ll[1]))

    def __v3_to_uv(self, v):
        #cntr = array(screens.center()+(0.0,))
        # apparently this has nothing to do with screen setup
        cntr = array([0.0, 0.0, 0.0])
        nrm = array(screens.norm())
        #vv = line_plane_intersection(v, nrm, cntr, nrm)
        # let's assume that this is unnecessary
        vv = v
        w = screens.width()
        h = screens.height()
        w = 3135.0
        h = 575.0
        ovr_hat = array(screens.over())
        uhp_hat = array(screens.up())
        dee = vv - cntr
        u = numpy.dot(dee, ovr_hat) / w
        v = numpy.dot(dee, uhp_hat) / h
        return array([u, v])

    def __uv_to_carto(self, uv):
        vect = array(uv.tolist()+[0.0])
        t = self.__transform_v3(vect)
        return array(t.tolist()[:2])

    def __align(self, gq):
        w = screens.width()
        h = screens.height()
        w = 3135.0
        h = 575.0
        left = -0.5 * w
        right = 0.5 * w
        top = -0.5 * h
        bot = 0.5 * h
        uv_bl = self.__v3_to_uv(array([bot, left, 0.0]))
        uv_tr = self.__v3_to_uv(array([top, right, 0.0]))
        cxcy_bl = self.__uv_to_carto(uv_bl)
        cxcy_tr = self.__uv_to_carto(uv_tr)
        self.__set_uv_xform_by_carto(cxcy_bl, cxcy_tr)

    def __set_uv_xform_by_carto(self, bl, tr):
        dee_xy = (tr[0] - bl[0], tr[1] - bl[1])
        g_cntr_cxcy = (bl[0] + dee_xy[0] / 2.0,
                       bl[1] + dee_xy[1] / 2.0)
        self.__load_from_corners( (-0.5 - g_cntr_cxcy[0]) / dee_xy[0],
                             (-0.5 - g_cntr_cxcy[1]) / dee_xy[1],
                             ( 0.5 - g_cntr_cxcy[0]) / dee_xy[0],
                             ( 0.5 - g_cntr_cxcy[1]) / dee_xy[1]
        )

    def __load_from_corners(self, x_bl, y_bl, x_tr, y_tr):
        ident = numpy.identity(4)
        sc = ident * numpy.array([
            1.0 / (x_tr - x_bl),
            1.0 / (y_tr - y_bl),
            1.0, 1.0])
        tr = numpy.identity(4)
        tr[0][3] = -1.0 * (x_tr - 0.5 * (x_tr - x_bl))
        tr[1][3] = -1.0 * (y_tr - 0.5 * (y_tr - y_bl))
        tr[3][3] = 1.0
        self.__tm = matrix(sc) * matrix(tr)

    def __transform_v3(self, vect):
        v = array(list(vect)+[1.0])
        t = (v * self.__tm).tolist()[0]
        return array([t[0], t[1], t[2]])

    def __latlon_to_carto(self, lat, lon):
        rad_lat = lat * math.pi / 180.0
        y = math.log((math.sin(rad_lat) + 1.0) / math.cos(rad_lat))
        y = (1.0 + y / math.pi) / 2.0
        x = (lon / 180.0) / 2.0
        return (x, y)

    def zoom(self):
        carto_min = self.__latlon_to_carto(*self.bl())
        carto_max = self.__latlon_to_carto(*self.tr())
        return 1.0 / (carto_max[1] - carto_min[1])

    def __carto_to_latlon(self, cc):
        cx = cc[0]
        cy = cc[1]
        yy = cy + 0.5
        yy = min(1.0, max(0.0, yy))
        rad = math.atan(math.sinh( ((2.0 * yy) - 1.0) * math.pi ))
        lat = rad * 180.0 / math.pi
        xx = cx + 0.5
        lon = (2.0 * xx - 1.0) * 180.0
        return (lat, lon)

class Mummy(object):

    def __init__(self, name=None):
        self.name = name
        self.__map = obmap({
            'name': 'Map',
            'config': { },
            'lat': 0.0,
            'lon': 0.0,
            'zoom': 1.0,
        })
        self.__scopes = oblist([])

    def to_json(self, degrade=False):
        return {
            'name': self.name,
            'map': self.__map.to_json(degrade),
            'scopes': self.__scopes.to_json(degrade)
        }

    def configure_map(self, **kwargs):
        self.__map['config'].update(kwargs)

    def zoom_to(self, lat=None, lon=None, zoom=None):
        if lat is not None and lon is not None:
            self.__map['lat'] = lat
            self.__map['lon'] = lon
        if zoom is not None:
            self.__map['zoom'] = zoom

    def add_fluoro(self, name, config=None, bl=None, tr=None):
        if isinstance(name, basestring):
            f = FluoroConfig(name)
        elif isinstance(name, FluoroConfig):
            f = name
        else:
            raise Exception("can't add %s(%s) as a fluoroscope" % (type(name), name))
        if config is not None:
            f.configure(**config)
        if bl is not None and tr is not None:
            f.set_bounds(bl, tr)
        self.__scopes.append(f)
        return f

    @classmethod
    def load_from_protein_file(cls, fn):
        init_screens()
        name = os.path.splitext(os.path.split(fn)[1])[0].replace('_', ' ')
        self = cls(name)
        p = parse_yaml_protein(file(fn))
        scopes = list(x.right for x in p.ingests()['wide-screen']['fluoroscopes'])
        for scope in scopes:
            #if scope['class-name'] == 'StaticImageFluoroscope' and scope['permanent-and-static']:
            #    static layers
            #if scope['class-name'] == 'AtlasQuad':
            #    ## map
            #elif scope['class-name'] == 'StaticImageFluoroscope':
            #    ## non-decorated texture
            #elif scope['class-name'] == 'DecoratedStaticImageFluoroscope':
            #    ## decorated texture
            #elif scope['class-name'] == 'NodeyScope':
            #    ## network
            config = scope.get('configuration')
            if config is None:
                ## static layer; this is managed by the map fluoro,
                ## so we can skip it
                ## TODO: might it be a web fluoro?
                continue
            attrs = s.parse_fluoroscope_config(config)
            if scope['class-name'] == 'AtlasQuad':
                ## this is the map
                self.configure_map(**attrs)
                gq = GeoQuad(scope['geo-quad'])
                lat, lon = gq.loc()
                self.zoom_to(lat, lon, gq.zoom())
                ## TODO: zoom from geoquad?
            else:
                f = self.add_fluoro(config.get('name'))
                f.configure(**attrs)
                gq = GeoQuad(scope['geo-quad'])
                f.set_bounds(gq.bl(), gq.tr())
        return self

    @classmethod
    def load_from_config(cls, config):
        self = cls(config['name'])
        self.configure_map(**config['map']['config'])
        self.zoom_to(config['map']['lat'], config['map']['lon'], config['map']['zoom'])
        for scope in config['scopes']:
            f = self.add_fluoro(scope['name'])
            f.configure(**scope['config'])
            f.set_bounds(scope['bl'], scope['tr'])
        return self

    @classmethod
    def mummify(cls, name):
        ## TODO
        self = cls(name)
        ## get current list of fluoroscopes
        ## and build up a new mummy

    def vivify(self):
        ## TODO
        ## doesn't clear anything, just instantiates fluoroscopes
        ## list fluoro templates to match name -> config
        ## configure map
        ## pan/zoom
        for scope in self.__scopes:
            ## copy config from paramus
            ## set attributes according to our mummy
            ## instantiate at bl, tr
            pass

if '__main__' == __name__:
    import sys
    m = Mummy.load_from_protein_file(sys.argv[1])
    print json.dumps(m.to_json(True), indent=2, sort_keys=True)
    #extract_fluoroscopes(sys.argv[1])

