import os, subprocess, re
from copy import deepcopy
from sluice.types import *
from plasma import Protein
from plasma.yamlio import parse_yaml_protein, dump_yaml_protein
import numpy
from numpy import array
from sluice.api import Sluice

try:
    from AppKit import NSScreen
except ImportError:
    NSScreen = None

try:
    from Quartz import CGDisplayScreenSize
except ImportError:
    CGDisplayScreenSize = None

NAMES = [
    None,
    ['main'],
    ['main', 'right'],
    ['left', 'main', 'right'],
]

def xrandr():
    try:
        out = subprocess.Popen(['xrandr'], stdout=subprocess.PIPE).communicate()
    except OSError as e:
        return None
    screens = []
    for line in out[0].split('\n'):
        m = re.search(r'^(.*?)\s+connected\s+(\d+)x(\d+)\+(\d+)\+(\d+)\s+.*?(\d+)mm x (\d+)mm', line)
        if m:
            screens.append({
                'name': m.group(1),
                'width_px':  int(m.group(2)),
                'height_px': int(m.group(3)),
                'left_px':   int(m.group(4)),
                'top_px':    int(m.group(5)),
                'width_mm':  float(m.group(6)),
                'height_mm': float(m.group(7)),
            })
    screens = sorted(screens, key=lambda x: x['left_px'])
    return screens

class Screens(object):
    def __init__(self):
        self.__s = list()
        self.__f = list()

    @staticmethod
    def count_displays():
        if NSScreen is not None:
            return len(NSScreen.screens())
        ## TODO: autodetect screen config on linux
        screens = xrandr()
        if screens:
            return len(screens)
        return 3

    def save(self, dn=None):
        if dn is None:
            dn = os.getenv('OB_ETC_PATH').split(os.path.pathsep)[0]
        self.__save_screens(dn)
        self.__save_felds(dn)

    def arrange_screens(self):
        w_mm = 0
        for s in self.__s:
            w_mm += s['phys-size'][0] + (2 * s['bezel'])
        i = NAMES[len(self.__s)].index('main')
        left_mm = -1 * w_mm / 2.0
        left_px = 0
        h_mm = self.__s[i]['phys-size'][1]
        #top_mm = -1 * (h_mm / 2.0)
        self.__f = list(None for i in xrange(len(self.__s)))
        for i, s in enumerate(self.__s):
            ratio = s['phys-size'][0] / s['phys-size'][1]
            s['phys-size'] = [h_mm * ratio, h_mm]
            left_mm += s['bezel']
            dpmm = s['px-size'][1] / float(s['phys-size'][1])
            #top_px = int(dpmm * (h_mm - s['phys-size'][1]) / 2.0)
            top_px = 0
            s['type'] = 'basic'
            s['cent'] = [left_mm + s['phys-size'][0]/2.0, 0.0, -1518.0]
            s['norm'] = [0.0, 0.0, 1.0]
            s['over'] = [1.0, 0.0, 0.0]
            s['px-origin'] = [left_px, top_px]
            s['eye-dist'] = 500.0
            self.__f[i] = {
                'window': [left_px, top_px, s['px-size'][0], s['px-size'][1]],
                'screen': s['name']
            }
            left_px += s['px-size'][0] + 1
            left_mm += s['phys-size'][0] + s['bezel']

    def get_screens(self):
        return self.__s

    def get_felds(self):
        return self.__f

    def export_screens(self):
        screens = obmap()
        ## because for some reason, this matters
        screens['main'] = None
        for xs in self.__s:
            s = obmap()
            s['type'] = xs['type']
            for k in ('cent', 'phys-size', 'norm', 'over'):
                s[k] = oblist(float64(x) for x in xs[k])
            for k in ('px-size', 'px-origin'):
                s[k] = oblist(int64(x) for x in xs[k])
            s['eye-dist'] = float64(xs['eye-dist'])
            #s = deepcopy(xs)
            #name = s.pop('name')
            #for k in ('bezel', 'diagonal', 'aspect_ratio'):
            #    s.pop(k, None)
            screens[xs['name']] = s
        return screens

    def export_felds(self):
        felds = obmap()
        felds['main'] = None
        for xf in self.__f:
            f = obmap()
            f['window'] = oblist(int64(x) for x in xf['window'])
            f['screen'] = xf['screen']
            felds[f['screen']] = f
        return felds

    def __save_screens(self, dn):
        screens = self.export_screens()
        p = Protein(['visidrome', 'screen-info'], { 'screens': screens })
        txt = dump_yaml_protein(p)
        fn = os.path.join(dn, 'screen.protein')
        file(fn, 'w').write(txt)
        return fn

    def __save_felds(self, dn):
        felds = self.export_felds()
        p = Protein(['visidrome', 'feld-info'], { 'felds': felds })
        txt = dump_yaml_protein(p)
        fn = os.path.join(dn, 'feld.protein')
        file(fn, 'w').write(txt)
        return fn

    def make_fullscreen(self):
        self.__s = list()
        self.__f = list()
        screens = []
        felds = []
        if NSScreen is not None:
            ## mac way
            screens = sorted(NSScreen.screens(), key=lambda s: s.frame().origin.x)[:3]
            n = len(screens)
            for name, s in zip(NAMES[n], screens):
                px = s.frame().size
                orig = s.frame().origin
                felds.append({
                    'window': [int(orig.x), 0,
                               int(px.width), int(px.height)],
                    'screen': name
                })
                width_px = int(px.width)
                height_px = int(px.height)
                if CGDisplayScreenSize is not None:
                    mm = CGDisplayScreenSize(s.deviceDescription()['NSScreenNumber'])
                    width_mm = mm.width
                    height_mm = mm.height
                    if width_mm == 0 or height_mm == 0:
                        res = s.deviceDescription()['NSDeviceResolution'].sizeValue()
                        width_mm = 25.4 * width_px / float(res.width)
                        height_mm = 25.4 * height_px / float(res.height)
                else:
                    res = s.deviceDescription()['NSDeviceResolution'].sizeValue()
                    width_mm = 25.4 * width_px / float(res.width)
                    height_mm = 25.4 * height_px / float(res.height)
                ## average half inch bezel
                bezel = 25.4 * 0.5
                self.add_screen(name, width_mm=width_mm,
                                      height_mm=height_mm,
                                      width_px=width_px,
                                      height_px=height_px,
                                      bezel=bezel)
        else:
            ## linux way? xrandr seems to be the popular answer
            ## but it's not installed on any of our linux boxen
            ## at the moment
            screens = xrandr()
            if not screens:
                screens = [
                    { 'left_px': 0, 'top_px': 0, 'width_px': 1920, 'height_px': 1080, 'width_mm': 640.0, 'height_mm': 360.0 },
                    { 'left_px': 1920, 'top_px': 0, 'width_px': 1920, 'height_px': 1080, 'width_mm': 640.0, 'height_mm': 360.0 },
                    { 'left_px': 3840, 'top_px': 0, 'width_px': 1920, 'height_px': 1080, 'width_mm': 640.0, 'height_mm': 360.0 }
                ]
            n = len(screens)
            for name, s in zip(NAMES[n], screens):
                felds.append({
                    'window': [s['left_px'], s['top_px'],
                               s['width_px'], s['height_px']],
                    'screen': name
                })
                ## average half inch bezel
                bezel = 25.4 * 0.5
                self.add_screen(name, width_mm=s['width_mm'],
                                      height_mm=s['height_mm'],
                                      width_px=s['width_px'],
                                      height_px=s['height_px'],
                                      bezel=bezel)
        self.arrange_screens()
        self.__f = felds

    def make_windowed(self, n=3, w=None, h=None, aspect_ratio=None):
        if n > 3:
            n = 3
        if aspect_ratio is None:
            aspect_ratio = 1.6 ## 1440/900
            #aspect_ratio = 16.0 / 9.0
        if w is None:
            if h is not None:
                w = int(h * aspect_ratio)
            elif NSScreen is None:
                w = int(1920 / n)
            else:
                w = int(NSScreen.screens()[0].frame().size.width / n)
        if NSScreen is not None and CGDisplayScreenSize is not None:
            s = NSScreen.screens()[0]
            w_px = s.frame().size.width
            w_mm = CGDisplayScreenSize(s.deviceDescription()['NSScreenNumber']).width
            dpmm = float(w_px) / float(w_mm)
        elif NSScreen is not None:
            dpmm = NSScreen.screens()[0].deviceDescription()['NSDeviceResolution'].sizeValue().width / 25.4
        else:
            dpmm = 72.0 / 25.4
        w_mm = w / dpmm
        if h is None:
            h = int(w / aspect_ratio)
        h_mm = h / dpmm
        aspect_ratio = float(w) / float(h)
        self.__s = []
        felds = []
        #print 'width = %s/%s, height = %s/%s' % (w, w_mm, h, h_mm)
        for i, name in enumerate(NAMES[n]):
            self.add_screen(name, width_mm=1400.0, height_mm=900.0, width_px=w, height_px=h)
            felds.append({
                'window': [i * w, 20, w, h],
                'screen': name
            })
        self.arrange_screens()
        self.__f = felds

    def add_screen(self, name, *args, **kwargs):
        screen = self.make_screen(*args, **kwargs)
        screen['name'] = name
        self.__s.append(screen)

    def make_screen(self, aspect_ratio=None, diagonal_mm=None, width_mm=None, height_mm=None, width_px=None, height_px=None, dpi=None, bezel=0.0):
        if dpi is not None:
            dpmm = dpi / 25.4
        else:
            dpmm = None
        if width_mm is None or height_mm is None or \
           width_px is None or height_px is None:
            if width_mm is not None and height_mm is not None:
                aspect_ratio = float(width_mm) / float(height_mm)
            elif width_px is not None and height_px is not None:
                aspect_ratio = float(width_px) / float(height_px)
            if width_mm is None:
                if diagonal_mm is not None and aspect_ratio is not None:
                    width_mm = diagonal_mm / math.sqrt(1 + 1.0 / (aspect_ratio**2))
                elif height_mm is not None and aspect_ratio is not None:
                    width_mm = aspect_ratio * height_mm
                elif height_mm is not None and diagonal_mm is not None:
                    width_mm = math.sqrt(diagonal_mm**2 - height_mm**2)
                elif width_px is not None and dpmm is not None:
                    width_mm = width_px / dpmm
            if height_mm is None:
                if diagonal_mm is not None and aspect_ratio is not None:
                    height_mm = diagonal_mm / math.sqrt(1 + (aspect_ratio**2))
                elif width_mm is not None and aspect_ratio is not None:
                    height_mm = width_mm / aspect_ratio
                elif width_mm is not None and diagonal is not None:
                    height_mm = math.sqrlt(diagonal_mm**2 - width_mm**2)
                elif height_px is not None and dpmm is not None:
                    height_mm = height_px / dpmm
            if width_mm is not None and height_mm is not None:
                diagonal = math.sqrt(width_mm**2 + height_mm**2)
                aspect_ratio = width_mm / height_mm
                if dpmm is None:
                    if width_px is not None:
                        dpmm = width_px / width_mm
                    elif height_px is not None:
                        dpmm = height_px / height_mm
                    else:
                        dpmm = 72.0 / 25.4
            if width_px is None:
                if width_mm is not None and dpmm is not None:
                    width_px = int(dpmm * width_mm)
                elif height_px is not None and aspect_ratio is not None:
                    width = int(height_px * aspect_ratio)
            if height_px is None:
                if height_mm is not None and dpmm is not None:
                    height_px = int(dpmm * height_mm)
                elif width_px is not None and aspect_ratio is not None:
                    height_px = int(width_px / aspect_ratio)
        if width_mm is None or height_mm is None or \
           width_px is None or height_px is None:
            raise Exception("screen dimensions not adequately specified")
        return {
            'type': 'basic',
            'phys-size': [width_mm, height_mm],
            'px-size': [width_px, height_px],
            'eye-dist': 500.0,
            'norm': [0.0, 0.0, 1.0],
            'over': [1.0, 0.0, 0.0],
            'bezel': bezel
        }

    @classmethod
    def load(cls):
        self = cls()
        fn = Sluice.get_etc_file('screen.protein')
        xscreens = parse_yaml_protein(file(fn)).ingests()['screens']
        screens = list()
        for k in sorted(xscreens.keys(), key=lambda k: xscreens[k]['cent'][0]):
            v = xscreens[k]
            v['name'] = k
            screens.append(v)
        if len(screens) == 1:
            screens[0]['bezel'] = 0.0
        elif len(screens) == 2:
            l = screens[0]
            r = screens[1]
            gap = (r['cent'][0] - r['phys-size'][0] / 2.0) - \
                  (l['cent'][0] + l['phys-size'][0] / 2.0)
            screens[0]['bezel'] = gap / 2.0
            screens[1]['bezel'] = gap / 2.0
        elif len(screens) > 2:
            for i in xrange(1, len(screens)):
                me = screens[i]
                l = screens[i-1]
                left_gap = (me['cent'][0] - me['phys-size'][0] / 2.0) - \
                           (l['cent'][0] + l['phys-size'][0] / 2.0)
                if 'bezel' in l:
                    me['bezel'] = left_gap - l['bezel']
                else:
                    r = screens[i+1]
                    right_gap = (r['cent'][0] - r['phys-size'][0] / 2.0) - \
                                (me['cent'][0] + me['phys-size'][0] / 2.0)
                    me['bezel'] = (left_gap + right_gap) / 4.0
            me = screens[0]
            r = screens[1]
            right_gap = (r['cent'][0] - r['phys-size'][0] / 2.0) - \
                        (me['cent'][0] + me['phys-size'][0] / 2.0)
            me['bezel'] = right_gap - r['bezel']
        self.__s = screens
        fn = Sluice.get_etc_file('feld.protein')
        felds = parse_yaml_protein(file(fn)).ingests()['felds']
        self.__f = sorted(felds.values(), key=lambda f: f['window'][0])
        return self

    def bottom(self):
        return min((x['cent'][1] - x['phys-size'][1] / 2.0) for x in self.__s)

    def top(self):
        return max((x['cent'][1] + x['phys-size'][1] / 2.0) for x in self.__s)

    def left(self):
        return min((x['cent'][0] - x['phys-size'][0] / 2.0) for x in self.__s)

    def right(self):
        return max((x['cent'][0] + x['phys-size'][0] / 2.0) for x in self.__s)

    def width(self):
        return self.right() - self.left()

    def height(self):
        return self.top() - self.bottom()

    def center(self):
        cx = self.left() + self.width() / 2.0
        cy = self.bottom() + self.height() / 2.0
        return (cx, cy)

    def screen_at(self, x, y):
        for s in self.__s:
            if s['cent'][0] + s['phys-size'][0] / 2.0 >= x and \
               s['cent'][0] - s['phys-size'][0] / 2.0 <= x and \
               s['cent'][1] + s['phys-size'][1] / 2.0 >= y and \
               s['cent'][1] - s['phys-size'][1] / 2.0 <= y:
                return s
        return None

    def norm(self):
        c = self.center()
        s = self.screen_at(*c)
        return s['norm']

    def over(self):
        c = self.center()
        s = self.screen_at(*c)
        return s['over']

    def up(self):
        n = array(self.norm())
        o = array(self.over())
        return numeric_array(numpy.cross(n, o).tolist(), float64)


