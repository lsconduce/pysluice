import sys
import os
import signal
import traceback
import numpy

from sluice import TEXTURE_REQUEST_POOL, TEXTURE_RESPONSE_POOL, APP, PROTO
from sluice.texture import Texture
from sluice.geometry import Location

from cplasma.native import Hose
from cplasma.metabolizer import Metabolizer
from cplasma import Protein, Slaw

class CTexture(Texture):

    def generate(self, ing):
        """
        Note: this takes an ingests dict as the argument, not the
        full protein (which sluice.texture.Texture does)
        """
        edge = Edge(self.bl, self.tr)
        self.draw_text(edge, self.__class__.__name__)

class CTextureFluoro():
    name = 'Generic'
    texture_class = CTexture

    def __init__(self, pool_host=None, output=None):
        self.pool_host = pool_host
        self.__children = dict()
        self.__hoses = dict()

    def get_hose(self, name):
        if self.pool_host is not None and '://' not in name:
            name = 'tcp://%s/%s' % (self.pool_host, name)
        if name not in self.__hoses:
            self.__hoses[name] = Hose(name)
        return self.__hoses[name]

    def metabolize_texture_request(self, p):
        qid = tuple(p.descrips().nth(4).emit()[1])
        if qid in self.__children:
            os.kill(self.__children[qid], signal.SIGTERM)
        pid = self.handle_texture_request(p)
        self.__children[qid] = pid
        return pid

    def handle_texture_request(self, p):
        pid = os.fork()
        if pid:
            return pid
        try:
            ing = p.ingests().emit()
            bl = Location(*ing['bl'])
            tr = Location(*ing['tr'])
            w, h = ing['px']
            if w <= 0 or h <= 0:
                # bad dimensions, ignore
                return False
            texture = self.texture_class(bl, tr, w)
            if 'disabled' in ing.get('style', '').split(','):
                texture.empty()
            else:
                texture.generate(ing)
            data = texture.png()
            data.seek(0)
            self.send_texture(p, data, px=[int(texture.w), int(texture.h)])
            sys.exit()
        except KeyboardInterrupt:
            raise
        except SystemExit:
            raise
        except Exception as e:
            traceback.print_exc()
            raise
        sys.exit()

    def file_to_numarr(self, f):
        return numpy.fromstring(f.read(), dtype=numpy.uint8)

    def send_texture(self, req, img, bl=None, tr=None, px=None):
        h = self.get_hose(TEXTURE_REQUEST_POOL)
        des = req.descrips().emit()
        ing = req.ingests().emit()
        if bl is None:
            bl = ing['bl']
        if tr is None:
            tr = ing['tr']
        if px is None:
            px = ing['px']
        qid = des[4]
        name = des[5]
        res_i = {
            'status': { 'msg': 'OK', 'tort': 0 },
            'bl': bl,
            'tr': tr,
            'px': px,
            'type': 'png',
            'bytes': self.file_to_numarr(img)
        }
        res_d = [APP, PROTO, 'response', 'texture', name, qid]
        res = Protein(res_d, res_i)
        h.deposit(res)

    def startup(self):
        self.met = Metabolizer()
        if self.pool_host is not None:
            pool = 'tcp://%s/%s' % (self.pool_host, TEXTURE_RESPONSE_POOL)
        else:
            pool = TEXTURE_RESPONSE_POOL
        self.met.poolParticipate(pool)
        self.met.appendMetabolizer(
            [APP, PROTO, 'request', 'texture', self.name],
            self.metabolize_texture_request,
            'tex-req'
        )

    def run(self):
        while True:
            self.met.metabolizeOne()
            while True:
                try:
                    if len(self.__children):
                        pid, status = os.waitpid(-1, os.WNOHANG)
                        if pid == 0:
                            break
                        for k, v in self.__children.iteritems():
                            if v == pid:
                                self.__children.pop(k, None)
                                break
                    else:
                        break
                except OSError:
                    break

    def quit(self):
        for child in self.__children.values():
            try:
                os.kill(child, signal.SIGTERM)
            except:
                pass

#def main():
#    req_hose = Hose('sluice-to-fluoro')
#    res_hose = Hose('fluoro-to-sluice')
#    req_d = ['sluice', 'prot-spec v1.0', 'request', 'texture', 'Financials']
#    res_d = ['sluice', 'prot-spec v1.0', 'response', 'texture']
#    while True:
#        req_descrips = Slaw(req_d)
#
#        pinfo = req_hose.probeForwardAwait(req_d, -1.0)
#        req = pinfo[0].ingests().emit()
#        req_descrips = pinfo[0].descrips().emit()
#        res_descrips = res_d + [req_descrips[5], req_descrips[4]]
#        bl = Location(*req['bl'])
#        tr = Location(*req['tr'])
#        tex = ZippyKiYayTexture(bl, tr, req['px'][0])
#        tex.generate(req.get('style'))
#        data = tex.png()
#        res_i = {
#            'status': { 'msg': 'OK', 'tort': 0 },
#            'bl': req['bl'],
#            'tr': req['tr'],
#            'px': req['px'],
#            'type': 'png',
#            'bytes': numpy.fromstring(data.getvalue(), dtype=numpy.uint8),
#        }
#        res = Protein(res_descrips, res_i)
#        res_hose.deposit(res)

