from collections import OrderedDict
from plasma.obtypes import v3float64, oblist, obmap

class TopologyLocation(object):
    def __init__(self, lat, lon, elev=0.0):
        self.lat = lat
        self.lon = lon
        self.elev = elev

    def __len__(self):
        return 3

    def __getitem__(self, i):
        if i == 0:
            return self.lat
        if i == 1:
            return self.lon
        if i == 2:
            return self.elev
        raise IndexError('list index out of range')

    def __setitem__(self, i, v):
        if i == 0:
            self.lat = v
        elif i == 1:
            self.lon = v
        elif i == 0:
            self.elev = v
        else:
            raise IndexError('list index out of range')

    @property
    def lat(self):
        return self.__lat

    @lat.setter
    def lat(self, v):
        self.__lat = float(v)

    @property
    def lon(self):
        return self.__lon

    @lon.setter
    def lon(self, v):
        self.__lon = float(v)

    @property
    def elev(self):
        return self.__elev

    @elev.setter
    def elev(self, v=0.0):
        self.__elev = float(v)

    def vect(self):
        return v3float64(self.__lat, self.__lon, self.__elev)

class TopologyPath(object):
    def __init__(self, verts=[]):
        self.__verts = [TopologyLocation(*v) for v in verts]

    def __len__(self):
        return len(self.__verts)

    def __iter__(self):
        return iter(self.__verts)

    def append(self, vert):
        self.__verts.append(TopologyLocation(*vert))

    def extend(self, it):
        for vert in it:
            self.append(vert)

    def pop(self, i):
        return self.__verts.pop(i)

    def oblist(self):
        return oblist(v.vect() for v in self.__verts)

class TopologyEntity(object):
    def __init__(self, id, kind, loc=None, path=None, timestamp=None, attrs={}):
        self.__id = None
        self.__kind = None
        self.__loc = None
        self.__path = None
        self.__timestamp = None
        self.__attrs = Attrs()
        self.id = id
        self.kind = kind
        self.loc = loc
        self.path = path
        self.timestamp = timestamp
        self.attrs.update(attrs)

    def keys(self):
        return filter((lambda x: getattr(self, x) is not None), ['id', 'kind', 'loc', 'path', 'timestamp', 'attrs'])

    def iterkeys(self):
        return iter(self.keys())

    def __getitem__(self, k):
        return getattr(self, k)

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, v):
        if v is None:
            raise KeyError('id')
        if not isinstance(v, basestring):
            v = unicode(v)
        self.__id = v

    @property
    def loc(self):
        return self.__loc

    @loc.setter
    def loc(self, v):
        if v is not None:
            assert(self.path is None)
            self.__loc = TopologyLocation(*v)
        else:
            self.__loc = None

    @property
    def path(self):
        return self.__path

    @path.setter
    def path(self, v):
        if v is not None:
            assert(self.loc is None)
            self.__path = TopologyPath(v)
        else:
            self.__path = None

    @property
    def kind(self):
        return self.__kind

    @kind.setter
    def kind(self, v):
        if v is None:
            raise KeyError('kind')
        if not isinstance(v, basestring):
            v = unicode(v)
        self.__kind = v

    @property
    def timestamp(self):
        return self.__timestamp

    @timestamp.setter
    def timestamp(self, v):
        if v is not None:
            if not isinstance(v, (basestring, int, long, float)):
                raise TypeError('timestamp must be a datestring or a numeric')
        self.__timestamp = v

    @property
    def attrs(self):
        return self.__attrs

    def update(self):
        d = obmap()
        d['id'] = self.id
        if self.loc:
            d['loc'] = self.loc.vect()
        elif self.path:
            d['path'] = self.path.oblist()
        else:
            raise KeyError("one of loc or path required")
        d['kind'] = self.kind
        if self.timestamp is not None:
            d['timestamp'] = self.timestamp
        return d

    def inject(self):
        d = self.update()
        d['attrs'] = obmap(self.attrs)
        return d

class ObservationEntity(object):
    def __init__(self, id, timestamp=None, attrs={}, **kwargs):
        self.__id = None
        self.__timestamp = None
        self.__attrs = Attrs()
        self.__attr_id = None
        self.id = id
        self.timestamp = timestamp
        self.attrs.update(attrs)
        ## need kwargs to deal with hyphenated keys (attr-id)
        if 'attr-id' in kwargs:
            self.attr_id = kwargs.pop('attr-id')
        elif 'attr_id' in kwargs:
            self.attr_id = kwargs.pop('attr_id')
        if len(kwargs) > 0:
            raise TypeError("__init__() got an unexpected keyword argument '%s'" % kwargs.keys()[0])

    def keys(self):
        return filter((lambda x: getattr(self, x) is not None), ['id', 'timestamp', 'attrs', 'attr_id'])

    def iterkeys(self):
        return iter(self.keys())

    def __getitem__(self, k):
        return getattr(self, k.replace('-', '_'))

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, v):
        if v is None:
            raise KeyError('id')
        if not isinstance(v, basestring):
            v = unicode(v)
        self.__id = v

    @property
    def timestamp(self):
        return self.__timestamp

    @timestamp.setter
    def timestamp(self, v):
        if v is not None:
            if not isinstance(v, (basestring, int, long, float)):
                raise TypeError('timestamp must be a datestring or a numeric')
        self.__timestamp = v

    @property
    def attrs(self):
        return self.__attrs

    @property
    def attr_id(self):
        return self.__attr_id

    @attr_id.setter
    def attr_id(self, v):
        if not v:
            self.__attr_id = None
        else:
            if not isinstance(v, basestring):
                v = unicode(v)
            self.__attr_id = v

    def update(self):
        d = obmap()
        d['id'] = self.id
        if self.timestamp:
            d['timestamp'] = self.timestamp
        d['attrs'] = obmap(self.attrs)
        return d

    def remove(self):
        d = obmap()
        d['id'] = self.id
        if self.timestamp:
            d['timestamp'] = self.timestamp
        d['attr-id'] = self.attr_id
        return d

class Attrs(OrderedDict):
    def __getitem__(self, k):
        if not isinstance(k, basestring):
            k = unicode(k)
        return super(Attrs, self).__getitem__(k)

    def __setitem__(self, k, v):
        if not isinstance(k, basestring):
            k = unicode(k)
        if v is None:
            self.pop(k, None)
            return
        if not isinstance(v, basestring):
            v = unicode(v)
        return super(Attrs, self).__setitem__(k, v)

