#!/usr/bin/env python

import sys, time, uuid, os, json, traceback
import logging
from itertools import islice
import warnings

from sluice.types import (float64, int64, obbool, v2float64, v3float64,
    unt8, oblist, numeric_array, numeric_array_from_file, obcons)
from plasma.const import POOL_WAIT_FOREVER
from plasma import Protein, Hose, HoseGang
from plasma.exceptions import *
#convert protein to yaml for file output
from plasma.yamlio import dump_yaml_protein
from sluice.entities import *

PYSLUICE_VERSION = '1.1.3'

APP = 'sluice'
PROTO = 'prot-spec v1.0'
UPDATE_LAG = 0.25
HEARTBEAT_POOL        = 'sluice-to-heart'
EDGE_REQUEST_POOL     = 'edge-to-sluice'
EDGE_RESPONSE_POOL    = 'sluice-to-edge'
NETWORK_REQUEST_POOL  = 'topo-to-sluice'
NETWORK_RESPONSE_POOL = 'sluice-to-topo'
OBSERVATION_POOL      = 'topo-to-sluice'
TEXTURE_REQUEST_POOL  = 'fluoro-to-sluice'
TEXTURE_RESPONSE_POOL = 'sluice-to-fluoro'

DEBUG = True

def chunk(it, size):
    """
    Split a iterable into chunks
    >>> list(chunk(xrange(8), 3))
    [(0, 1, 2), (3, 4, 5), (6, 7)]
    """
    it = iter(it)
    return iter(lambda: tuple(islice(it, size)), ())

class Sluice(object):
    """
    Base class for creating fluoroscope daemons.  Create your daemon as
    a subclass.
    """

    heart_listen = False
    video_listen = False
    texture_listen = True
    edge_listen = True
    network_listen = True

    def __init__(self, pool_host='localhost', output=None):
        self.pool_host = pool_host
        self.__output = output
        self.__hoses = dict()
        self.__gangs = dict()
        self.lag = False
        gang = []
        if self.heart_listen:
            gang.append(HEARTBEAT_POOL)
        if self.edge_listen:
            gang.append(EDGE_RESPONSE_POOL)
        if self.texture_listen or self.video_listen:
            gang.append(TEXTURE_RESPONSE_POOL)
        if self.network_listen:
            gang.append(NETWORK_RESPONSE_POOL)
        self.__default_gang = tuple(gang)

    def send_to_pool(self, protein, pool_name):
        if self.__output is None:
            self.get_hose(pool_name).deposit(protein)
        elif hasattr(self.__output, 'write'):
            dump_yaml_protein(protein, self.__output)

    def debug(self, msg):
        """
        Prints debugging messages to stdout if DEBUG is enabled.
        """
        if DEBUG:
            print msg

    @classmethod
    def get_share_file(cls, fn, new=False):
        """
        Find a file in OB_SHARE_PATH
        """
        return cls.get_path_file(os.getenv('OB_SHARE_PATH', '.'), fn, new)

    @classmethod
    def append_share_path(cls, path):
        return cls.append_path('OB_SHARE_PATH', path)

    @classmethod
    def prepend_share_path(cls, path):
        return cls.prepend_path('OB_SHARE_PATH', path)

    @classmethod
    def get_etc_file(cls, fn, new=False):
        """
        Find a file in OB_ETC_PATH
        """
        return cls.get_path_file(os.getenv('OB_ETC_PATH', '.'), fn, new)

    @classmethod
    def append_etc_path(cls, path):
        return cls.append_path('OB_ETC_PATH', path)

    @classmethod
    def prepend_etc_path(cls, path):
        return cls.prepend_path('OB_ETC_PATH', path)

    @classmethod
    def get_var_file(cls, fn, new=False):
        """
        Find a file in OB_VAR_PATH
        """
        return cls.get_path_file(os.getenv('OB_VAR_PATH', '.'), fn, new)

    @classmethod
    def append_var_path(cls, path):
        return cls.append_path('OB_VAR_PATH', path)

    @classmethod
    def prepend_var_path(cls, path):
        return cls.prepend_path('OB_VAR_PATH', path)

    @classmethod
    def get_path_file(cls, path, fn, new=False):
        """
        Find a file in a colon separated list of paths
        """
        for dn in path.split(':'):
            x = os.path.join(dn, fn)
            if os.path.exists(x) or new:
                return x
        logging.warning('%s not found in %s' % (fn, path))
        return None

    @classmethod
    def append_path(cls, name, path):
        if os.getenv(name):
            pathlist = os.getenv(name).split(os.pathsep)
        else:
            pathlist = []
        if path not in pathlist:
            pathlist.append(path)
            os.environ[name] = os.pathsep.join(pathlist)
        return os.getenv(name)

    @classmethod
    def prepend_path(cls, name, path):
        if os.getenv(name):
            pathlist = os.getenv(name).split(os.pathsep)
        else:
            pathlist = []
        if path in pathlist:
            pathlist.remove(path)
        pathlist.insert(0, path)
        os.environ[name] = os.pathsep.join(pathlist)
        return os.getenv(name)

    def get_hose(self, name):
        """
        If this is the first request for a given pool hose, setup a
        connection to the pool (participate) and cache it.  Subsequent
        requests for the hose will return the cached connection.
        """
        if name not in self.__hoses:
            if self.pool_host:
                pool = 'tcp://%s/%s' % (self.pool_host, name)
            else:
                pool = name
            hose = Hose.participate(pool)
            self.__hoses[name] = hose
        return self.__hoses[name]

    def get_default_gang(self):
        return self.__default_gang

    def set_default_gang(self, *names):
        """
        Set the default list of pools for gang await.
        """
        self.__default_gang = names

    def get_gang(self, *names):
        """
        Like get_hose, but set up and cache a group of hoses for gang await.
        """
        if not names:
            names = self.__default_gang
        if len(names) == 1:
            return self.get_hose(names[0])
        key = '|'.join(sorted(names))
        if key not in self.__gangs:
            gang = HoseGang()
            for name in names:
                gang.add_hose(self.get_hose(name))
            self.__gangs[key] = gang
        return self.__gangs[key]

    def await(self, names=None, timeout=POOL_WAIT_FOREVER):
        """
        Gang await.  If names is empty or not provided, uses the default
        gang.  Timeout defaults to POOL_WAIT_FOREVER.
        """
        if names is None:
            names = ()
        gang = self.get_gang(*names)
        return gang.await_next(timeout)

    def cancel_awaiter(self, names=None):
        """
        Interrupt a gang await
        """
        if names is None:
            names = ()
        gang = self.get_gang(*names)
        gang.cancel_awaiter()

    def run(self, names=None):
        """
        Main run loop.  Awaits proteins on the given set of pools (or the
        default gang), and passes them off to an appropriate handler method.
        Can also handle deferred actions with a timeout on the awaiter (set
        the global UPDATE_LAG)

        Send a SIGINT (or raise a KeyboardInterrupt exception) to terminate
        the run loop.  This does not close down connections, so you can
        call run() multiple times.
        """
        self.debug("running %s" % self.__class__.__name__)
        while True:
            try:
                if self.lag:
                    try:
                        self.debug("await for lag (%s)" % UPDATE_LAG)
                        p = self.await(names, UPDATE_LAG)
                    except PoolAwaitTimedoutException:
                        try:
                            self.lagged_update()
                        except KeyboardInterrupt:
                            raise
                        except Exception as e:
                            self.debug("EXCEPTION (lagged_update)")
                            traceback.print_exc()
                        continue
                else:
                    p = self.await(names)
                if p.matches([APP, PROTO]):
                    try:
                        resp = self.handle_protein(p)
                    except KeyboardInterrupt:
                        raise
                    except Exception as e:
                        self.debug("EXCEPTION (handle_protein)")
                        traceback.print_exc()
            except KeyboardInterrupt:
                self.cancel_awaiter(names)
                break
        self.debug("%s finished" % self.__class__.__name__)

    def lagged_update(self):
        """
        Subclass this method to handle deferred actions
        """
        self.lag = None

    def handle_protein(self, p):
        """
        Figures out which method to call for a protein based on the pool
        it came from and its descrips.
        """
        #self.debug(json.dumps(p.to_json(True), indent=2, sort_keys=True))
        if p.matches(['psa', 'heartbeat']):
            return self.handle_heartbeat(p)
        if p.matches(['psa', 'atlas-view']):
            return self.handle_atlas_view(p)
        if p.matches(['psa', 'fluoroscopes']):
            return self.handle_fluoroscope_list(p)
        if p.matches(['psa', 'fluoroscope-templates']):
            return self.handle_template_list(p)
        if p.matches(['psa', 'new-fluoro-instance']):
            return self.handle_new_fluoroscope_instance(p)
        if p.matches(['psa', 'update-fluoro']):
            return self.handle_fluoroscope_configuration_update(p)
        if p.matches(['psa', 'poked-it']):
            return self.handle_poked_it(p)
        if p.matches(['fluoroscopes', 'pos-delta']):
            return self.handle_pos_delta(p)
        if p.matches(['debug',]):
            return self.handle_debug(p)
        if p.matches(['request', 'texture']):
            return self.handle_texture_request(p)
        if p.matches(['request', 'video']):
            return self.handle_video_request(p)
        return None

    def quit(self):
        """
        Shuts down all hose and gang connections.  Theoretically, you could
        call run on this object again, and the hoses would be reestablished.
        """
        for hose in self.__hoses.itervalues():
            try:
                hose.withdraw()
            except:
                pass
        self.__hoses.clear()
        self.__gangs.clear()

    def qid_to_hex(self, qid):
        """
        Sluice gives us fluoroscope ids as a list of bytes.  This turns that
        into an easier to read hex string
        """
        return ''.join('%02x' % x for x in qid)

    def hex_to_qid(self, hexval):
        """
        Reverse of quid_to_hex
        """
        return numeric_array(list(int(hexval[i:i+2], 16) for i in xrange(0, len(hexval), 2)), unt8)

    ## ------------------------------------------------------------------
    ## HEARTBEAT PROTOCOL

    def writepid(self, pidfile=None):
        if pidfile is None:
            pidfile = os.path.join('run', self.__class__.__name__+'.pid')
        piddir = os.path.dirname(pidfile)
        if piddir and not os.path.exists(piddir):
            os.makedirs(piddir)
        file(pidfile, 'w').write(str(os.getpid()))
        return pidfile

    def startup(self):
        """
        This method waits for sluice to start up.  It first waits for the
        pool server to start (if it's not already running), then for the
        heartbeat pool to be created (if it doesn't already exist), and
        finally, for sluice to deposit a protein in the heartbeat pool.
        When this method returns, sluice is up and running and it is safe
        to call run()
        """
        #self.writepid()
        while True:
            try:
                h = self.get_hose(HEARTBEAT_POOL)
            except PoolServerUnreachException:
                ## wait for sluice to start pool tcp server
                time.sleep(1.0)
                continue
            except PoolNoSuchPoolException:
                ## wait for sluice to create heartbeat pool
                time.sleep(1.0)
                continue
            break
        search = [APP, PROTO, 'psa', 'heartbeat']
        self.debug("Waiting for Sluice to start")
        p = h.await_probe_frwd(search)
        self.debug("got heartbeat")
        self.handle_heartbeat(p)

    def alive(self, timeout=2.0):
        search = [APP, PROTO, 'psa', 'heartbeat']
        h = self.get_hose(HEARTBEAT_POOL)
        try:
            h.await_probe_frwd(search, timeout)
            return True
        except PoolAwaitTimedoutException:
            return False

    def handle_heartbeat(self, p):
        """
        This is a NOOP for now, but it could be used to alert the app
        that sluice is no longer running.

        Protein spec:
        d: [ sluice, prot-spec v1.0, psa, heartbeat ]
        i:  {
                location:
                    {
                        lat: float64 latitude,
                        lon: float64 longitude,
                        zoom: int64 zoom level
                    },
                time:
                    {
                        current: float64 time,
                        paused: bool is_paused,
                        rate: float64 rate of play,
                        live: bool is_live
                    }
            }
        """
        pass

    ## ------------------------------------------------------------------
    ## EDGE PROTOCOL

    def handle_debug(self, p):
        """
        There isn't a real debug protein in the sluice protocol, but it
        can be useful to query the state of your python app.  Subclass this
        method to dump debugging information to your taste.
        """
        ## technically not part of sluice protocol
        pass

    def handle_poked_it(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, psa, poked-it ]
        i:  { loc: vect [ latitude, longitude, elevation ] }

        OR

        d: [ sluice, prot-spec v1.0, psa, poked-it ]
        i:  {
                loc: vect [ latitude, longitude, elevation ],
                id: Str entity-id,
                time: float64 timestamp,
                attrs:
                    {
                        Str attr_1_key: Str attr_1_val,
                        ...
                    },
                kind: Str entity_type
            }
        """
        pass

    def handle_pos_delta(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, fluoroscopes, pos-delta ]
        i: {
                SlawedQID: unt8_array unique_id,
                bl: [ float64 lat, float64 lon ],
                tr: [ float64 lat, float64 lon ]
           }
        """
        pass

    def request_atlas_view(self):
        """
        Sends a request to sluice for the current atlas view.
        """
        d = [APP, PROTO, 'request', 'atlas-view']
        i = {}
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def handle_atlas_view(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, psa, atlas-view ]
        i:  {
                lat_min: float64 minimum_latitude,
                lon_min: float64 minimum_longitide,
                lat_max: float64 maximum_latitude,
                lon_max: float64 maximum_longitide,
                lat_center: float64 center_latitude,
                lon_center: float64 center_longitide,
                level: float64 zoom_level
            }
        """
        pass

    def request_zoom_loc(self, lat, lng, zoom):
        """
        Sends a request to sluice for the view to be centered on the given
        lat/lng and magnified to the given zoom.
        """
        d = [APP, PROTO, 'request', 'zoom']
        i = {
            'level': float64(zoom),
            'lat': float64(lat),
            'lon': float64(lng)
        }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def request_zoom_entity(self, entity, zoom=None):
        """
        Sends a request to sluice for the view to be centered on the given
        entity and magnified to the given zoom.
        """
        d = [APP, PROTO, 'request', 'zoom']
        i = { 'entity-id': str(entity) }
        if zoom is not None:
            i['level'] = float64(zoom)
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def request_bookmark_list(self):
        """
        Sends a request to sluice for a list of bookmarks.
        """
        d = [APP, PROTO, 'request', 'bookmarks']
        i = {}
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def handle_bookmarks_list(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, psa, bookmarks ]
        i:  {
                [
                    {
                        name: Str bookmark_name,
                        lat: float64 latitude,
                        lon: float64 longitude,
                        level: float64 zoom_level,
                    },
                    ...
                ]
            }
        """
        pass

    def handle_bookmark_error(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, response, new-bookmark ]
        i:  {
                name: Str bookmark_name,
                uid: Str unique_id_of_request,
                summary: Str error_summary,
                description: Str further_error_description
            }
        """
        pass

    def create_bookmark(self, name, lat=None, lng=None, zoom=None):
        """
        Sends a request to sluice to create a new bookmark.  If lat/lng/zoom
        are specified, those coordinates are used, otherwise the current
        view is used.
        """
        d = [APP, PROTO, 'request', 'new-bookmark']
        uid = str(uuid.uuid4())
        i = {
            'name': name,
            'uid': uid
        }
        if lat is not None and lng is not None and zoom is not None:
            i['lat'] = float64(lat)
            i['lon'] = float64(lng)
            i['level'] = float64(zoom)
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)
        return uid

    def delete_bookmark(self, name):
        """
        Sends a request to sluice to delete the named bookmark.
        """
        d = [APP, PROTO, 'request', 'delete-bookmark']
        i = { 'name': name }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def add_static_layer(self, name, image, bl, tr,
                         curtain_min_zoom_level=None, curtain_color=None,
                         curtain_fade_time=None):
        d = [APP, PROTO, 'request', 'add-static-layer']
        i = {
            'layer': name,
            'image_file': image,
            'lat_min': bl[0],
            'lon_min': bl[1],
            'lat_max': tr[0],
            'lon_max': tr[1]
        }
        if curtain_min_zoom_level and curtain_min_zoom_level is not None:
            i['curtain_min_zoom_level'] = curtain_min_zoom_level
        if curtain_color and curtain_color is not None:
            i['curtain_color'] = curtain_color
        if curtain_fade_time and curtain_fade_time is not None:
            i['curtain_fade_time'] = curtain_fade_time
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def remove_static_layer(self, name):
        d = [APP, PROTO, 'request', 'remove-static-layer']
        i = { 'layer': name }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def remove_all_static_layers(self):
        d = [APP, PROTO, 'request', 'remove-all-static-layers']
        i = { }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    #def change_zoom_parameters(self):
    #    d = [APP, PROTO, 'request', 'zoom-params']
    #    i = {
    #    }
    #    self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def request_fluoroscopes(self):
        """
        Sends a request to sluice to list the currently active fluoroscopes.
        """
        d = [APP, PROTO, 'request', 'fluoroscopes']
        i = {}
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def handle_fluoroscope_list(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [sluice, prot-spec v1.0, psa, fluoroscopes]
        i:  {
                fluoroscopes:
                    [
                        {
                            SlawedQID: unt8_array unique_id,
                            * See the Fluoroscope Configuration Protein
                              Spec for documentation on the rest of this map
                        },
                        ...
                    ]
            }
        """
        pass

    def request_fluoroscope_templates(self):
        """
        Sends a request to sluice to list the templates in the paramus.
        """
        d = [APP, PROTO, 'request', 'fluoroscope-templates']
        i = {}
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def handle_template_list(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [sluice, prot-spec v1.0, psa, fluoroscope-templates]
        i: {
            templates: [
                {
                    qid: ("SlawedQID", unt8_array),
                    * See the Fluoroscope Configuration Protein
                      Spec for documentation on the rest of this map
                },
                ...
            ]
        }
        """
        pass

    def parse_fluoroscope_config(self, fluoro):
        """
        Parses fluoroscope config attributes into a nice key/value dict
        """
        cfg = dict()
        for attr in fluoro.get('attributes', []):
            k = attr.get('name', '')
            if attr.get('selection-type', 'inclusive') == 'inclusive':
                vs = list()
                for v in attr.get('contents', []):
                    if v.get('selected', False):
                        vs.append(v.get('name', ''))
                cfg[k] = vs
            else:
                cfg[k] = None
                for v in attr.get('contents', []):
                    if v.get('selected', False):
                        cfg[k] = v.get('name', '')
                        break
        return cfg

    def update_fluoroscope_config(self, fluoro, **kwargs):
        """
        Sets attribute values in a fluoroscope config.  Does not actually
        send the updated config to Sluice.  Do this with configure_fluoroscope
        """
        for attr in fluoro.get('attributes', []):
            name = attr.get('name', '')
            if name in kwargs:
                seltype = attr.get('selection-type', 'inclusive')
                if seltype == 'inclusive':
                    if not isinstance(kwargs[name], (list, tuple)):
                        kwargs[name] = [kwargs[name]]
                    vals = set(kwargs[name])
                    for i, val in enumerate(attr.get('contents', [])):
                        vname = val.get('name', '')
                        if vname in vals or i in vals or '__ALL__' in vals:
                            val['selected'] = 1
                        else:
                            val['selected'] = 0
                else:
                    if isinstance(kwargs[name], (list, tuple)):
                        if len(kwargs[name]) == 0:
                            kwargs[name] = None
                        else:
                            kwargs[name] = kwargs[name][0]
                    found = False
                    for i, val in enumerate(attr.get('contents', [])):
                        if found:
                            val['selected'] = 0
                        else:
                            vname = val.get('name', '')
                            if vname == kwargs[name] or i == kwargs[name]:
                                val['selected'] = 1
                                found = True
                            else:
                                val['selected'] = 0
                    if not found:
                        x = attr.get('contents', [])
                        if len(x) > 0:
                            x[0]['selected'] = 1
        return fluoro

    def configure_fluoroscope(self, f):
        """
        Sends a request to sluice to change the configuration of a
        fluoroscope.
        """
        ## this may not be the right thing to do.  possible it should
        ## be provided by the app
        qid = numeric_array(list(ord(x) for x in uuid.uuid4().bytes), unt8)
        d = [APP, PROTO, 'request', 'configure-fluoro', qid]
        i = { 'SlawedQID': qid }
        i.update(f)
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)
        return qid

    def handle_fluoroscope_configuration_update(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, psa, update-fluoro, fluoro_qid_slaw ]
        i:  {
                SlawedQID: unt8_array unique_id,
                * See the Fluoroscope Configuration Protein Spec for
                  documentation on the rest of this map
            }
        """
        pass

    def create_fluoroscope_template(self, f):
        """
        Sends a request to sluice to set up a new fluoroscope type
        """
        d = [APP, PROTO, 'request', 'new-fluoro-template']
        i = {}
        i.update(f)
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def handle_new_fluoroscope_template(self, p):
        """
        Subclass this method.

        Protein spec:
        a) on success
        d: [ sluice, prot-spec v1.0, response, new-fluoro-template ]
        i:  {
                summary: Success
                description: There are n fluoroscopes in the fluoroscope
                             palette.
            }

        b) on failure
        d: [ sluice, prot-spec v1.0, response, new-fluoro-template,
             advisory ]
        i:  {
                summary: Sorry, the fluoroscope palette is full
                description: There are n fluoroscopes in the fluoroscope
                             palette.
            }
        """
        pass

    def create_fluoroscope_instance(self, f, bl=None, tr=None, windshield=False, id=None):
        """
        Sends a request to sluice to instantiate a fluoroscope on the map.
        """
        d = [APP, PROTO, 'request', 'new-fluoro-instance']
        i = { }
        if isinstance(f, basestring):
            i['name'] = f
        else:
            i['fluoro-config'] = f
        if id:
            #attach to a node
            i['id'] = id
        if bl is not None and tr is not None:
            i['bl'] = v3float64(bl[0], bl[1], 1.0)
            i['tr'] = v3float64(tr[0], tr[1], 1.0)
        if windshield:
            i['windshield'] = obbool(True)
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def handle_new_fluoroscope_instance(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, psa, new-fluoro-instance ]
        i:  {
                SlawedQID: unt8_array unique_id,
                * See the Fluoroscope Configuration Protein Spec for
                  documentation on the rest of this map
            }
        """
        pass

    def remove_fluoroscope_instance(self, qid):
        """
        Sends a request to sluice to remove a fluoroscope from the map
        """
        d = [APP, PROTO, 'request', 'remove-fluoro-instance']
        i = { 'SlawedQID': qid }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def remove_fluoroscope_template(self, qid):
        """
        Sends a request to sluice to remove a fluoroscope template from
        the paramus
        """
        d = [APP, PROTO, 'request', 'remove-fluoro-template']
        i = { 'qid': obcons(('SlawedQID', qid)) }
        p = Protein(d, i)
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def _set_fluoro_position(self, ing, windshield=None,
                             placement_entity_id=None, bl=None, tr=None,
                             loc=None, size=None, feld=None):
        """
        The fluoro can be either on/off the windshield.
        You can either specify:
          id: id of a network entity to use as the center of the fluoroscope
          bl & tr: lat long bounds for the fluoroscope
          loc & size & feld: location in feld coords.  feld is optional (main if not specified)
        if no position is specified, none if forwarded
        """
        #make sure only 1 type is specified
        loc_id = placement_entity_id is not None
        loc_bltr = bl is not None or tr is not None
        loc_feld = loc is not None or size is not None or feld is not None
        if (int(loc_id) + int(loc_bltr) + int (loc_feld)) > 1:
            raise PlasmaException('Only specify 1 type of position: id, bl/tr, or feld/loc/size')
        if windshield is not None:
            ing['windshield'] = obbool(windshield)
        if loc_id:
            ing['id'] = placement_entity_id
        if loc_bltr:
            ing['bl'] = [bl[0], bl[1]]
            ing['tr'] = [tr[0], tr[1]]
        if loc_feld:
            #In sluice, default loc = [0, 0]
            #In sluice, default size = [1.0, 1.0]
            #In sluice, default feld = main
            #But at least 1 field must be specified
            if loc is not None:
                ing['loc'] = [loc[0], loc[1]]
            if size is not None:
                ing['size'] = [size[0], size[1]]
            if feld is not None:
                if feld not in ('main', 'left', 'right'):
                    raise PlasmaException('Feld must be either "left", "right", or "main"')
                ing['feld'] = feld

    def request_video_fluoro(self, name, vid_type, vid_path, windshield=None,
                             placement_entity_id=None,
                             bl=None, tr=None, loc=None, size=None, feld=None):
        """
        Sends a request to sluice to create a video fluoroscope.  This
        uses the static video source where the source dose not change,
        so a type and path for the video are required.
        type: 'file' or 'pool'
        vid_path: path to file in share dir, or pool name
        See _set_fluoro_position for info on the position related arguments
          (windshield, id, bl, tr, loc, size, feld)
        """
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ing = {
            'fluoro-config': {
                'name': name,
                'type': 'video',
                'vid_loc': {
                    'type': vid_type,
                    'path': vid_path
                }
            }
        }
        self._set_fluoro_position(ing, windshield=windshield,
                                  placement_entity_id=placement_entity_id,
                                  bl=bl, tr=tr, loc=loc, size=size, feld=feld)
        self.send_to_pool(Protein(descrips, ing), EDGE_REQUEST_POOL)

    def request_texture_fluoro(self, name, daemon, static=False, attrs=None,
                               update_frequency=None, disable_notifications=None,
                               windshield=None, placement_entity_id=None,
                               bl=None, tr=None, loc=None, size=None, feld=None):
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        config = {
            'name': name,
            'daemon': daemon,
            'type': 'texture',
        }
        if attrs is not None:
            config['attrs'] = attrs
        if static:
            config['image-is-static'] = True
            config['ignore-movement'] = True
        if disable_notifications is not None:
            config['disable-notifications'] = obbool(disable_notifications)
        if update_frequency is not None:
            config['update-frequency'] = float64(update_frequency)
        ing = {'fluoro-config': config}
        self._set_fluoro_position(ing, windshield=windshield,
                                  placement_entity_id=placement_entity_id,
                                  bl=bl, tr=tr, loc=loc, size=size, feld=feld)
        self.send_to_pool(Protein(descrips, ing), EDGE_REQUEST_POOL)

    def request_web_fluoro(self, name, url, post=None, visible=None,
                           background=None, windshield=None,
                           placement_entity_id=None,
                           bl=None, tr=None, loc=None, size=None, feld=None):
        """
        Sends a request to sluice to create a web fluoroscope.  There are 3
        optional arguments, post, visible, and background:
          - post: POST data for the request
          - visible:  hide/show a browser without removing it
          - background: background color
        See _set_fluoro_position for info on the position related arguments
          (windshield, id, bl, tr, loc, size, feld)
        """
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        config = {
            'name': name,
            'type': 'web',
            'url': url
        }
        if visible is not None:
            config['visible'] = obbool(visible)
        if background is not None:
            #TODO: support sending background = None?
            config['background'] = background
        if post is not None:
            config['post'] = post
        ing = {'fluoro-config': config}
        self._set_fluoro_position(ing, windshield=windshield,
                                  placement_entity_id=placement_entity_id,
                                  bl=bl, tr=tr, loc=loc, size=size, feld=feld)
        self.send_to_pool(Protein(descrips, ing), EDGE_REQUEST_POOL)

    def request_localized_webpage(self, id, url, size=None, background=None):
        warnings.warn('this is deprecated, use request_web_fluoro', DeprecationWarning)
        d = [APP, PROTO, 'request', 'web']
        i = {
            'id': id,
            'url': url,
            'background': background
        }
        if size is not None:
            if isinstance(size, (int, float)):
                i['size'] = v2float64(size, size)
            elif hasattr(size, '__len__') and 1 < len(size):
                i['size'] = v2float64(size[0], size[1])
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def request_webpage(self, name, url, post=None, size=None, loc=None,
                        feld='main', visible = True, background=None):
        """
        Sends a request to sluice to display a webpage.  name and url are
        required.  If post is provided, it is sent as the POST data for
        the request.
        """
        warnings.warn('this is deprecated, use request_web_fluoro', DeprecationWarning)

        windshield = {
            'name': name,
            'feld': feld,
            'url': url,
            'visible': obbool(visible),
            'background': background
        }

        d = [APP, PROTO, 'request', 'web']
        i = {
            'windshield': [ windshield ]
        }

        if post is not None:
            i['windshield'][0]['post'] = post

        if size is not None:
            if isinstance(size, (int, float)):
                windshield['size'] = v2float64(size, size)
            elif hasattr(size, '__len__') and 1 < len(size):
                windshield['size'] = v2float64(size[0], size[1])

        if loc is not None and hasattr(loc, '__len__') and 1 < len(loc):
            windshield['loc'] = v2float64(loc[0], loc[1])

        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def inject_javascript(self, name, js):
        """
        Sends a request to sluice to run the given javascript code in the
        named web browser.
        """
        d = [APP, PROTO, 'request', 'web']
        i = {
            'windshield': [
                {
                    'name': name,
                    'js': js
                }
            ]
        }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def remove_webpage(self, name):
        """
        Sends a request to sluice to remove the named web browser from
        the display.
        """
        d = [APP, PROTO, 'request', 'remove-web-instance']
        i = { 'windshield': [ { 'name': name } ] }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def remove_localized_webpage(self):
        d = [APP, PROTO, 'request', 'remove-web-instance']
        i = { 'remove-atlas-instance': True, 'windshield': [ { 'name': 'webby' } ] }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def request_current_time(self):
        """
        Sends a request to sluice for its notion of the current time.
        """
        d = [APP, PROTO, 'request', 'time']
        i = {}
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def handle_current_time(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, psa, time ]
        i:  {
                current: float64 time,
                paused: bool is_paused,
                rate: float64 rate of play,
                live: bool is_live,
                min: float64 earliest_known_time,
                max: float64 most_recent_time
            }
        """
        pass

    def set_live(self):
        """
        Sends a request to sluice to fast forward to the present.
        """
        d = [APP, PROTO, 'request', 'set-time']
        i = { 'live': obbool(True) }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def set_time(self, t, rate, pause):
        """
        Sends a request to sluice to show the state of the world at the
        given time.
        """
        d = [APP, PROTO, 'request', 'set-time']
        i = {
            'time': t,
            'rate': float64(rate),
            'pause': obbool(pause)
        }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def save_layout(self, name):
        """
        Sends a request to sluice to save the current view to name.
        """
        d = [APP, PROTO, 'request', 'mummify']
        i = { 'filename': name }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def open_layout(self, name):
        """
        Sends a request to sluice to load a previously saved view.
        """
        d = [APP, PROTO, 'request', 'vivify']
        i = { 'filename': name }
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    def force_zoom_level(self, level=None):
        """
        Forced zoom levels
        Sluice now respects a new descrips list sent to edge-to-sluice:

            descrips:
            - sluice
            - "prot-spec v1.0"
            - store
            - force-levels
            ingests:
              forced-level: "zoom level here"
              # or leave this out if you don't want to set it to null

        A present *and* non-null `forced-level` ingest will cause Sluice --
        every new Fluoroscope to be created, at least -- to use the named
        zoom level and only the named zoom level.  If you supply either a
        null `forced-level` value or no `forced-level` value Sluice will go
        back to choosing a zoom-level based on visible latitude.

        This zoom level will be saved to and loaded from mummy proteins.
        """
        d = [APP, PROTO, 'store', 'force-levels']
        i = {}
        if level is not None:
            i['forced-level'] = level
        self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

    ## ------------------------------------------------------------------
    ## NETWORK (TOPOLOGY) PROTOCOL

    def handle_topology_request(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, topology, request ]
        i:  { }
        """
        pass

    def _batch_send_to_pool(self, pool, d, raw_i, i_type, batch_size=100):
        """
        Batch the ingests before sending to the pool
        """
        n = 0
        for batch in chunk(iter(raw_i), batch_size):
            batch = list(batch)
            n += len(batch)
            i = { i_type: batch }
            self.send_to_pool(Protein(d, i), pool)
        return n

    def inject_topology(self, entities):
        """
        Adds new entities to the fluoroscope.  entities should be an
        iterable of dicts of the form:
            {
                'id': str,
                'loc': v3float64(lat, lng, elev),
                'kind': str,
                'timestamp': float,
                'attrs': {
                    str(key): str(val),
                    ...
                }
            }
        or
            {
                'id': str,
                'path': [
                    v3float64(lat, lng, elev),
                    v3float64(lat, lng, elev),
                    ...
                ],
                'kind': str,
                'timestamp': float,
                'attrs': {
                    str(key): str(val),
                    ...
                }
            }
        """

        protein_spec = """
        d: [ sluice, prot-spec v1.0, topology, add, x/n ]
        i: {
                topology:
                    [
                        {
                            id: Str entity_id,
                            loc: vect [lat, lon, elevation],
                            kind: Str entity_type,
                            timestamp: <Str timestamp, float64 timestamp>,
                            attrs:
                              {
                                  Str attr_1_id: Str attr_1_val,
                                  Str attr_2_id: Str attr_2_val,
                                  ...
                              }
                        },
                        {
                            id: Str entity_id,
                            path:
                                [
                                    vect [lat, lon, elevation],
                                    vect [lat, lon, elevation],
                                    vect [lat, lon, elevation],
                                    ...
                                ],
                            kind: Str entity_type,
                            timestamp: <Str timestamp, float64 timestamp>,
                            attrs:
                                {
                                    Str attr_1_id: Str attr_1_val,
                                    Str attr_2_id: Str attr_2_val,
                                    ...
                                }
                        }, ...
                    ]
           }
        """
        entities = iter(TopologyEntity(**e).inject() for e in entities)
        d = [APP, PROTO, 'topology', 'add']
        n = self._batch_send_to_pool(NETWORK_REQUEST_POOL, d, entities, 'topology')
        self.debug("injected %d entities" % n)

    def update_topology(self, entities):
        """
        Updates positional data for existing fluoroscope entities.
        entities should be an iterable of dicts of the form:
            {
                'id': str,
                'loc': v3float64(lat, lng, elev),
                'kind': str,
                'timestamp': float
            }
        or
            {
                'id': str,
                'path': [
                    v3float64(lat, lng, elev),
                    v3float64(lat, lng, elev),
                    ...
                ],
                'kind': str,
                'timestamp': float
            }
        """
        protein_spec = """
        d: [ sluice, prot-spec v1.0, topology, change ]
        i:  {
                topology:
                    [
                        {
                            id: Str entity_id,
                            kind: Str entity_type,
                            timestamp: <Str timestamp, float64 timestamp>,
                            loc: vect [lat, lon, elevation]
                        },
                        {
                            id: Str entity_id,
                            kind: Str entity_type,
                            timestamp: <Str timestamp, float64 timestamp>,
                            path:
                                [
                                    vect [lat, lon, elevation],
                                    vect [lat, lon, elevation],
                                    vect [lat, lon, elevation],
                                    ...
                                ]
                        }, ...
                    ]
            }
        """
        entities = iter(TopologyEntity(**e).update() for e in entities)
        d = [APP, PROTO, 'topology', 'change']
        entities_iter = iter(entities)
        finished = False
        n = 0
        while not finished:
            topo = []
            for j in xrange(100):
                try:
                    topo.append(entities_iter.next())
                except StopIteration:
                    finished = True
                    break
            n += len(topo)
            if len(topo):
                i = { 'topology': topo }
                self.send_to_pool(Protein(d, i), NETWORK_REQUEST_POOL)
            else:
                break
        self.debug("updated %d entities" % n)

    def remove_topology(self, ids):
        """
        Sends a request to sluice to remove entities from the fluoroscope.
        ids should be an iterable of the id strings of the entities to
        be removed.
        """
        protein_spec = """
        d: [ sluice, prot-spec v1.0, topology, remove ]
        i:  {
                topology:
                    [
                        { id: Str entity_id }.
                        { id: Str entity_id },
                        ...
                    ]
            }
        """
        if ids:
            d = [APP, PROTO, 'topology', 'remove']
            i = { 'topology': list({ 'id': x } for x in ids) }
            self.send_to_pool(Protein(d, i), NETWORK_REQUEST_POOL)

    def clear_topology(self, *args, **kwargs):
        """
        Sends a request to sluice to remove all entities.
        """
        protein_spec = """
        d: [ sluice, prot-spec v1.0, topology, clear ]
        i:  { }
        """
        d = [APP, PROTO, 'topology', 'clear']
        i = { }
        self.send_to_pool(Protein(d, i), NETWORK_REQUEST_POOL)

    ## ------------------------------------------------------------------
    ## OBSERVATION PROTOCOL

    def update_observations(self, update=None, remove=None):
        """
        Sends a request to sluice to update attribute data for existing
        entities.  update should be an iterable of dicts of the form:
            {
                'id': str,
                'timestamp': float,
                'attrs': {
                    str(key): str(val),
                    ...
                }
            }
        To remove an attribute from an entity, provide remove as an
        iterable of dicts of the form:
            {
                'id': str,
                'attr-id': str(key),
                'timestamp': float
            }
        """

        protein_spec = """
        d: [ sluice, prot-spec v1.0, observations ]
        i:  {
                observations:
                    [
                        {
                            id: Str entity_id,
                            timestamp: Str timestamp,
                            attrs:
                                {
                                    Str attr_1_id: Str attr_1_val,
                                    Str attr_2_id: Str attr_2_val,
                                    ...
                                },
                        },
                        ...
                    ]
                remove-observations:
                    [
                        {
                            id: Str entity_id,
                            attr-id: Str attribute_id,
                            timestamp: Str timestamp
                        },
                        ...
                    ]
            }
        """
        d = [APP, PROTO, 'observations']
        if update:
            update = iter(ObservationEntity(**e).update() for e in update)
            n = self._batch_send_to_pool(OBSERVATION_POOL, d, update, 'observations')
            self.debug("updated %d observations " % n)
        if remove:
            remove = iter(ObservationEntity(**e).remove() for e in remove)
            n = self._batch_send_to_pool(OBSERVATION_POOL, d, remove, 'remove-observations')
            self.debug("removed %d observations " % n)


    ## ------------------------------------------------------------------
    ## TEXTURE PROTOCOL

    def handle_texture_request(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, request, texture, <fluoro_qid_slaw>,
             <fluoro_type> ]
        i:  {
                bl: [ float64 lat, float64 lon ],
                tr: [ float64 lat, float64 lon ],
                style: Str style,
                px: [ float64 width, float64 height ],
                name: Str name,
                time: float64 current time,
                live: bool is_live
            }
        """
        pass

    def send_texture(self, req, img, bl=None, tr=None, px=None):
        """
        Sends the requested texture to sluice.
        * req is the protein from the texture request.
        * img is a file-like object with the PNG image data.
        * bl and tr are the bottom left and top right lat/lng coordinates,
          which will be taken from the request if not provided.
        * px is the width and height of the image, which will be taken
          from the request if not provided
        """
        if bl is None:
            bl = req.ingests()['bl']
        if tr is None:
            tr = req.ingests()['tr']
        if px is None:
            px = req.ingests()['px']
        d = [APP, PROTO, 'response', 'texture',
             req.descrips()[5], req.descrips()[4]]
        i = {
            'status': {
                'tort': int64(0),
                'msg': 'OK'
            },
            'type': 'png',
            'tr': oblist([float64(tr[0]), float64(tr[1])]),
            'bl': oblist([float64(bl[0]), float64(bl[1])]),
            'px': oblist([int64(px[0]), int64(px[1])]),
            'bytes': self.file_to_numarr(img)
        }
        self.send_to_pool(Protein(d, i), TEXTURE_REQUEST_POOL)

    def file_to_numarr(self, fp):
        """
        Convenience method to convert a file-like object into an array of
        bytes.
        """
        return numeric_array_from_file(fp)

    def handle_video_request(self, p):
        """
        Subclass this method.

        Protein spec:
        d: [ sluice, prot-spec v1.0, request, video, <fluoro_qid_slaw>,
             <fluoro_type> ]
        i:  {
                bl: [ float64 lat, float64 lon ],
                tr: [ float64 lat, float64 lon ],
                style: Str style,
                px: [ float64 width, float64 height ],
                name: Str name,
                time: float64 current time,
                live: bool is_live
            }
        """
        pass

