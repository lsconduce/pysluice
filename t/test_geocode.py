import sys, os
sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))
import unittest
import sluice.geocode
import json

class TestGeocode(unittest.TestCase):

    def test_parse_address(self):
        tests = [
            {
                "string": "123 Fake St. Springfield, NY 12345",
                "address": "123 Fake St",
                "number": "123",
                "street": "Fake St",
                "street_name": "Fake",
                "city": "Springfield",
                "state": "NY",
                "zipcode": "12345"
            },
            {
                "string": "1026 e orange grove ave pasadena, ca 91106",
                "address": "1026 E Orange Grove Ave",
                "number": "1026",
                "street": "E Orange Grove Ave",
                "street_name": "Orange Grove",
                "city": "Pasadena",
                "state": "CA",
                "zipcode": "91106"
            },
            {
                "string": "1219 S Alta Vista Ave. #113 Monrovia, CA 91016",
                "address": "1219 S Alta Vista Ave # 113",
                "number": "1219",
                "street": "S Alta Vista Ave",
                "street_name": "Alta Vista",
                "unit": "# 113",
                "city": "Monrovia",
                "state": "CA",
                "zipcode": "91016"
            },
            {
                "string": "9875 S 24th St. NW\nSuite 100\nSeattle, WA 98765-1234",
                "address": "9875 S 24th St NW Suite 100",
                "number": "9875",
                "street": "S 24th St NW",
                "street_name": "24th",
                "unit": "Suite 100",
                "city": "Seattle",
                "state": "WA",
                "zipcode": "98765",
                "plus4": "1234"
            },
            #{
            #    "string": "53rd st. & 3rd ave, New York, NY 12345",
            #    "address": "53rd",
            #    "street": "53rd",
            #    "street_name": "53rd",
            #    "intersection": "3rd",
            #    "city": "New York",
            #    "state": "NY",
            #    "zipcode": "12345"
            #}
        ]
        for exp in tests:
            print exp['string']
            act = sluice.geocode.parse_address(exp['string'])
            self.assertDictEqual(exp, act)

    def test_zipcode_lookup(self):
        exp = {
            'city': 'Monrovia',
            'state': 'CA',
            'zipcode': '91016',
            'fips': '06037',
            'lat': 34.140806,
            'lng': -118.00184,
            'timezone': -8,
            'dst': True,
        }
        act = sluice.geocode.zipcode_lookup(91016)
        self.assertDictEqual(exp, act)

    def test_zipcode_reverse_lookup(self):
        exp = {
            'city': 'Monrovia',
            'state': 'CA',
            'zipcode': '91016',
            'fips': '06037',
            'lat': 34.140806,
            'lng': -118.00184,
            'timezone': -8,
            'dst': True,
        }
        act = sluice.geocode.zipcode_reverse_lookup(34.154955, -117.999234)
        self.assertDictEqual(exp, act)

    def test_google_lookup(self):
        exp = {
            'address': '219 N Ivy Ave',
            'number': u'219',
            'street': u'N Ivy Ave',
            'city': u'Monrovia',
            'state': u'CA',
            'zipcode': u'91016',
            'county': u'Los Angeles County',
            'country': u'United States',
            'lat': 34.154955,
            'lng': -117.999234
        }
        act = sluice.geocode.google_lookup('219 n ivy ave monrovia, ca 91016')
        act.pop('raw', None)
        self.assertDictEqual(exp, act)

    def test_google_reverse_lookup(self):
        exp = {
            'address': '219 N Ivy Ave',
            'number': u'219',
            'street': u'N Ivy Ave',
            'city': u'Monrovia',
            'state': u'CA',
            'zipcode': u'91016',
            'county': u'Los Angeles County',
            'country': u'United States',
            'lat': 34.154955,
            'lng': -117.999234
        }
        act = sluice.geocode.google_reverse_lookup(34.154955, -117.999234)
        act.pop('raw', None)
        self.assertDictEqual(exp, act)

    def test_osm_lookup(self):
        exp = {
            'address': '219 North Ivy Avenue',
            'number': u'219',
            'street': u'North Ivy Avenue',
            'city': u'Monrovia',
            'county': u'Los Angeles',
            'state': u'California',
            'zipcode': u'91016',
            'country': u'United States of America',
            'country_code': u'us',
            'lat': 34.1548181509485,
            'lng': -117.998999514373
        }
        act = sluice.geocode.osm_lookup('219 n ivy ave monrovia, ca 91016')
        act.pop('raw', None)
        self.assertDictEqual(exp, act)

    def test_osm_reverse_lookup(self):
        exp = {
            'address': 'North Ivy Avenue',
            'number': None,
            'street': u'North Ivy Avenue',
            'city': u'Monrovia',
            'county': u'Los Angeles',
            'state': u'California',
            'zipcode': u'91016',
            'country': u'United States of America',
            'country_code': u'us',
            'lat': 34.1569274,
            'lng': -117.9989557
        }
        act = sluice.geocode.osm_reverse_lookup(34.1548181509485, -117.998999514373)
        act.pop('raw', None)
        self.assertDictEqual(exp, act)

    def test_postgis_lookup(self):
        exp = {
            'address': '219 N Ivy Ave',
            'number': '219',
            'street': 'N Ivy Ave',
            'direction': 'N',
            'street_name': 'Ivy',
            'type': 'Ave',
            'quadrant': None,
            'unit': None,
            'city': 'Monrovia',
            'state': 'CA',
            'zipcode': '91016',
            'lat': 34.1547398434692,
            'lng': -117.999007163348,
            'rating': 0,
        }
        act = sluice.geocode.postgis_lookup('219 n ivy ave monrovia, ca 91016')[0]
        act.pop('raw', None)
        self.assertDictEqual(exp, act)

    def test_postgis_reverse_lookup(self):
        exp = {
            'address': '219 Ivy Ave',
            'number': '219',
            'street': 'Ivy Ave',
            'direction': None,
            'street_name': 'Ivy',
            'type': 'Ave',
            'quadrant': None,
            'unit': None,
            'city': 'Monrovia',
            'state': 'CA',
            'zipcode': '91016',
            'lat': 34.1547398434692,
            'lng': -117.999007163348,
            'cross_street': 'Cedar Ave',
        }
        act = sluice.geocode.postgis_reverse_lookup(34.1547398434692, -117.999007163348)[0]
        act.pop('raw', None)
        self.assertDictEqual(exp, act)

if '__main__' == __name__:
    unittest.main()
