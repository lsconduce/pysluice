import unittest
from mock import patch
from sluice.settings import datasets


class DatasetsTest(unittest.TestCase):

    @patch.object(datasets.Sluice, 'get_share_file')
    def test_add_template_misc_extensions(self, sluice_mock):
        #Make sure .protein is not appended to the filename
        dc = datasets.Dataset('demo_id', 'demo_name')
        for ext in ('protein', 'prot', 'yaml'):
            fname = 'fake_topo.%s' % ext
            dc.add_topo(fname)
            #should be called with the filename, nothing appended
            sluice_mock.assert_called_with(fname)
            sluice_mock.reset_mock()
            dc.add_obs(fname)
            #should be called with the filename, nothing appended
            sluice_mock.assert_called_with(fname)
            sluice_mock.reset_mock()

if '__main__' == __name__:
    unittest.main()

