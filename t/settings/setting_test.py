import unittest
from mock import patch
from sluice import settings


class SettingsTest(unittest.TestCase):

    @patch.object(settings.Daemon, 'standard_set')
    @patch.object(settings, 'parse_yaml_protein')
    @patch.object(settings.Fluoro, 'from_config')
    def test_add_template_misc_extensions(self, from_config_mock, parse_yaml_mock, standard_set_mock):
        #Make sure various file extensions are supported for fluoroscope templates
        sc = settings.Settings('fakeid')
        #Video Fluoros are simple, so use them
        from_config_mock.return_value = settings.VideoFluoro('demo_id')
        # Try a few different extensions to make sure they load
        for ext in ('protein', 'yaml'):
            num_scopes_pre = len(sc.get_templates())
            with patch('__builtin__.file') as mock_file:
                sc.add_template('demo_name/fluoro_template_fname.%s' % ext)
                num_scopes_post = len(sc.get_templates())
                #expect 1 new scope
                self.assertEquals(num_scopes_pre + 1, num_scopes_post)

if '__main__' == __name__:
    unittest.main()

