import sys, os, unittest, json
sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))

class SuiteGenerator(type):
    """
    Metaclass for generating test methods from JSON
    """
    def __new__(self, cls, parents, cls_dict):
        #hack: this class must be in the same folder as the subclass using it
        module_name = cls_dict['__module__']
        if module_name.endswith('_test'):
            module_name = module_name.replace('_test', '').replace('_test.', '.')
            test_dir = os.path.dirname(__file__)
            conf_filepath = os.path.join(test_dir, 'conf', '%s.json' % module_name)

            test_data = json.load(open(conf_filepath))
            def get_test_data(self):
                """
                Return the raw json data as a dict
                """
                return test_data

            cls_dict['get_test_data'] = get_test_data

            test_defitions = test_data['tests']
            def test_generator(test_name):
                def test_function(self):
                    self._run_test(test_name, test_defitions[test_name])
                return test_function
            for fun_name in test_defitions:
                if fun_name in cls_dict:
                    raise Exception('Tests not compatible')
                cls_dict['test_%s' % fun_name] = test_generator(fun_name)
        return super(SuiteGenerator, self).__new__(self, cls, parents, cls_dict)

class JSONTestCase(unittest.TestCase):
    __metaclass__ = SuiteGenerator
    obj = None
    skipAll = False

    def get_output(self, test_name, test_obj):
        td = self.get_test_data()
        mod = test_obj.get('module', td.get('module', None))
        cls = test_obj.get('class', td.get('class', None))
        obj = self.obj
        method = test_obj.get('method', td.get('method', None))
        args = test_obj.get('args', [])
        kwargs = test_obj.get('kwargs', {})
        if obj:
            return getattr(obj, method)(*args, **kwargs)
        if mod:
            if mod not in sys.modules:
                __import__(mod)
            mod = sys.modules[mod]
        if cls:
            if not mod:
                cls = globals()[cls]
            else:
                cls = getattr(mod, cls)
            cstr = test_obj.get('constructor', td.get('constructor', {}))
            if isinstance(cstr, list):
                c_args = cstr
                c_kwargs = {}
            elif isinstance(cstr, dict):
                x = set(('args', 'kwargs'))
                if set(cstr.keys()).union(x) == x:
                    c_args = cstr.get('args', [])
                    c_kwargs = cstr.get('kwargs', {})
                else:
                    c_args = []
                    c_kwargs = cstr
            obj = cls(*c_args, **c_kwargs)
            return getattr(obj, method)(*args, **kwargs)
        if '.' in method:
            path = method.split('.')
            method = path.pop()
        else:
            path = []
        for p in path:
            if mod is None:
                mod = globals()[p]
            else:
                mod = getattr(mod, p)
        return getattr(mod, method)(*args, **kwargs)
        
    def _run_test(self, test_name, test_obj):
        if self.skipAll:
            self.skipTest(self.skipAll)
        if 'exception' in test_obj:
            try:
                self.get_output(test_name, test_obj)
                self.fail("%s not raised" % test_obj['exception'])
            except Exception as e:
                self.assertEqual(test_obj['exception'], e.__class__.__name__)
        else:
            actual = self.get_output(test_name, test_obj)
            actual = self.ignore(test_obj['expected'], actual)
            actual = self.make_unicode(actual)
            if isinstance(test_obj['expected'], dict):
                self.assertDictEqual(test_obj['expected'], actual)
            elif isinstance(test_obj['expected'], list):
                self.assertListEqual(test_obj['expected'], actual)
            else:
                self.assertEqual(test_obj['expected'], actual)

    def make_unicode(self, obj):
        if isinstance(obj, list):
            return list(self.make_unicode(v) for v in obj)
        elif isinstance(obj, set):
            return set(self.make_unicode(v) for v in obj)
        elif isinstance(obj, dict):
            return dict((self.make_unicode(k), self.make_unicode(v)) for k, v in obj.iteritems())
        elif isinstance(obj, str):
            return unicode(obj)
        elif isinstance(obj, tuple):
            return tuple(self.make_unicode(v) for v in obj)
        else:
            return obj

    def ignore(self, expected, actual):
        if isinstance(expected, dict):
            new = dict()
            for k, v in expected.iteritems():
                if v == '__OPTIONAL__':
                    new[k] = '__OPTIONAL__'
                elif v == '__IGNORE__' and k in actual:
                    new[k] = '__IGNORE__'
                elif k in actual:
                    new[k] = self.ignore(v, actual[k])
            return new
        elif isinstance(expected, list):
            new = list()
            has_optional = False
            for i, v in enumerate(expected):
                if has_optional:
                    raise Exception("__OPTIONAL__ may only be last element of a list")
                if v == '__OPTIONAL__':
                    new.append('__OPTIONAL__')
                    has_optional = True
                elif v == '__IGNORE__' and len(actual) > i:
                    new.append('__IGNORE__')
                elif len(actual) > i:
                    new.append(self.ignore(v, actual[i]))
                else:
                    break
            return new
        elif isinstance(expected, basestring):
            if expected == '__OPTIONAL__':
                return expected
            if expected == '__IGNORE__':
                return expected
        return actual

