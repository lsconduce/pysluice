import unittest
from mock import Mock, patch
from plasma.obtypes import oblist, obmap, v3float64

import sluice.api
from sluice.entities import *

class EntityTest(unittest.TestCase):

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testInjectDicts(self, send_to_pool):
        s = sluice.api.Sluice()
        entities = [
            {
                'id': 'test-1',
                'kind': 'test-ent',
                'loc': [30, -75],
                'attrs': {
                    'name': 'Test 1'
                }
            },
            {
                'id': 'test-2',
                'kind': 'test-path',
                'path': [[30, -75], [31, -74], [32, -73]],
                'attrs': {
                    'name': 'Test 2'
                }
            }
        ]
        s.inject_topology(entities)
        actual = send_to_pool.call_args[0][0].ingests().get('topology')
        expected = [
            obmap([('id', 'test-1'),
                   ('loc', v3float64(30.0, -75.0, 0.0)),
                   ('kind', 'test-ent'),
                   ('attrs', obmap([('name', 'Test 1')]))]),
            obmap([('id', 'test-2'),
                   ('path', oblist([
                    v3float64(30.0, -75.0, 0.0),
                    v3float64(31.0, -74.0, 0.0),
                    v3float64(32.0, -73.0, 0.0)
                   ])),
                   ('kind', 'test-path'),
                   ('attrs', obmap([('name', 'Test 2')]))])
        ]
        self.assertListEqual(expected, actual)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testInjectObjects(self, send_to_pool):
        s = sluice.api.Sluice()
        entities = [
            TopologyEntity(id='test-1', kind='test-ent', loc=[30, -75], attrs={ 'name': 'Test 1' }),
            TopologyEntity(id='test-2', kind='test-path', path=[[30, -75], [31, -74], [32, -73]], attrs={ 'name': 'Test 2' })
        ]
        s.inject_topology(entities)
        actual = send_to_pool.call_args[0][0].ingests().get('topology')
        expected = [
            obmap([('id', 'test-1'),
                   ('loc', v3float64(30.0, -75.0, 0.0)),
                   ('kind', 'test-ent'),
                   ('attrs', obmap([('name', 'Test 1')]))]),
            obmap([('id', 'test-2'),
                   ('path', oblist([
                    v3float64(30.0, -75.0, 0.0),
                    v3float64(31.0, -74.0, 0.0),
                    v3float64(32.0, -73.0, 0.0)
                   ])),
                   ('kind', 'test-path'),
                   ('attrs', obmap([('name', 'Test 2')]))])
        ]
        self.assertListEqual(expected, actual)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testInjectBadDicts(self, send_to_pool):
        s = sluice.api.Sluice()
        entities = [
            {
                'kind': 'test-ent',
                'loc': [30, -75],
            }
        ]
        self.assertRaises(TypeError, s.inject_topology, entities)
        entities = [
            {
                'id': 'foo',
                'kind': 'test-ent',
                'loc': [30, -75],
                'path': [[30, -75], [31, -74], [32, -73]]
            }
        ]
        self.assertRaises(AssertionError, s.inject_topology, entities)
        entities = [
            {
                'id': 'foo',
                'kind': 'test-ent'
            }
        ]
        self.assertRaises(KeyError, s.inject_topology, entities)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testUpdateDicts(self, send_to_pool):
        s = sluice.api.Sluice()
        entities = [
            {
                'id': 'test-1',
                'kind': 'test-ent',
                'loc': [30, -75],
            },
            {
                'id': 'test-2',
                'kind': 'test-path',
                'path': [[30, -75], [31, -74], [32, -73]],
            }
        ]
        s.update_topology(entities)
        actual = send_to_pool.call_args[0][0].ingests().get('topology')
        expected = [
            obmap([('id', 'test-1'),
                   ('loc', v3float64(30.0, -75.0, 0.0)),
                   ('kind', 'test-ent')]),
            obmap([('id', 'test-2'),
                   ('path', oblist([
                    v3float64(30.0, -75.0, 0.0),
                    v3float64(31.0, -74.0, 0.0),
                    v3float64(32.0, -73.0, 0.0)
                   ])),
                   ('kind', 'test-path')])
        ]
        self.assertListEqual(expected, actual)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testUpdateObjects(self, send_to_pool):
        s = sluice.api.Sluice()
        entities = [
            TopologyEntity(id='test-1', kind='test-ent', loc=[30, -75]),
            TopologyEntity(id='test-2', kind='test-path', path=[[30, -75], [31, -74], [32, -73]])
        ]
        s.update_topology(entities)
        actual = send_to_pool.call_args[0][0].ingests().get('topology')
        expected = [
            obmap([('id', 'test-1'),
                   ('loc', v3float64(30.0, -75.0, 0.0)),
                   ('kind', 'test-ent')]),
            obmap([('id', 'test-2'),
                   ('path', oblist([
                    v3float64(30.0, -75.0, 0.0),
                    v3float64(31.0, -74.0, 0.0),
                    v3float64(32.0, -73.0, 0.0)
                   ])),
                   ('kind', 'test-path')])
        ]
        self.assertListEqual(expected, actual)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testUpdateBadDicts(self, send_to_pool):
        s = sluice.api.Sluice()
        entities = [
            {
                'kind': 'test-ent',
                'loc': [30, -75],
            }
        ]
        self.assertRaises(TypeError, s.update_topology, entities)
        entities = [
            {
                'id': 'foo',
                'kind': 'test-ent',
                'loc': [30, -75],
                'path': [[30, -75], [31, -74], [32, -73]]
            }
        ]
        self.assertRaises(AssertionError, s.update_topology, entities)
        entities = [
            {
                'id': 'foo',
                'kind': 'test-ent'
            }
        ]
        self.assertRaises(KeyError, s.update_topology, entities)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testObsDicts(self, send_to_pool):
        s = sluice.api.Sluice()
        update = [
            {
                'id': 'test-1',
                'attrs': { 'name': 'Update 1' }
            },
            {
                'id': 'test-2',
                'attrs': { 'change': 'something' }
            }
        ]
        s.update_observations(update)
        actual = send_to_pool.call_args[0][0].ingests().get('observations')
        expected = [
            obmap([('id', 'test-1'), ('attrs', obmap([('name', 'Update 1')]))]),
            obmap([('id', 'test-2'), ('attrs', obmap([('change', 'something')]))])
        ]
        self.assertListEqual(expected, actual)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testObsObjects(self, send_to_pool):
        s = sluice.api.Sluice()
        update = [
            ObservationEntity(id='test-1', attrs={ 'name': 'Update 1' }),
            ObservationEntity(id='test-2', attrs={ 'change': 'something' })
        ]
        s.update_observations(update)
        actual = send_to_pool.call_args[0][0].ingests().get('observations')
        expected = [
            obmap([('id', 'test-1'), ('attrs', obmap([('name', 'Update 1')]))]),
            obmap([('id', 'test-2'), ('attrs', obmap([('change', 'something')]))])
        ]
        self.assertListEqual(expected, actual)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testObsBadDicts(self, send_to_pool):
        s = sluice.api.Sluice()
        update = [
            { 'attrs': { 'name': 'Update 1' } },
            { 'attrs': { 'change': 'something' } }
        ]
        self.assertRaises(TypeError, s.update_observations, update)

    @patch.object(sluice.api.Sluice, 'send_to_pool')
    def testObsRemoveDicts(self, send_to_pool):
        s = sluice.api.Sluice()
        remove = [
            {
                'id': 'test-1',
                'attr-id': 'name'
            },
            ObservationEntity(id='test-2', attr_id='change')
        ]
        s.update_observations(remove=remove)
        actual = send_to_pool.call_args[0][0].ingests().get('remove-observations')
        expected = [
            obmap([('id', 'test-1'), ('attr-id', 'name')]),
            obmap([('id', 'test-2'), ('attr-id', 'change')])
        ]
        self.assertListEqual(expected, actual)
        
if '__main__' == __name__:
    unittest.main()

