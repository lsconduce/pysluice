import unittest
import os, uuid
from mock import Mock, patch
from plasma.hose import Hose
from plasma.hose.gang import HoseGang
from plasma.protein import Protein
from sluice.api import *

root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))

class SafeEnv:
    def __enter__(self):
        self.orig = dict(os.environ)
        return self.orig

    def __exit__(self, type, value, traceback):
        for k, v in self.orig.iteritems():
            os.environ[k] = v
        for k in os.environ.keys():
            if k not in self.orig:
                del(os.environ[k])

class APITest(unittest.TestCase):
    @patch('os.path.exists')
    def testGetShareFile(self, exists):
        exists.side_effect = lambda x: 'found' in x
        share_path = [
            '/tmp/foo/bar',
            '/home/found/share'
        ]
        with SafeEnv() as env:
            os.environ['OB_SHARE_PATH'] = os.path.pathsep.join(share_path)
            self.assertEquals('/home/found/share/some/file', Sluice.get_share_file('some/file'))
            self.assertEquals('/tmp/foo/bar/some/file', Sluice.get_share_file('some/file', True))
        share_path = [
            '/tmp/foo/bar',
            '/home/missing/share'
        ]
        with SafeEnv() as env:
            os.environ['OB_SHARE_PATH'] = os.path.pathsep.join(share_path)
            self.assertIsNone(Sluice.get_share_file('some/file'))
            self.assertEquals('/tmp/foo/bar/some/file', Sluice.get_share_file('some/file', True))

    def testAppendSharePath(self):
        share_path = [
            '/tmp/foo/bar',
            '/home/missing/share'
        ]
        extra = '/home/found/share'
        with SafeEnv() as env:
            os.environ['OB_SHARE_PATH'] = os.path.pathsep.join(share_path)
            Sluice.append_share_path(extra)
            self.assertListEqual(share_path+[extra], os.getenv('OB_SHARE_PATH').split(os.path.pathsep))
            Sluice.append_share_path(share_path[0])
            self.assertListEqual(share_path+[extra], os.getenv('OB_SHARE_PATH').split(os.path.pathsep))

    def testPrependSharePath(self):
        share_path = [
            '/tmp/foo/bar',
            '/home/missing/share'
        ]
        extra = '/home/found/share'
        with SafeEnv() as env:
            os.environ['OB_SHARE_PATH'] = os.path.pathsep.join(share_path)
            Sluice.prepend_share_path(extra)
            self.assertListEqual([extra]+share_path, os.getenv('OB_SHARE_PATH').split(os.path.pathsep))
            Sluice.prepend_share_path(share_path[0])
            self.assertListEqual([share_path[0], extra, share_path[1]], os.getenv('OB_SHARE_PATH').split(os.path.pathsep))

    @patch('os.path.exists')
    def testGetEtcFile(self, exists):
        exists.side_effect = lambda x: 'found' in x
        etc_path = [
            '/tmp/foo/bar',
            '/home/found/etc'
        ]
        with SafeEnv() as env:
            os.environ['OB_ETC_PATH'] = os.path.pathsep.join(etc_path)
            self.assertEquals('/home/found/etc/some/file', Sluice.get_etc_file('some/file'))
            self.assertEquals('/tmp/foo/bar/some/file', Sluice.get_etc_file('some/file', True))
        etc_path = [
            '/tmp/foo/bar',
            '/home/missing/etc'
        ]
        with SafeEnv() as env:
            os.environ['OB_ETC_PATH'] = os.path.pathsep.join(etc_path)
            self.assertIsNone(Sluice.get_etc_file('some/file'))
            self.assertEquals('/tmp/foo/bar/some/file', Sluice.get_etc_file('some/file', True))

    def testAppendEtcPath(self):
        etc_path = [
            '/tmp/foo/bar',
            '/home/missing/etc'
        ]
        extra = '/home/found/etc'
        with SafeEnv() as env:
            os.environ['OB_ETC_PATH'] = os.path.pathsep.join(etc_path)
            Sluice.append_etc_path(extra)
            self.assertListEqual(etc_path+[extra], os.getenv('OB_ETC_PATH').split(os.path.pathsep))
            Sluice.append_etc_path(etc_path[0])
            self.assertListEqual(etc_path+[extra], os.getenv('OB_ETC_PATH').split(os.path.pathsep))

    def testPrependEtcPath(self):
        etc_path = [
            '/tmp/foo/bar',
            '/home/missing/etc'
        ]
        extra = '/home/found/etc'
        with SafeEnv() as env:
            os.environ['OB_ETC_PATH'] = os.path.pathsep.join(etc_path)
            Sluice.prepend_etc_path(extra)
            self.assertListEqual([extra]+etc_path, os.getenv('OB_ETC_PATH').split(os.path.pathsep))
            Sluice.prepend_etc_path(etc_path[0])
            self.assertListEqual([etc_path[0], extra, etc_path[1]], os.getenv('OB_ETC_PATH').split(os.path.pathsep))

    @patch('os.path.exists')
    def testGetVarFile(self, exists):
        exists.side_effect = lambda x: 'found' in x
        var_path = [
            '/tmp/foo/bar',
            '/home/found/var'
        ]
        with SafeEnv() as env:
            os.environ['OB_VAR_PATH'] = os.path.pathsep.join(var_path)
            self.assertEquals('/home/found/var/some/file', Sluice.get_var_file('some/file'))
            self.assertEquals('/tmp/foo/bar/some/file', Sluice.get_var_file('some/file', True))
        var_path = [
            '/tmp/foo/bar',
            '/home/missing/var'
        ]
        with SafeEnv() as env:
            os.environ['OB_VAR_PATH'] = os.path.pathsep.join(var_path)
            self.assertIsNone(Sluice.get_var_file('some/file'))
            self.assertEquals('/tmp/foo/bar/some/file', Sluice.get_var_file('some/file', True))

    def testAppendVarPath(self):
        var_path = [
            '/tmp/foo/bar',
            '/home/missing/var'
        ]
        extra = '/home/found/var'
        with SafeEnv() as env:
            os.environ['OB_VAR_PATH'] = os.path.pathsep.join(var_path)
            Sluice.append_var_path(extra)
            self.assertListEqual(var_path+[extra], os.getenv('OB_VAR_PATH').split(os.path.pathsep))
            Sluice.append_var_path(var_path[0])
            self.assertListEqual(var_path+[extra], os.getenv('OB_VAR_PATH').split(os.path.pathsep))

    def testPrependVarPath(self):
        var_path = [
            '/tmp/foo/bar',
            '/home/missing/var'
        ]
        extra = '/home/found/var'
        with SafeEnv() as env:
            os.environ['OB_VAR_PATH'] = os.path.pathsep.join(var_path)
            Sluice.prepend_var_path(extra)
            self.assertListEqual([extra]+var_path, os.getenv('OB_VAR_PATH').split(os.path.pathsep))
            Sluice.prepend_var_path(var_path[0])
            self.assertListEqual([var_path[0], extra, var_path[1]], os.getenv('OB_VAR_PATH').split(os.path.pathsep))

    @patch.object(Hose, 'participate')
    def testGetHose(self, m):
        def set_hose_name(name):
            h = Mock()
            h.name=name
            return h
        m.side_effect = set_hose_name
        s = Sluice()
        h1 = s.get_hose('some-pool')
        self.assertEquals('tcp://localhost/some-pool', h1.name)
        h1.name = 'junk'
        h2 = s.get_hose('some-pool')
        self.assertEquals('junk', h2.name)
        s = Sluice(pool_host='otherhost')
        h3 = s.get_hose('some-pool')
        self.assertEquals('tcp://otherhost/some-pool', h3.name)

    def testDefaultGang(self):
        s = Sluice()
        self.assertListEqual([EDGE_RESPONSE_POOL, TEXTURE_RESPONSE_POOL, NETWORK_RESPONSE_POOL], list(s.get_default_gang()))
        s.set_default_gang(EDGE_RESPONSE_POOL, HEARTBEAT_POOL)
        self.assertListEqual([EDGE_RESPONSE_POOL, HEARTBEAT_POOL], list(s.get_default_gang()))
        class sub(Sluice):
            heart_listen = True
            texture_listen = True
            edge_listen = False
            network_listen = False
        s = sub()
        self.assertListEqual([HEARTBEAT_POOL, TEXTURE_RESPONSE_POOL], list(s.get_default_gang()))

    @patch.object(Hose, 'participate')
    def testGetGang(self, m):
        def set_hose_name(name):
            h = Mock()
            h.name = name
            return h
        m.side_effect = set_hose_name
        s = Sluice()
        g1 = s.get_gang()
        names = list(g1.nth_hose(n).name for n in xrange(g1.hose_count()))
        exp = list('tcp://localhost/%s' % x for x in (EDGE_RESPONSE_POOL, TEXTURE_RESPONSE_POOL, NETWORK_RESPONSE_POOL))
        self.assertListEqual(exp, names)
        h1 = s.get_hose(EDGE_RESPONSE_POOL)
        h1.name = 'junk'
        g2 = s.get_gang(EDGE_RESPONSE_POOL, HEARTBEAT_POOL)
        names = list(g2.nth_hose(n).name for n in xrange(g2.hose_count()))
        self.assertListEqual(['junk', 'tcp://localhost/%s' % HEARTBEAT_POOL], names)
        g3 = s.get_gang(HEARTBEAT_POOL)
        self.assertEquals('tcp://localhost/%s' % HEARTBEAT_POOL, g3.name)

    @patch.object(Hose, 'participate')
    @patch.object(HoseGang, 'await_next')
    def testAwait(self, mgang, mhose):
        def set_hose_name(name):
            h = Mock()
            h.name = name
            h.await_next = Mock()
            h.await_next.return_value = Protein(['mock'], { 'origin': name })
            return h
        mhose.side_effect = set_hose_name
        mgang.return_value = Protein(['mock'], { 'origin': 'gang' })
        s = Sluice()
        p = s.await()
        self.assertEquals('gang', p.ingests().get('origin'))
        p = s.await(['junk'])
        self.assertEquals('tcp://localhost/junk', p.ingests().get('origin'))

    def checkSentProteins(self, method, pool, descrips, ingests, *args, **kwargs):
        with patch.object(Sluice, 'send_to_pool') as m:
            s = Sluice()
            getattr(s, method)(*args, **kwargs)
            p = m.call_args[0][0]
            xpool = m.call_args[0][1]
            self.assertListEqual(descrips, p.descrips().to_json(True))
            self.assertDictEqual(ingests, p.ingests().to_json(True))
            self.assertEquals(pool, xpool)

    def testRequestAtlasView(self):
        method = 'request_atlas_view'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'atlas-view']
        ingests = {}
        self.checkSentProteins(method, pool, descrips, ingests)

    def testRequestZoomLoc(self):
        method = 'request_zoom_loc'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'zoom']
        ingests = {
            'level': 400.0,
            'lat': 25.0,
            'lon': 75.0
        }
        self.checkSentProteins(method, pool, descrips, ingests, 25.0, 75.0, 400.0)

    def testRequestZoomEntity(self):
        method = 'request_zoom_entity'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'zoom']
        ingests = {
            'entity-id': 'foobar'
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'foobar')
        ingests['level'] = 400.0
        self.checkSentProteins(method, pool, descrips, ingests, 'foobar', 400.0)

    def testRequestBookmarkList(self):
        method = 'request_bookmark_list'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'bookmarks']
        ingests = {}
        self.checkSentProteins(method, pool, descrips, ingests)

    @patch.object(uuid, 'uuid4')
    def testCreateBookmark(self, m):
        id = '01234567-89ab-cdef-0123-456789abcdef'
        m.return_value = uuid.UUID(id)
        method = 'create_bookmark'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-bookmark']
        ingests = {
            'name': 'test-book',
            'uid': id
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'test-book')
        ingests['lat'] = 25.0
        ingests['lon'] = 75.0
        ingests['level'] = 400.0
        self.checkSentProteins(method, pool, descrips, ingests, 'test-book', 25.0, 75.0, 400.0)

    def testDeleteBookmark(self):
        method = 'delete_bookmark'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'delete-bookmark']
        ingests = { 'name': 'del-book' }
        self.checkSentProteins(method, pool, descrips, ingests, 'del-book')

    def testAddStaticLayer(self):
        method = 'add_static_layer'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'add-static-layer']
        ingests = {
            'layer': 'layer-name',
            'image_file': 'image.png',
            'lat_min': 25.0,
            'lat_max': 35.0,
            'lon_min': 75.0,
            'lon_max': 90.0
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'layer-name', 'image.png', (25.0, 75.0), (35.0, 90.0))

    def testRemoveStaticLayer(self):
        method = 'remove_static_layer'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'remove-static-layer']
        ingests = { 'layer': 'layer-name' }
        self.checkSentProteins(method, pool, descrips, ingests, 'layer-name')

    def testRemoveAllStaticLayers(self):
        method = 'remove_all_static_layers'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'remove-all-static-layers']
        ingests = { }
        self.checkSentProteins(method, pool, descrips, ingests)

    def testRequestFluoroscopes(self):
        method = 'request_fluoroscopes'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'fluoroscopes']
        ingests = {}
        self.checkSentProteins(method, pool, descrips, ingests)

    def testRequestFluoroscopeTemplates(self):
        method = 'request_fluoroscope_templates'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'fluoroscope-templates']
        ingests = {}
        self.checkSentProteins(method, pool, descrips, ingests)

    def testParseFluoroscopeConfig(self):
        pass

    def testUpdateFluoroscopeConfig(self):
        pass

    def testConfigureFluoroscope(self):
        pass

    def testCreateFluoroscopeTemplate(self):
        pass

    def testCreateFluoroscopeInstance(self):
        pass

    def testRemoveFluoroscopeInstance(self):
        qid = [0,1,2,3,4,5,6,7]
        method = 'remove_fluoroscope_instance'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'remove-fluoro-instance']
        ingests = {
            'SlawedQID': qid
        }
        self.checkSentProteins(method, pool, descrips, ingests, qid)

    def testRemoveFluoroscopeTemplate(self):
        qid = [0,1,2,3,4,5,6,7]
        method = 'remove_fluoroscope_template'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'remove-fluoro-template']
        ingests = {
            'qid': ['SlawedQID', qid]
        }
        self.checkSentProteins(method, pool, descrips, ingests, qid)

    def testRequestLocalizedWebpage(self):
        method = 'request_localized_webpage'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'web']
        ingests = {
            'background': None,
            'id': 'foo-0',
            'url': 'http://maps.google.com/',
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'foo-0', 'http://maps.google.com/')

    def testRequestWebpage(self):
        method = 'request_webpage'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'web']
        ingests = {
            'windshield': [
                {
                    'background': None,
                    'name': 'webfoo',
                    'feld': 'main',
                    'url': 'http://conduce.com/',
                    'visible': True,
                }
            ]
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/')
        ingests['windshield'][0]['feld'] = 'left'
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/', feld='left')

    def testRequestWebpageWithSize(self):
        method = 'request_webpage'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'web']
        ingests = {
            'windshield': [
                {
                    'background': None,
                    'name': 'webfoo',
                    'feld': 'main',
                    'url': 'http://conduce.com/',
                    'visible': True,
                    'size': [0.5, 0.5]
                }
            ]
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/', size = 0.5)


    def testRequestWebpageWithLoc(self):
        method = 'request_webpage'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'web']
        ingests = {
            'windshield': [
                {
                    'background': None,
                    'name': 'webfoo',
                    'feld': 'main',
                    'url': 'http://conduce.com/',
                    'visible': True,
                    'loc': [0.5, 0.5]
                }
            ]
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/', loc = [0.5, 0.5])

    def testInjectJavascript(self):
        method = 'inject_javascript'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'web']
        ingests = {
            'windshield': [
                {
                    'name': 'webfoo',
                    'js': 'document.write("hello world");'
                }
            ]
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'document.write("hello world");')

    def testRemoveWebpage(self):
        method = 'remove_webpage'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'remove-web-instance']
        ingests = {
            'windshield': [ { 'name': 'webfoo' } ]
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo')

    def testRemoveLocalizedWebpage(self):
        method = 'remove_localized_webpage'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'remove-web-instance']
        ingests = {
            'remove-atlas-instance': True,
            'windshield': [ { 'name': 'webby' } ]
        }
        self.checkSentProteins(method, pool, descrips, ingests)

    def testRequestCurrentTime(self):
        method = 'request_current_time'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'time']
        ingests = { }
        self.checkSentProteins(method, pool, descrips, ingests)

    def testRequestVideoFluoroID(self):
        method = 'request_video_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'fluoro-config': {
                'name': 'vname',
                'type': 'video',
                'vid_loc': {
                    'path': 'vpath',
                    'type': 'file'
                }
            },
            'id': 'pid'
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'vname', 'file', 'vpath', placement_entity_id='pid')

    def testRequestVideoFluoroBLTR(self):
        method = 'request_video_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'fluoro-config': {
                'name': 'vname',
                'type': 'video',
                'vid_loc': {
                    'path': 'vpath',
                    'type': 'file'
                }
            },
            'bl': [10, 20],
            'tr': [11, 21]
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'vname', 'file', 'vpath', bl=[10, 20], tr=[11, 21])

    def testRequestVideoFluoroLoc(self):
        method = 'request_video_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'fluoro-config': {
                'name': 'vname',
                'type': 'video',
                'vid_loc': {
                    'path': 'vpath',
                    'type': 'file'
                }
            },
            'loc': [0.5, 0.5],
            'windshield': True
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'vname', 'file', 'vpath', loc=[0.5, 0.5], windshield=True)

    def testRequestTextureFluoroID(self):
        method = 'request_texture_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'fluoro-config': {
                'name': 'tname',
                'type': 'texture',
                'daemon': 'td',
            },
            'id': 'pid'
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'tname', 'td', placement_entity_id='pid')

    def testRequestTextureFluoroBLTR(self):
        method = 'request_texture_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'fluoro-config': {
                'name': 'tname',
                'type': 'texture',
                'daemon': 'td',
            },
            'bl': [10, 20],
            'tr': [11, 21]
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'tname', 'td', bl=[10, 20], tr=[11, 21])

    def testRequestTextureFluoroLoc(self):
        method = 'request_texture_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'fluoro-config': {
                'name': 'tname',
                'type': 'texture',
                'daemon': 'td',
            },
            'loc': [0.5, 0.5],
            'windshield': True
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'tname', 'td', loc=[0.5, 0.5], windshield=True)

    def testRequestStaticTextureFluoroLoc(self):
        # This is most likely to be how we use static images
        method = 'request_texture_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'fluoro-config': {
                'name': 'tname',
                'type': 'texture',
                'daemon': 'td',
                'ignore-movement': True,
                'image-is-static': True,
            },
            'loc': [0.5, 0.5],
            'windshield': True
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'tname', 'td', loc=[0.5, 0.5], windshield=True, static=True)

    def testRequestWebID(self):
        method = 'request_web_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'id': 'foo-0',
            'fluoro-config': {
                'name': 'webfoo',
                'url': 'http://conduce.com/',
                'type': 'web',
            }
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/', placement_entity_id='foo-0')

    def testRequestWebFeld(self):
        method = 'request_web_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'fluoro-config': {
                'name': 'webfoo',
                'type': 'web',
                'url': 'http://conduce.com/',
            }
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/')
        ingests['feld'] = 'left'
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/', feld='left')

    def testRequestWebWithSize(self):
        method = 'request_web_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'size': [0.5, 0.5],
            'fluoro-config': {
                'name': 'webfoo',
                'type': 'web',
                'url': 'http://conduce.com/',
            }
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/', size=[0.5, 0.5])

    def testRequestWebWithLoc(self):
        method = 'request_web_fluoro'
        pool = EDGE_REQUEST_POOL
        descrips = [APP, PROTO, 'request', 'new-fluoro-instance']
        ingests = {
            'loc': [0.5, 0.5],
            'fluoro-config': {
                'name': 'webfoo',
                'type': 'web',
                'url': 'http://conduce.com/',
            }
        }
        self.checkSentProteins(method, pool, descrips, ingests, 'webfoo', 'http://conduce.com/', loc=[0.5, 0.5])

if '__main__' == __name__:
    unittest.main()

