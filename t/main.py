import sys, os
import unittest

def get_suite(tests=None):
    root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    os.environ['OB_ETC_PATH'] = root
    os.environ['OB_SHARE_PATH'] = os.path.pathsep.join((os.path.join(root, 'share'), '/Users/rclancey/src/sluice-data/supply_chain/lib/python'))
    os.environ['OB_VAR_PATH'] = os.path.join(root, 'var')
    dn = os.path.dirname(__file__)
    if dn:
        os.chdir(dn)
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    if tests:
        suite.addTests(loader.loadTestsFromNames(tests))
    else:
        suite.addTests(loader.discover('.', pattern='*_test.py'))
    return suite

def main(tests):
    suite = get_suite(tests)
    runner = unittest.TextTestRunner()
    runner.run(suite)

if '__main__' == __name__:
    main(sys.argv[1:])

